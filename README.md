# Summary

A Java implementation of

[Robust Entity Clustering via Phylogenetic Inference] (ACL 2014)

The code clusters names (in context) into entities. It should be straightforward to extend this code for other types of data by implementing appropriate features.

Please cite the above paper if you use any of this code in your research. For related projects, see [my homepage].

# Using the Code

## Build

* Compile the code from the command line:

        mvn compile

* To build a single jar with all the dependencies included:

        mvn compile assembly:single

## Input

Input files consist of one line per name, where each line has the following format:

        <entity id>\t<mention id>\t<name>\t<document>\n

where

* entity id: a unique integer for each entity
* mention id: not used directly by the code, but may be useful for processing the output
* name: named-entity mention
* document: all words in the document

The entity IDs are provided for evaluation.

## Running experiments

A script showing sample usage is included which may be run from the command line:

        scripts/twitter.sh

This runs on a portion of the Grammy dataset used in the paper.

## Options

Many advanced options are provided in edu.jhu.hlt.phylo.io.ExperimentRunner, which are documented there. Some notable options:

* -root_log_weight: Adjust this parameter to control the number of entities. Larger values result in more entities (higher precision / lower recall).

* -fixed_ordering: Use this if you have a large corpus or some reasonable ordering of the names. See the paper for some discussion on why marginalizing over orderings is recommended in general.

* -resample_topics: Resampling topics may result in a boost in accuracy at the expense of slower inference.

* -pruning: Use trigram pruning to speed up inference. This usually speeds up inference without affecting the quality of the clustering.


[Robust Entity Clustering via Phylogenetic Inference]:http://cs.jhu.edu/~noa/publications/phylo-acl-14.pdf
[my homepage]:http://cs.jhu.edu/~noa/