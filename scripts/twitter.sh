#!/bin/bash

SETTINGS="-corpus data/twitter.txt -num_em_iter 5 -num_estep_samples 10 -num_decode_samples 50 -random_init -pruning"
NTHREAD="4"
THREAD="-XX:ParallelGCThreads=$NTHREAD"
ASSERTIONS="-da"
export MAVEN_OPTS="-XX:MaxPermSize=256m -Xms2G -Xmx4G $ASSERTIONS $THREAD"
mvn exec:java -Dexec.mainClass="edu.jhu.hlt.phylo.io.ExperimentRunner" -Dexec.args="$SETTINGS $DATA $OUTPUT"

#eof
