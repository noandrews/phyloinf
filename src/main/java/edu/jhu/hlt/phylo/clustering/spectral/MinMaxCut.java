package edu.jhu.hlt.phylo.clustering.spectral;

// Feiping Nie, Chris Ding, Dijun Luo, Heng Huang.  Improved
// MinMax Cut Graph Clustering with Nonnegative Relaxation.
// The European Conference on Machine Learning and Principles
// and Practice of Knowledge Discovery in Databases (ECML
// PKDD), Barcelona, 2010.

import java.util.List;
import java.util.ArrayList;

import org.ejml.data.DenseMatrix64F;
import org.ejml.simple.SimpleMatrix;
import org.ejml.ops.CommonOps;

public class MinMaxCut {

    private static final double EPS = 2.2204e-16;

    public static int [] cluster(int ITER, double [][] sim, double [][] init) {

        SimpleMatrix A = SimpleMatrix.wrap(new DenseMatrix64F(sim));
        SimpleMatrix Q = SimpleMatrix.wrap(new DenseMatrix64F(init));

        int class_num = Q.numCols();
        
        //System.err.println("class_num = " + class_num);

        // D = diag(sum(A,2));
        SimpleMatrix D = new SimpleMatrix(sim.length, sim.length);
        for(int i=0;i<sim.length;i++) {
            double sum = 0.0;
            for(int j=0; j<sim.length; j++) {
                sum += sim[i][j];
            }
            D.set(i,i,sum);
        }

        List<Double> obj   = new ArrayList<Double>();
        List<Double> obj1  = new ArrayList<Double>();
        List<Double> orobj = new ArrayList<Double>();

        for(int iter=0; iter<ITER; iter++) {


            // dA = 1./diag(Q'*A*Q);
            SimpleMatrix dA = Q.transpose().mult(A).mult(Q).extractDiag();
            for(int i=0;i<dA.numRows();i++) {
                for(int j=0;j<dA.numCols();j++) {
                    double temp = dA.get(i,j);
                    dA.set(i,j,1.0/temp);
                }
            }
            
            //            System.err.print("dA: "); dA.print();

            // QQ = Q'*D*Q;
            SimpleMatrix QQ = Q.transpose().mult(D).mult(Q);

            //            System.err.print("QQ: "); QQ.print();

            // dD = diag(QQ);
            SimpleMatrix dD = QQ.extractDiag();

            //            System.err.print("dD: "); dD.print();

            // Qb = Q*diag((dA.^2));
            SimpleMatrix temp = new SimpleMatrix(dA);
            for(int i=0;i<dA.numRows();i++) {
                for(int j=0;j<dA.numCols();j++) {
                    double d = temp.get(i,j);
                    temp.set(i,j,d*d);
                }
            }

            //            System.err.print("temp: "); temp.print();

            SimpleMatrix temp_diag = new SimpleMatrix(dA.numRows(), dA.numRows());
            for(int ii=0; ii<dA.numRows(); ii++) {
                temp_diag.set(ii,ii,temp.get(ii,0));
            }
            
            //            System.err.print("temp_diag: "); temp_diag.printDimensions();
            
            SimpleMatrix Qb = Q.mult(temp_diag);

            //            System.err.print("Qb: "); Qb.print(6,6);

            // Lamda = Q'*A*Qb;
            SimpleMatrix Lambda = Q.transpose().mult(A).mult(Qb);

            //            System.err.print("Lambda: "); Lambda.print(6,6);
            
            // Lamda = (Lamda + Lamda')/2;
            Lambda = Lambda.plus(Lambda.transpose()).scale(0.5);

            // S = (A*Qb + eps)./(D*Q*Lamda + eps);
            SimpleMatrix S = A.mult(Qb);
            SimpleMatrix denominator = D.mult(Q).mult(Lambda);

            CommonOps.add(S.getMatrix(), EPS);
            CommonOps.add(denominator.getMatrix(), EPS);
            CommonOps.elementDiv(S.getMatrix(), denominator.getMatrix());
            
            //            System.err.print("S: "); S.print(6,6);

            // S = S.^(1/2);
            for(int i=0;i<S.numRows();i++) {
                for(int j=0; j<S.numCols();j++) {
                    double d = S.get(i,j);
                    S.set(i,j,Math.sqrt(d));
                }
            }

            //            System.err.print("S: "); S.print(6,6);

            // Q = Q.*S;
            CommonOps.elementMult(Q.getMatrix(), S.getMatrix());

            // Q = Q*diag(sqrt(1./diag(Q'*D*Q)));
            temp = Q.transpose().mult(D).mult(Q);
            temp = temp.extractDiag();

            for(int i=0; i<temp.numRows(); i++) {
                for(int j=0; j<temp.numCols(); j++) {
                    double d = temp.get(i,j);
                    temp.set(i,j,Math.sqrt(1.0/d));
                }
            }
            //            System.err.print("temp: "); temp.print(6,6);
            
            temp_diag = new SimpleMatrix(temp.numRows(), temp.numRows());
            for(int ii=0; ii<temp.numRows(); ii++) {
                temp_diag.set(ii,ii,temp.get(ii,0));
            }
            
            // Q is the output clustering

            Q = Q.mult(temp_diag);

            //            System.err.print("Q: "); Q.print(6,6);
    
            // QQI = QQ - eye(class_num);
            SimpleMatrix QQI = QQ.minus(SimpleMatrix.identity(class_num));

            //            System.err.print("QQI: "); QQI.print(6,6);

            // obj(iter) = sum(dA) - trace(Lamda*(QQI));
            double obj_val = dA.elementSum() - Lambda.mult(QQI).trace();
            //System.err.println("obj val: " + obj_val);
            obj.add(obj_val);

            // obj1(iter) = sum(dD.*dA);
            temp = new SimpleMatrix(dD);
            CommonOps.elementMult(temp.getMatrix(), dA.getMatrix());
            double obj1_val = temp.elementSum();
            //System.err.println("obj1 val: " + obj1_val);
            obj1.add(obj1_val);

            // orobj(iter) = sqrt(trace(QQI'*QQI)/(class_num*(class_num-1)));
            double val = Math.sqrt(QQI.transpose().mult(QQI).trace() / ((double)class_num*(class_num-1.0)));
            //System.err.println("Iter " + iter + ": " + val);
            orobj.add(val);
        }

        // Assign clusters to each data point
        int [] ret = new int[sim.length];
        for(int i=0; i<ret.length; i++) {
            double max = Double.NEGATIVE_INFINITY;
            int maxat  = -1;
            for(int k=0; k<class_num; k++) {
                if(Q.get(i,k) > max) {
                    max = Q.get(i,k);
                    maxat = k;
                }
            }
            ret[i] = maxat;
        }
        return ret;
    } 
}