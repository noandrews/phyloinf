package edu.jhu.hlt.phylo.clustering;

import java.io.PrintWriter;
import java.io.Serializable;
import java.util.List;

import edu.jhu.hlt.phylo.evaluate.Evaluator;
import edu.jhu.hlt.phylo.util.Instance;

public interface Clusterer extends Serializable {

	public void cluster(List<Instance> instances);
	
	public void writeTimeStepDebug(PrintWriter pw);
	
	public void writeEvalObjective(Evaluator eval, PrintWriter pw);
	
	public void shutdown();
}
