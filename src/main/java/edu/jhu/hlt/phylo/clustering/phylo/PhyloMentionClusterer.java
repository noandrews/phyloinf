package edu.jhu.hlt.phylo.clustering.phylo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Collections;
import java.util.ArrayList;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.TimeUnit;

import edu.jhu.gm.inf.BeliefPropagation;
import edu.jhu.gm.inf.BeliefPropagation.BeliefPropagationPrm;
import edu.jhu.gm.inf.BeliefPropagation.BpScheduleType;
import edu.jhu.gm.inf.BeliefPropagation.BpUpdateOrder;
import edu.jhu.gm.model.DenseFactor;
import edu.jhu.gm.model.ExplicitFactor;
import edu.jhu.gm.model.Factor;
import edu.jhu.gm.model.FactorGraph;
import edu.jhu.gm.model.Var;
import edu.jhu.gm.model.Var.VarType;
import edu.jhu.gm.model.VarConfig;
import edu.jhu.gm.model.VarSet;
import edu.jhu.hlt.phylo.match.SequencePruner;
import edu.jhu.hlt.phylo.evaluate.Evaluator;
import edu.jhu.hlt.phylo.evaluate.b3Evaluator;
import edu.jhu.hlt.phylo.evaluate.softB3Evaluator;
import edu.jhu.hlt.phylo.graph.AdjacencyList;
import edu.jhu.hlt.phylo.graph.CompleteGraph;
import edu.jhu.hlt.phylo.graph.Edge;
import edu.jhu.hlt.phylo.graph.Edmonds;
import edu.jhu.hlt.phylo.graph.Node;
import edu.jhu.hlt.phylo.distrib.ConditionalStringModel.StringPair;
import edu.jhu.hlt.phylo.distrib.ConditionalStringModel;
import edu.jhu.hlt.phylo.distrib.IndexSampler;
import edu.jhu.hlt.phylo.util.RandomNumberGenerator;
import edu.jhu.hlt.phylo.util.Label;
import edu.jhu.hlt.phylo.util.StringLabel;
import edu.jhu.hlt.phylo.util.Instance;
import edu.jhu.hlt.phylo.lang.AnnotatedString.Language;
import edu.jhu.hlt.phylo.lang.AnnotatedString.EntityType;
import edu.jhu.hlt.phylo.lang.Corpus;
import edu.jhu.hlt.phylo.lang.Document;
import edu.jhu.hlt.phylo.lang.Mention;
import edu.jhu.hlt.phylo.lang.StopWords;
import edu.jhu.hlt.phylo.clustering.spectral.MinMaxCut;

import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ejml.simple.SimpleMatrix;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import edu.jhu.hlt.phylo.distrib.LDA;
import edu.jhu.prim.list.IntArrayList;

public class PhyloMentionClusterer {
	
	public enum MutationModel {
		LOGLINEAR
	}
	
	public enum ContextModel {
		NONE, FIXED_TOPICS, SAMPLED_TOPICS, BLOCK_SAMPLED_TOPICS
	}
	
	public enum SamplerInit {
		RANDOM, TARJAN
	}

	public class TreeMentionClustererParam {
		// Parameter estimation
		public int em_iter = 10;
		public int NUM_E_SAMPLES = 100;
		public double adaGradEta = 0.25; // affinity model
	
		// Inference
		public SamplerInit init = SamplerInit.RANDOM;
		
		// Decoding
		public boolean do_mbr_decode = true;
		public int MBR_ITER = 10;
		public int NUM_DECODE_SAMPLES = 10;
		
		// Constraints
		public boolean trigram_pruning = true;
		public boolean use_doc_constraints = false;
		
		// Pragmatic model
		public boolean pragmatics = true;
		public double gamma = 1.0;
		
		// Mutation model
		public MutationModel mutation_model = MutationModel.LOGLINEAR;
		public boolean split_edit_models = true;
		public double mu = 0.1;
		public double log_mu = Math.log(mu);
		
		// Affinity model
		public boolean fixed_ordering = false;
		public boolean use_affinity_model = true;
		public boolean root_affinity = false;
		public double root_log_weight = Math.log(0.5);
		public double log_one_minus_root_prob = logSubstract(0, root_log_weight);
		public double[] phi = null;
		public double[] phi_grad_sum_squares = null;
		public double[] phi_mean = null;
		public double[] phi_var = null;
		public double topic_identity_bias = 1.0;
		public double phi_variance = 1e3;

		// Topic model
		public ContextModel context_model = ContextModel.SAMPLED_TOPICS;
		public boolean dont_train_topic_model = false;
		public int LDA_RESAMPLE_ITER = 1;
		public double alpha_sum = 0.1; // document-topic dirichlet concentration parameter
		public double tt_beta = 0.01;  // topic-word dirichlet parameter
		public boolean lda_opt = true;        // if the topic model is optimized
		public int NTOPICS = 32;
		public double[][] context_topic_dists = null; // fixed
	}
	
	// Parameters
	TreeMentionClustererParam prm = new TreeMentionClustererParam();
	
	// Hashing
	protected static HashFunction hf = Hashing.murmur3_32();

	// Logging
	private static final Logger log = LogManager.getLogger(PhyloMentionClusterer.class.getName());
	
	// Topic model
	LDA lda = null;
	String[][] docs = null;
	cc.mallet.types.Alphabet vocabulary = new cc.mallet.types.Alphabet();

	// Sampler state
	Variate state;
	
	protected boolean DEBUG = false;

	protected List<EntityType> mention_types = new ArrayList<EntityType>();
	protected List<Language> mention_lang = new ArrayList<Language>();

	protected BidiMap fmap = new DualHashBidiMap();

	// If certain features are given preference
	boolean asymmetric_prior = false;

	// Storage for the saved samples
	List<Variate> sample = new ArrayList<Variate>();

	protected final String ENTITY = "xxENTITYxx";

	// Sampler statistics
	int nperm_samples_accepted = 0;
	int nperm_samples_total = 0;
	int ntopic_samples_accepted = 0;
	int ntopic_samples_total = 0;

	boolean fix_initial_state = false;

	// Affinity model features
	private int[][] ordered_topic_feat;
	private int[][] unordered_topic_feat;
	private int same_topic_feat;
	private int diff_topic_feat;
	private int root_feat;
	private int non_root_feat;

	public final static String PUNC_PREFIX = "^\\p{Punct}+";
	public final static String PUNC_SUFFIX = "\\p{Punct}+$";
	
	protected int num_thread = 1;
	protected String write_name_tokens;

	protected int STATUS_INTERVAL = 100;
	protected int DEBUG_ITER = 0;

	protected int num_filtered_by_doc = 0;

	// Initialization
	protected String init = "best";

	// Corpus
	protected Corpus corpus = null;
	protected List<Instance> instances = Lists.newArrayList();
	protected int[] documents = null; // doc of each coref chain
	protected boolean initialized = false;
	protected boolean supervised = false;
	
	// Coref posterior
	protected boolean compute_posterior = true;
	protected int coref_ncounted = 0;
	protected double[][] coref_posterior = null;

	// Output paths
	protected String tree_path = "1best_phylogeny.txt";
	protected String posterior_path = "coref_posterior.txt";

	// ==================== Data structures ===================
	
	// Graph
	protected int N = -1;
	protected int ROOT = -1;
	protected BiMap<Mention, Integer> vertex_map = HashBiMap.create();
	protected Map<Integer, Set<Integer>> parent_map = Maps.newHashMap();
	protected Map<Integer, Set<Integer>> child_map = Maps.newHashMap();

	// Mutation model
	protected ConditionalStringModel edit_model = null;
	
	protected List<String> canonical_names = Lists.newArrayList();
	protected List<Language> languages = Lists.newArrayList();
	protected List<EntityType> types = Lists.newArrayList();
	protected List<Boolean> visibility = Lists.newArrayList();

	protected int V = -1; // size of the vocabulary
	protected Map<Language, String> topic_model_paths = Maps.newHashMap();

	// How mention strings are normalized
	protected boolean use_stanford_norm = false;

	// If name tokens are sorted
	protected boolean sort_name_tokens = false;

	// Edge probabilities
	protected SimpleMatrix W_mutate = null;    // transducer edge weights
	protected SimpleMatrix M = null;           // edge marginals
	protected double[][] edge_affinity = null; // distance in ddCRP

	// Decoding
	protected int num_mbr_starts = 5;

	// Diagnostics
	protected boolean DIAGNOSTICS = true;
	double mean_pragma_log_root = 0.0;
	int curr_em_iter = 0;

	// Scores per iteration
	List<Integer> em_indices = Lists.newArrayList();

	public static class Variate {

		public double[] Z;
		public int[] tree;
		public int[] topics;
		public List<Integer> perm;
		public List<Integer> ord;
		public List<Integer> entities;
		public double weight = 1.0;

		public Variate(Variate var) {
			int n = var.tree.length;
			this.Z = new double[n];
			System.arraycopy(var.Z, 0, this.Z, 0, n);
			this.tree = new int[n];
			System.arraycopy(var.tree, 0, this.tree, 0, n);
			this.topics = new int[n];
			System.arraycopy(var.topics, 0, this.topics, 0, n);
			this.perm = new ArrayList<Integer>(var.perm);
			this.ord = new ArrayList<Integer>(var.ord);
			this.entities = new ArrayList<Integer>(var.entities);
			this.weight = var.weight;
		}

		public Variate(int N) {
			tree = new int[N];
			topics = new int[N];
			Z = new double[N];
			perm = new ArrayList<Integer>();
			ord = new ArrayList<Integer>();
			entities = new ArrayList<Integer>();
			weight = 1.0;
		}

		public Variate(int[] tree, int[] topics, List<Integer> perm,
				List<Integer> ord, double[] Z) {
			this.tree = tree;
			this.topics = topics;
			this.perm = perm;
			this.ord = ord;
			this.entities = null;
			this.Z = Z;
		}

		public Variate(int[] tree, int[] topics, List<Integer> perm,
				List<Integer> ord, double[] Z, List<Integer> entities) {
			this.tree = tree;
			this.topics = topics;
			this.perm = perm;
			this.ord = ord;
			this.entities = entities;
			this.Z = Z;
		}

		public Variate(int[] tree, int[] topics, List<Integer> perm,
				List<Integer> ord, double[] Z, List<Integer> entities,
				double weight) {
			this(tree, topics, perm, ord, Z, entities);
			this.weight = weight;
		}

		@Override
		public int hashCode() {
			Hasher f = hf.newHasher();
			for (int i = 0; i < tree.length; i++) {
				f = f.putInt(tree[i]);
			}
			return f.hash().asInt();
		}

		@Override
		public boolean equals(final Object obj) {
			if (obj instanceof Variate) {
				final Variate other = (Variate) obj;
				if (tree.length != other.tree.length)
					return false;
				for (int i = 0; i < tree.length; i++) {
					if (tree[i] != other.tree[i])
						return false;
				}
				return true;
			} else {
				return false;
			}
		}
	}
	
	public PhyloMentionClusterer() {}

	public TreeMentionClustererParam getParam() {
		return prm;
	}

	public void exactMatchBaseline() {
		Map<String, Integer> partition = Maps.newHashMap();
		for (int i = 0; i < canonical_names.size(); i++) {
			String name = canonical_names.get(i);
			if (!partition.containsKey(name)) {
				partition.put(name, partition.size());
			}
			int l = partition.get(name);
			instances.get(i).setHypLabel(new StringLabel(Integer.toString(l)));
		}

		// Do a hard B3 evaluation
		Evaluator sampleB3Eval = new b3Evaluator();
		sampleB3Eval.evaluate(instances);
		StringWriter writer = new StringWriter();
		sampleB3Eval.display(new PrintWriter(writer));
		log.info("[Exact match B3]\t" + writer.toString().trim());
	}

	public double GEM(int max_iter) {
		double ll = Double.NaN;

		curr_em_iter = 0;

		// Initialize Ada Grad stuff
		prm.phi_grad_sum_squares = new double[prm.phi.length];

		while (curr_em_iter < max_iter) {

			long startTime = System.currentTimeMillis();

			if (!fix_initial_state) {
				//log.info("Setting initial sampler state...");
				getInitialState();
			} else {
				//log.info("Using a preset sampler state...");
			}
			
			// Step 1: E-Step -- Sample trees, topics, and ordering
			List<Variate> sample = runSampler(prm.NUM_E_SAMPLES);

			if (sample.size() < 1) {
				throw new RuntimeException("no samples in E step");
			}

			// Step 2: Compute conditional objective
			//log.info("Computing sample LL...");
			ll = computeSampleLogLikelihood(sample, prm.phi);

			if (Double.isNaN(ll) || Double.isInfinite(ll)) {
				throw new RuntimeException("bad LL: something is wrong");
			}

			long currTime = System.currentTimeMillis();
			long millis = currTime - startTime;

			String time = String.format(
					"%d min, %d sec",
					TimeUnit.MILLISECONDS.toMinutes(millis),
					TimeUnit.MILLISECONDS.toSeconds(millis)
							- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
									.toMinutes(millis)));

			//log.info("E-step time: " + time);

			// Step 3: M-Step -- Update parameters
			startTime = System.currentTimeMillis();

			// Optimize mutation model parameters
			Map<StringPair, Double> pairs = getWeightedPairs(sample);

			//log.debug("There are " + pairs.size() + " distinct name pairs.");

			if(DEBUG) {
				for(StringPair key : pairs.keySet()) {
					log.info("w("+key.input + " -> " + key.output+")="+pairs.get(key));
				}
			}
			
			edit_model.optimize(pairs);

			// Optimize affinity model parameters
			if (prm.context_model != ContextModel.NONE) {
				onlineImprove(curr_em_iter, sample);
			}

			// Step 4: Set the transducer edge weights based on updated params
			setEdgeWeights();

			currTime = System.currentTimeMillis();
			millis = currTime - startTime;

			time = String.format(
					"%d min, %d sec",
					TimeUnit.MILLISECONDS.toMinutes(millis),
					TimeUnit.MILLISECONDS.toSeconds(millis)
							- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
									.toMinutes(millis)));
			log.info("M-step time: " + time);

			// Step 5: Update iteration number
			log.info("[EM] Iteration " + curr_em_iter + " of " + this.prm.em_iter + ": sample score =" + ll);

			curr_em_iter++;
		}

		return ll;
	}

	// The instances should be marked with gold labels
	// and have ID's corresponding to the coref chains
	// in the document collections
	public List<Instance> cluster() {

		// Log parameter settings
		log.info("Learn root prob = " + prm.root_affinity);
		log.info("Log root weight / probability = " + prm.root_log_weight);
		log.info("Context model = " + prm.context_model);
		log.info("Mutation model = " + prm.mutation_model);
		log.info("Number of topics = " + prm.NTOPICS);
		log.info("Number of EM iterations = " + prm.em_iter);
		log.info("Number of samples per e-step = " + prm.NUM_E_SAMPLES);
		log.info("Number of samples for decode = " + prm.NUM_DECODE_SAMPLES);
		log.info("MBR decoding = " + prm.do_mbr_decode);
		log.info("Pragmatic model = " + prm.pragmatics);
		log.info("Pragmatic model strength = " + prm.gamma);
		log.info("Number of spectral clustering iterations = " + prm.MBR_ITER);
		log.info("Trigram pruning = " + prm.trigram_pruning);
		
		// Eestimate the parameters of the model
		GEM(prm.em_iter);

		// Find a 1-best phylogeny given the estimated parameters
		int[] phylogeny = getPhylogeny();

		// Diagnostic: exact string match baseline
		exactMatchBaseline();

		// Diagnostic: evaluate the last sample from the chain
		labelInstances(getConnectedComponents(sample.get(sample.size() - 1).tree));
		Evaluator sampleB3Eval = new b3Evaluator();
		sampleB3Eval.evaluate(instances);
		StringWriter writer = new StringWriter();
		sampleB3Eval.display(new PrintWriter(writer));
		log.info("[Last sample B3]\t" + writer.toString().trim());

		// Find connected components of the phylogeny
		List<Set<Integer>> cc = getConnectedComponents(phylogeny);

		// Labels instances according to connected components
		labelInstances(cc);

		Evaluator hardB3Eval = new b3Evaluator();
		hardB3Eval.evaluate(instances);
		writer = new StringWriter();
		hardB3Eval.display(new PrintWriter(writer));
		log.info("[Most likely B3]\t" + writer.toString().trim());

		// Do a soft evaluation
		Evaluator softB3Eval = new softB3Evaluator(this.coref_posterior);
		softB3Eval.evaluate(instances);
		writer = new StringWriter();
		softB3Eval.display(new PrintWriter(writer));
		log.info("[Fuzzy B3]\t" + writer.toString().trim());

		// MBR
		if (prm.do_mbr_decode) {
			doMBR(sample);
			hardB3Eval = new b3Evaluator();
			hardB3Eval.evaluate(instances);
			writer = new StringWriter();
			hardB3Eval.display(new PrintWriter(writer));
			log.info("[MBR B3] " + writer.toString().trim());
		}

		return instances;
	}
	
	
	// Resample:
	// 1) Ordering | Phylogeny, Topics (MH proposal)
	// 2) Topics | Ordering, Phylogeny (MH proposal)
	// 3) Phylogeny | Ordering, Topics (Gibbs sample)
	public void blockedSample() {
		// i | p, t
		if(!prm.fixed_ordering) {
			metropolisHastingsOrdering();
		}

		// t | i, p
		if (prm.context_model == ContextModel.BLOCK_SAMPLED_TOPICS) {
			blockMetropolisHastingsTopics();
		} else if (prm.context_model == ContextModel.SAMPLED_TOPICS) {
			metropolisHastingsTopics();
		}

		// p | i, t
		this.mean_pragma_log_root  = 0.0;
		samplePhylogeny(prm.pragmatics);
		this.mean_pragma_log_root /= N;
	}
	
	List<Variate> runSampler(int iter) {
		log.debug("Running sampler for " + iter + " iterations");

		List<Variate> sample = Lists.newArrayList();

		for (int i = 0; i < iter; i++) {
			blockedSample();
			sample.add(state);
		}

//		log.debug("Number of distinct trees in sample: " + numTrees(sample)
//				+ " (of " + sample.size() + ")");
//		log.debug("Number of distinct perms in sample: " + numPerms(sample)
//				+ " (of " + sample.size() + ")");
//		log.debug("p_accept=" + nperm_samples_accepted + "/"
//				+ nperm_samples_total + " t_accept=" + ntopic_samples_accepted
//				+ "/" + ntopic_samples_total);

		// Reset counters
		this.nperm_samples_accepted = 0;
		this.nperm_samples_total = 0;
		this.ntopic_samples_accepted = 0;
		this.ntopic_samples_total = 0;
		
		return sample;
	}
	
	public void doMBR(List<Variate> sample) {

		if (sample.size() < 1) {
			throw new RuntimeException("empty sample");
		}

		log.debug("\n" + numTrees(sample) + " of " + sample.size()
				+ " distinct trees in sample\n");

		// Best solution so far according to different criteria
		double bestERand = 0;
		int[] bestLabels = null;
		int bestIndex = 0;

		Set<Integer> numCCs = Sets.newHashSet();

		int i = 0;
		for (Variate var : sample) {
			List<Set<Integer>> cc = getConnectedComponents(var.tree);

			// Diagnostics
			numCCs.add(cc.size());

			int[] labels = ccToInts(cc);
			double eRand = expectedRandIndex(labels, coref_posterior);

			if (eRand > bestERand) {
				bestERand = eRand;
				bestLabels = labels;
				bestIndex = i;
			}

			i += 1;
		}

		log.debug("Best expected Rand in sample: " + bestERand);
		log.debug("B3 of best expected Rand sample: ");
		assignLabelsToInstances(bestLabels);
		Evaluator sampleB3Eval = new b3Evaluator();
		sampleB3Eval.evaluate(instances);
		sampleB3Eval.display(new PrintWriter(System.out, true));

		if (this.num_mbr_starts > 0) {

			// Now pick some samples at random and try optimizing MBR
			Collections.shuffle(sample, RandomNumberGenerator.getRandom());

			// MBR optimization from the best sample
			Variate var = sample.get(bestIndex);
			List<Set<Integer>> cc = getConnectedComponents(var.tree);

			int[] labels = ccToInts(cc);
			double eRand = expectedRandIndex(labels, coref_posterior);

			log.debug("Expected Rand index prior to clustering: " + eRand);

			Clustering clustering = MBR(cc);

			log.debug("MBR eRand = " + clustering.score);

			if (clustering.score > bestERand) {
				bestERand = clustering.score;
				bestLabels = clustering.labels;
			}

			for (i = 0; i < this.num_mbr_starts - 1; i++) {
				var = sample.get(i);
				cc = getConnectedComponents(var.tree);

				labels = ccToInts(cc);
				eRand = expectedRandIndex(labels, coref_posterior);

				log.debug("Expected Rand index prior to clustering: " + eRand);

				clustering = MBR(cc);

				log.debug("MBR eRand = " + clustering.score);

				if (clustering.score > bestERand) {
					bestERand = clustering.score;
					bestLabels = clustering.labels;
				}
			}
		}

		// Print diagnostics
		log.debug(numCCs.size() + " distinct #s of entities");
		log.debug("Best expected Rand Index = " + bestERand);

		// Assign the best labels
		assignLabelsToInstances(bestLabels);
	}

	public Clustering MBR(List<Set<Integer>> cc) {
		if (this.coref_ncounted == 0) {
			log.debug("No coref stats counted. Can't compute MBR solution.");
			System.exit(1);
		}

		int K = cc.size();

		// Take the current labels as the initial clustering
		double[][] init = new double[N][];
		for (int i = 0; i < N; i++) {
			init[i] = new double[K];
			for (int k = 0; k < K; k++) {
				init[i][k] = 0.2d;
			}
		}

		int cluster = 0;
		int total_pts = 0;
		for (Set<Integer> component : cc) {
			for (int i : component) {
				total_pts += 1;
				init[i][cluster] += 1.0d;
			}
			cluster++;
		}

		// Solve the non-negative relaxation of the cut
		// log.debug("=> Doing MBR decode (may take a while");
		int[] labels = MinMaxCut.cluster(prm.MBR_ITER, coref_posterior, init);

		if (total_pts != N) {
			log.debug("we have a problem");
			System.exit(1);
		}

		double eRand = expectedRandIndex(labels, coref_posterior);

		return new Clustering(labels, eRand);
	}

	public double expectedFBeta(int[] labels, double[][] posterior, double beta) {
		double eTP = 0.0;
		double eFP = 0.0;
		double eFN = 0.0;

		for (int i = 0; i < N; i++) {
			for (int j = i; j < N; j++) {
				if (i != j) {
					boolean same_cluster = labels[i] == labels[j] ? true
							: false;
					if (same_cluster) {
						eTP += posterior[i][j];
						eFP += (1.0 - posterior[i][j]);
					} else {
						eFN += posterior[i][j];
					}
				}
			}
		}

		double eP = eTP / (eTP + eFP);
		double eR = eTP / (eTP + eFN);

		return (beta * beta + 1.0) * eP * eR / (beta * beta * eP + eR);
	}

	public double expectedRandIndex(int[] labels, double[][] posterior) {
		// log.debug("Computing expected Rand index...");

		double eTP = 0.0;
		double eTN = 0.0;
		double eFP = 0.0;
		double eFN = 0.0;

		for (int i = 0; i < N; i++) {
			for (int j = i; j < N; j++) {
				if (i != j) {
					boolean same_cluster = labels[i] == labels[j] ? true
							: false;
					if (same_cluster) {
						eTP += posterior[i][j];
						eFP += (1 - posterior[i][j]);
					} else {
						eFN += posterior[i][j];
						eTN += (1 - posterior[i][j]);
					}
				}
			}
		}

		return (eTP + eTN) / (eTP + eTN + eFP + eFN);
	}

	public List<Variate> getSample() {
		return sample;
	}

	public int[] getPhylogeny() {

		log.debug("===== DECODING =====");

		if (!fix_initial_state) {
			log.info("Setting initial sampler state...");
			getInitialState();
		} else {
			log.info("Using a preset sampler state...");
		}
		
		// Accumulate samples (fixed model parameters)
		this.sample = runSampler(prm.NUM_DECODE_SAMPLES);

		log.debug("Counting the sample...(size=" + sample.size() + ")");

		// Initialize storage for partial counts
		this.coref_posterior = new double[N][];
		for (int i = 0; i < N; i++) {
			this.coref_posterior[i] = new double[N];
		}

		this.M = new SimpleMatrix(N, N);

		for (Variate var : sample) {
			countTreeSample(var.tree);
			countCoref(var.tree, coref_posterior);
		}

		normalizeEdgeCounts(sample.size());

		for (int x = 0; x < N; x++) {
			for (int y = 0; y < N; y++) {
				coref_posterior[x][y] /= sample.size();
			}
		}

		return bestTree(sample);
	}

	protected void countTreeSample(int[] s) {
		// Collect sufficient statistics of the sample
		for (int i = 0; i < s.length; i++) {
			int x = s[i];
			double m = M.get(x, i);
			M.set(x, i, m + 1.0);
		}
	}

	public int[] bestTree(List<Variate> sample) {
		int[] best = null;
		double best_score = Double.NEGATIVE_INFINITY;
		for (Variate var : sample) {
			double score = logPScore(var, prm.phi);
			if (score > best_score) {
				best = var.tree;
				best_score = score;
			}
		}
		return best;
	}

	public void fixInitialState() {
		this.fix_initial_state = true;
	}

	public int ntopics() {
		return prm.NTOPICS;
	}

	public void learnRootAffinity(boolean b) {
		log.debug("Learn root affinity: " + b);
		prm.root_affinity = b;
	}

	public void dontTrainTopicModel() {
		prm.dont_train_topic_model = true;
	}

	public void setTopicIdentityBias(double bias) {
		prm.topic_identity_bias = bias;
	}

	public List<Integer> getOrdering() {
		return this.state.perm;
	}

	public double[] getAffinityParams() {
		return prm.phi;
	}

	public double[][] getTopicDistributions() {
		return prm.context_topic_dists;
	}

	public void setNumTopics(int ntopics) {
		log.debug("Using " + ntopics + " LDA topics");
		prm.NTOPICS = ntopics;
	}

	public void initGraph() {

		// All parents
		Set<Integer> all = new HashSet<Integer>();
		for (int i = 0; i < N; i++) {
			all.add(i);
		}

		if (prm.trigram_pruning) {
			log.debug("Using trigram pruning...");

			// Pruning stats
			double avg_num_parents = 0.0;

			// Build parent sets
			for (int v1 = 0; v1 < N; v1++) {
				String v1_str_lower = canonical_names.get(v1).toLowerCase();
				Set<Integer> v1_parent_set = new HashSet<Integer>();

				// Properties of v1
				EntityType v1_type = types.get(v1);

				for (int v2 = 0; v2 < N; v2++) {

					// Short-circuit
					if (v1 == v2) {
						// Everyone has ROOT as a possible parent
						v1_parent_set.add(v1);
						continue;
					}

					String v2_str_lower = canonical_names.get(v2).toLowerCase();
					EntityType v2_type = types.get(v2);

					// Check that v1 and v2 have the same type
					if (v1_type != v2_type) {
						continue;
					}

					// If both strings are <= 3, they must match (case
					// insensitive)
					if (v1_str_lower.length() <= 3
							&& v2_str_lower.length() <= 3) {
						if (!v1_str_lower.equals(v2_str_lower)) {
							continue;
						}
					}

					if (v1_str_lower.length() <= 3) {
						if (!v2_str_lower.contains(v1_str_lower)) {
							continue;
						}
					}

					if (v2_str_lower.length() <= 3) {
						if (!v1_str_lower.contains(v2_str_lower)) {
							continue;
						}
					}

					if (v1_str_lower.length() >= 3
							&& v2_str_lower.length() >= 3) {
						if (!SequencePruner.shareTrigrams(v1_str_lower,
								v2_str_lower)) {
							continue;
						}
					}

					v1_parent_set.add(v2);
				}

				if (!(v1_parent_set.size() >= 1)) {
					throw new RuntimeException(
							"parent set should at least include v1");
				}

				avg_num_parents += v1_parent_set.size();

				if (prm.use_doc_constraints) {
					v1_parent_set = filterByDoc(v1, v1_parent_set);
				}

				parent_map.put(v1, v1_parent_set);
			}

			if (prm.use_doc_constraints) {
				log.debug("WARNING: within-document coref used as constrainst: "
						+ this.num_filtered_by_doc + " filtered edges.\n");
			}

			log.debug("Average number of parents: " + (avg_num_parents / N));

			// Create the child set from the parent set
			for (int v1 = 0; v1 < N; v1++) {
				Set<Integer> v1_child_set = new HashSet<Integer>();
				for (int v2 = 0; v2 < N; v2++) {
					if (parent_map.get(v2).contains(v1)) {
						v1_child_set.add(v2);
					}
				}
				v1_child_set.remove(v1);
				child_map.put(v1, v1_child_set);
			}

			// Edge from ROOT
			child_map.put(ROOT, all);

			return;
		}

		for (int i = 0; i < N; i++) {
			// Parents
			parent_map.put(i, all);

			// Children
			Set<Integer> children = new HashSet<Integer>(all);
			children.remove(i);
			child_map.put(i, children);
		}

		// Edge from ROOT
		child_map.put(ROOT, all);
	}

	protected Set<Integer> filterByDoc(int v, Set<Integer> vset) {
		Set<Integer> res = Sets.newHashSet();

		for (int u : vset) {
			if (u == v) {
				res.add(u);
			} else if (documents[u] != documents[v]) {
				res.add(u);
			} else {
				num_filtered_by_doc++;
			}
		}

		return res;
	}

	public IntArrayList getEdgeFeatures(int parent, int child, int xt, int yt) {

		IntArrayList fs = new IntArrayList(10);

		if (prm.root_affinity) {
			if (parent == child) {
				// ROOT -> y
				fs.add(root_feat);
				return fs;
			} else {
				// x -> y
				fs.add(non_root_feat);
			}
		}

		int[] ts = { xt, yt };

		// Sort
		Arrays.sort(ts);

		// Topic sequence
		fs.add(ordered_topic_feat[xt][yt]);

		// Topic combination
		fs.add(unordered_topic_feat[xt][yt]);

		// Topic identity with label
		if (xt == yt) {
			fs.add(same_topic_feat);
		} else {
			fs.add(diff_topic_feat);
		}

		assert (fs.size() > 0) : "no features for parent=" + parent + " child="
				+ child;

		return fs;
	}

	protected int getFeature(String f) {
		// return fmap.lookupIndex(f);
		if (fmap.containsKey(f)) {
			return (Integer) fmap.get(f);
		}
		int k = fmap.size();
		fmap.put(f, k);
		return k;
	}

	protected String getFeature(int k) {
		return (String) fmap.getKey(k);
	}

	protected void initAffinityModel() {

		log.debug("Initializing affinity model...");

		this.ordered_topic_feat = new int[prm.NTOPICS][];
		this.unordered_topic_feat = new int[prm.NTOPICS][];

		// Features for all topic combinations (no labels)
		for (int t1 = 0; t1 < prm.NTOPICS; t1++) {
			this.ordered_topic_feat[t1] = new int[prm.NTOPICS];
			this.unordered_topic_feat[t1] = new int[prm.NTOPICS];
			for (int t2 = 0; t2 < prm.NTOPICS; t2++) {
				this.ordered_topic_feat[t1][t2] = getFeature("topic_sequence=" + t1 + "," + t2);

				int[] ts = { t1, t2 };

				// Sort
				Arrays.sort(ts);

				// Topic sequence
				this.unordered_topic_feat[t1][t2] = getFeature("topic_combination=" + ts[0] + "," + ts[1]);
			}
		}

		// Backoff topic identity
		this.same_topic_feat = getFeature("topic-identity");

		// Root & non-root features
		this.root_feat = getFeature("root-parent");
		this.non_root_feat = getFeature("non-root-parent");

		// Debug display
		log.debug("\nUsing " + fmap.size() + " features!\n");

		// Initialize the parameter vector
		prm.phi = new double[fmap.size()];
		prm.phi_mean = new double[fmap.size()];
		prm.phi_var = new double[fmap.size()];

		// Default prior for all features
		for (int k = 0; k < fmap.size(); k++) {
			prm.phi_mean[k] = 0.0;
			prm.phi_var[k] = prm.phi_variance;
		}

		// Initialize affinity model parameters
		for (int k = 0; k < prm.phi.length; k++) {
			prm.phi[k] = prm.phi_mean[k];

			if (k == this.same_topic_feat) {
				prm.phi[k] += prm.topic_identity_bias;
			}
		}
	}

	public static boolean isNumeric(String str) {
		return str.matches("-?\\d+(\\.\\d+)?");
	}

	protected double logTopicProb(int[] t) {
		if (prm.context_model == ContextModel.NONE) {
			return 0.0;
		}
		if (prm.dont_train_topic_model) {
			return 0.0;
		}
		double log_p = 0;
		// The probability of the topic of each mention, conditioned
		// on the topics in the document it occurs
		for (int v = 0; v < N; v++) {
			log_p += Math.log(prm.context_topic_dists[v][t[v]]);
		}
		return log_p;
	}

	public double logPScore(Variate var, double[] phi) {
		// Unary topic factors
		double log_topic_score = logTopicProb(var.topics);

		// Binary factors
		double log_edge_score = 0;
		for (int y = 0; y < var.tree.length; y++) {
			int x = var.tree[y];
			log_edge_score += log_affinity(x, var.topics[x], y, var.topics[y], phi, var.Z);
			log_edge_score += W_mutate.get(x, y);
		}

		return log_topic_score + log_edge_score;
	}

	public double[] phi() {
		return prm.phi;
	}

	public double currLogScore() {
		return logPScore(state, prm.phi);
	}

	public double currLogAffinity() {
		return logPaffinity(state, prm.phi);
	}

	protected void initDataStructs(int[] s) {
		return;
	}

	public void printTree() {
		printTree(state);
	}

	public void printTree(Variate var) {
		int i = 0;
		List<Set<Integer>> cc = getConnectedComponents(var.tree);
		for (Set<Integer> c : cc) {
			log.info("Component " + i + ": ");
			for (int v : c) {
				int o = var.ord.get(v);
				int p = var.perm.get(v);
				int v_par = var.tree[v];
				String ord_str = "("+o+"/"+p+")";
				String v_str = canonical_names.get(v);
				String vpar_str = canonical_names.get(var.tree[v]);
				String v_topic = Integer.toString(var.topics[v]);
				String vpar_topic = Integer.toString(var.topics[var.tree[v]]);
				String v_lab = corpus.mentions().get(v).getEntity();
				String vpar_lab = corpus.mentions().get(var.tree[v]).getEntity();
				if (v == state.tree[v])
					log.info(ord_str + "\t" + "(" + v + ")" + v_str + "/" + v_lab + "/"
							+ v_topic + " <- ROOT");
				else
					log.info(ord_str + "\t" + "(" + v + ")" + v_str + "/" + v_lab + "/"
							+ v_topic + " <- " + "(" + v_par + ")" + vpar_str + "/" + vpar_lab
							+ "/" + vpar_topic);
			}
			i++;
		}
	}

	public void countCoref(int[] tree, double[][] coref_posterior) {
		List<Set<Integer>> ccs = getConnectedComponents(tree);
		for (Set<Integer> cc : ccs) {
			for (int u : cc) {
				for (int v : cc) {
					if (u != v) {
						coref_posterior[u][v] += 1.0;
					}
				}
				coref_posterior[u][u] += 1.0;
			}
		}
		this.coref_ncounted += 1;
	}

	protected int getVertex(Mention cc) {
		return vertex_map.get(cc);
	}

	protected Mention getCorefChain(int vertex) {
		return vertex_map.inverse().get(vertex);
	}

	protected Set<Integer> parents(int vertex) {
		return parent_map.get(vertex);
	}

	protected Set<Integer> children(int vertex) {
		return child_map.get(vertex);
	}

	public boolean initialize(Corpus corpus) {

		assert(corpus != null) : "null corpus";
		
		log.debug("Initializing...");

		this.corpus = corpus;
		
		for(Mention m : corpus.mentions()) {
			instances.add(new Instance(m.getEntity()));
		}
		
		N = instances.size();
		log.debug("N=" + N);
		if (N <= 0) {
			log.debug("Empty input");
			System.exit(1);
		}
		documents = new int[N];
		log.debug(corpus.mentions().size() + " mentions total");

		// Initialize sampler state
		state = new Variate(N);

		// Index the documents in the corpus
		Map<String, Integer> doc_indexes = Maps.newHashMap();
		int ndocs = 0;
		for (Mention m : corpus.mentions()) {
			String doc_id = m.getDocId();
			if (!doc_indexes.containsKey(doc_id)) {
				doc_indexes.put(doc_id, ndocs++);
			}
		}

		// Extract names from the canonical mention of each chain
		int index = 0;
		for (Mention m : corpus.mentions()) {
			String name = m.getMention();
			canonical_names.add(name);
			languages.add(m.getLanguage());
			types.add(m.getType());
			documents[index] = doc_indexes.get(m.getDocId());
			vertex_map.put(m, index++);
		}

		// Initialize the graph
		log.debug("Initializing the graph...");
		initGraph();
		log.debug("Done initializing the graph.");

		// Initialize the mutation model
		edit_model = new ConditionalStringModel(num_thread, canonical_names, languages, types, parent_map);

		// Finalize: initialize the transducer with all possible pairs
		// This is where a fixed array for all possible pair *types*
		// is allocated, and subsequently updated with batches in EM steps.
		edit_model.finalize();

		// Initialize transducer / mutation edge weights
		log.debug("Calculating transducer edge weights...");
		setEdgeWeights();

		// Initialize the topic model
		initTopicModel();

		// Initialize the edge affinity model
		initAffinityModel();

		initialized = true;

		// Finally, calibrate alpha
		log.debug("Done initializing.");

		return initialized;
	}

	// AdaGrad
	public double[] getStep(double[] grad, int iter) {
		double[] step = new double[grad.length];

		for (int i = 0; i < prm.phi.length; i++) {
			prm.phi_grad_sum_squares[i] += grad[i] * grad[i];
			double learningRate = prm.adaGradEta / Math.sqrt(1e-9 + prm.phi_grad_sum_squares[i]);
			assert !Double.isNaN(learningRate);
			if (learningRate == Double.POSITIVE_INFINITY) {
				if (prm.phi_grad_sum_squares[i] != 0.0) {
					log.warn("Gradient was non-zero but learningRate hit positive infinity: "
							+ prm.phi_grad_sum_squares[i]);
				}
				// Just return zero. The gradient is probably 0.0.
				learningRate = 0.0;
			}
			step[i] = learningRate;
		}

		return step;
	}

	public boolean onlineImprove(int iter, List<Variate> sample) {

		if (sample.size() < 1) {
			RuntimeException e = new RuntimeException("empty sample");
			e.printStackTrace();
			throw e;
		}

		double[] grad = sampleGradient(sample, prm.phi);
		double[] step = getStep(grad, iter);

		for (int i = 0; i < prm.phi.length; i++) {
			prm.phi[i] += step[i] * grad[i];
		}

		return true;
	}

	public static class Clustering {
		public int[] labels;
		public double score;

		public Clustering(int[] labels, double score) {
			this.labels = labels;
			this.score = score;
		}
	}

	public double dot(int parent, int child, int xt, int yt, double[] params) {
		double sum = 0d;
		IntArrayList fs = getEdgeFeatures(parent, child, xt, yt);
		for (int i = 0; i < fs.size(); i++) {
			sum += params[fs.get(i)];
		}
		return sum;
	}

	protected double[] sampleGradient(List<Variate> sample, double[] params) {
		double[] grad = new double[fmap.size()];

		double[] observed_counts = new double[fmap.size()];
		double[] expected_counts = new double[fmap.size()];

		for (Variate var : sample) {
			Set<Integer> hs = Sets.newTreeSet();
			for (int y : var.ord) {
				hs.add(y);
				Set<Integer> ps = Sets.intersection(hs, parents(y));
				for (int x : ps) {
					if(x==y && !prm.root_affinity) {
						continue;
					}
					
					double pr = Math.exp(dot(x, y, var.topics[x], var.topics[y], params) - var.Z[y]);
					IntArrayList fs = getEdgeFeatures(x, y, var.topics[x], var.topics[y]);

					// Observed counts
					if (x == var.tree[y]) {
						for (int i = 0; i < fs.size(); i++) {
							observed_counts[fs.get(i)] += 1.0f;
						}
					}

					// Expected counts
					for (int i = 0; i < fs.size(); i++) {
						expected_counts[fs.get(i)] += pr;
					}
				}
			}
		}

		for (int k = 0; k < grad.length; k++) {
			grad[k] = observed_counts[k] - expected_counts[k];
		}

		// Gaussian prior
		for (int k = 0; k < grad.length; k++) {
			grad[k] -= (params[k] - prm.phi_mean[k]) / (prm.phi_var[k] * prm.phi_var[k]);
		}

		return grad;
	}

	public int numTrees(List<Variate> sample) {
		Set<List<Integer>> trees = Sets.newHashSet();
		for (Variate var : sample) {
			List<Integer> tree = Lists.newArrayList(N);
			for (int i = 0; i < N; i++) {
				tree.add(var.tree[i]);
			}
			trees.add(tree);
		}
		return trees.size();
	}

	public int numPerms(List<Variate> sample) {
		Set<List<Integer>> perms = Sets.newHashSet();
		for (Variate var : sample) {
			perms.add(var.perm);
		}
		return perms.size();
	}

	public void checkSupervisedSample(Variate var) {

		List<Set<Integer>> cc = getConnectedComponents(var.tree);
		for (Set<Integer> c : cc) {
			Set<Label> sup_entities = Sets.newHashSet();
			for (int i : c) {
				Instance instance = instances.get(i);
				if (instance.hasVisibleLabel()) {
					Label l = instances.get(i).getGoldLabel();
					sup_entities.add(l);
				}
			}
			if (sup_entities.size() > 1) {
				throw new RuntimeException(
						"two supervised entities with different labels in same cluster");
			}
		}

	}

	public static int getNumEntities(Variate var) {
		int[] tree = var.tree;
		int E = 0;
		for (int y = 0; y < tree.length; y++) {
			if (tree[y] == y) {
				E += 1;
			}
		}
		return E;
	}

	public int getNumEntities() {
		return getNumEntities(state);
	}

	public static class MentionPair {
		int x;
		int y;

		public MentionPair(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int getInput() {
			return x;
		}

		public int getOutput() {
			return y;
		}

		@Override
		public int hashCode() {
			return Objects.hashCode(x, y);
		}

		@Override
		public boolean equals(final Object obj) {
			if (obj instanceof MentionPair) {
				final MentionPair other = (MentionPair) obj;
				return x == other.x && y == other.y;
			} else {
				return false;
			}
		}
	}

	public Map<StringPair, Double> getWeightedPairs(List<Variate> sample) {
		Map<StringPair, Double> pairs = Maps.newHashMap();

		for (Variate var : sample) {
			for (int y = 0; y < N; y++) {
				int x = var.tree[y];
				String xstr = canonical_names.get(x);
				String ystr = canonical_names.get(y);
				if (x == y) {
					xstr = null;
				}
				StringPair pair = new StringPair(xstr, ystr, x, y);
				if (pairs.containsKey(pair)) {
					double wgt = pairs.get(pair);
					pairs.put(pair, wgt + var.weight);
				} else {
					pairs.put(pair, var.weight);
				}
			}
		}

		return pairs;
	}

	protected double computeSampleLogLikelihood(List<Variate> sample, double[] params) {

		assert (sample.size() > 0) : "empty sample";

		double transducer_ll = 0;
		double affinity_ll = 0;

		//double nent = 0.0;
		double subaff = 0.0;
		
		for (Variate var : sample) {

			double[] log_Z = var.Z;

			for (int y = 0; y < N; y++) {
				int x = var.tree[y];
				int x_t = var.topics[x];
				int y_t = var.topics[y];

				// Probability of picking x as a parent of y
				affinity_ll += log_affinity(x, x_t, y, y_t, params, log_Z);

				// Sub-affinity
				if(!prm.root_affinity && x!=y) {
					subaff += dot(x,y,var.topics[x],var.topics[y],prm.phi)-var.Z[y];
				}
				
				// Probability of copying
				transducer_ll += W_mutate.get(x, y);
			}

			//nent += getNumEntities(var);
		}
		
		assert(!Double.isInfinite(affinity_ll)) : "divergent affinity";
		assert(!Double.isInfinite(subaff)) : "divergent subaff";

		
		affinity_ll /= sample.size();
		transducer_ll /= sample.size();


		// Gaussian prior if we're using an affinity model
		double reg = 0.0;
		for (int k = 0; k < params.length; k++) {
			reg += Math.pow(params[k] - prm.phi_mean[k], 2)
					/ (2.0 * prm.phi_var[k] * prm.phi_var[k]);
		}

		double ret = transducer_ll + affinity_ll - reg;

		log.debug("Score = " + ret + " : Transducer LL = " + transducer_ll
				+ " Affinity LL = " + affinity_ll + " SubAffinity= " + subaff + " L2 = " + reg);

		this.em_indices.add(this.em_indices.size());

		if (Double.isNaN(ret) || Double.isInfinite(ret)) {
			throw new RuntimeException("log-likelihood seems off: " + ret);
		}

		return ret;
	}

	public double[] listToArray(List<Double> list) {
		double[] ret = new double[list.size()];
		for (int i = 0; i < list.size(); i++) {
			ret[i] = list.get(i).doubleValue();
		}
		return ret;
	}

	// Averaged over all samples
	protected double computeSampleAffinityLogLikelihood(List<Variate> sample, double[] params) {

		assert (prm.context_model != ContextModel.NONE) : "affinity ll called when context model = none";
		assert (sample.size() > 0) : "empty sample";

		double ll = 0.0;

		for (Variate var : sample) {
			for (int y = 0; y < N; y++) {
				int x = var.tree[y];
				int x_t = var.topics[x];
				int y_t = var.topics[y];

				double log_a = log_affinity(x, x_t, y, y_t, params, var.Z);

				ll += log_a;
			}
		}

		// Gaussian prior
		double reg = 0.0;
		for (int k = 0; k < params.length; k++) {
			reg += Math.pow(params[k] - prm.phi_mean[k], 2)
					/ (2.0 * prm.phi_var[k] * prm.phi_var[k]);
		}

		ll /= sample.size();
		log.debug("Sample affinity ll = " + ll + ", reg = " + reg);
		double ret = ll - reg;

		assert (!Double.isNaN(ret)) : "log-likelihood not a number!";

		return ret;
	}

	// Increment marginal counts
	protected void countSample(int[] s) {

		// Collect edge counts
		for (int y = 0; y < s.length; y++) {
			int x = s[y];

			// Increment edge counts
			double m = M.get(x, y);
			M.set(x, y, m + 1.0);
		}

	}

	protected void normalizeEdgeCounts(int niter) {

		assert (niter > 0) : "trying to divide by 0 during normalization";

		log.debug("Normalizing edge counts: divide by " + niter);

		// Normalize the counts to get marginal edge probabilities
		for (int j = 0; j < N; j++) {
			for (int i : parents(j)) {
				if (i == j) {
					double m = M.get(j, j);
					M.set(j, j, m / niter);
				} else {
					double m = M.get(i, j);
					M.set(i, j, m / niter);
				}
			}
		}
	}

	// NOTE: This is checking a necessary but not sufficient condition
	public static boolean isSpanningTree(int[] s) {

		if (s.length == 0) {
			return true;
		}

		if (s.length == 1) {
			return true;
		}

		// For every vertex, check it has a path to the root
		for (int i = 0; i < s.length; i++) {
			int curr = i;
			int[] visited = new int[s.length];
			do {
				int parent = s[curr];
				if (visited[parent] == 1) { // check for cycles
					return false;
				}
				visited[curr] = 1;
				if (parent == curr) { // have reached the root
					break;
				}
				curr = parent;
			} while (true);
		}

		return true;
	}

	public static List<Set<Integer>> getConnectedComponents(int[] phylogeny) {

		if (!isSpanningTree(phylogeny)) {
			throw new RuntimeException("given graph is not a spanning tree!");
		}

		List<Set<Integer>> cc = Lists.newArrayList();

		Map<Integer, Integer> cc_assignments = Maps.newHashMap();
		int num_cc = 0;

		// Assign each vertex to a connected component
		for (int v = 0; v < phylogeny.length; v++) {
			if (cc_assignments.containsKey(v))
				continue;

			Set<Integer> ancestors = Sets.newHashSet();
			int j = v;

			// Walk up the tree until we reach the root
			ancestors.add(j);
			int hit_assigned = -1;

			while (phylogeny[j] != j) { // while the parent isn't ROOT
				j = phylogeny[j];
				if (cc_assignments.containsKey(j)) {
					hit_assigned = cc_assignments.get(j);
					break;
				}
				ancestors.add(j);
			}

			// If we hit the root without meeting any assigned
			// vertices, assign the current vertex and every vertex
			// encountered along the way to a new connected component.
			if (hit_assigned < 0) {
				for (int k : ancestors) {
					cc_assignments.put(k, num_cc);
				}
				num_cc++;
			} else {
				for (int k : ancestors) {
					cc_assignments.put(k, hit_assigned);
				}
			}
		}

		assert cc_assignments.keySet().size() == phylogeny.length : "Some vertices not assigned to components!";

		for (int i = 0; i < num_cc; i++) {
			Set<Integer> component = Sets.newHashSet();
			for (int v = 0; v < phylogeny.length; v++) {
				if (cc_assignments.get(v) == i) {
					component.add(v);
				}
			}
			cc.add(component);
		}

		return cc;
	}
	
	public int logRandomParentPragma(int v, Set<Integer> v_parents, Variate var, double [] phi, int [] entities, int nent) {
		int n_parents = v_parents.size();

		double[] ds = new double[n_parents];
		int[] is = new int[n_parents];

		// Index (position in mention ordering) of mention v 
		int i = var.perm.get(v); 

		// Calculate denominator shared between entities
		Map<Integer,List<Double>> numer_terms_by_entity = Maps.newHashMap();
		Map<Integer,Double> log_numer_by_entity = Maps.newHashMap();
		List<Double> denom_terms = Lists.newArrayList(); // same for all entities
		for(int parent : var.ord.subList(0, i+1)) {
			double log_aff = log_affinity(parent, var.topics[parent], v, var.topics[v], phi, var.Z);
			
			// Compute w(parent, v)
			double w = W_mutate.get(parent, v) + log_aff;
			
			// Scale so that tuning gamma generalizes
			if(parent != v) {
				w -= Math.log(i);
			}
			
			denom_terms.add(w);

			int e = (parent == v) ? nent : entities[parent];
			if(DEBUG)log.info("parent="+parent+" e="+e);

			if(numer_terms_by_entity.containsKey(e)) {
				numer_terms_by_entity.get(e).add(w);
			} else {
				List<Double> list = Lists.newArrayList();
				list.add(w);
				numer_terms_by_entity.put(e, list);
			}
		}

		List<Double> summands = Lists.newArrayList();
		for(int e : numer_terms_by_entity.keySet()) {
			double lsum = logSumExp(numer_terms_by_entity.get(e));
			summands.add(lsum);
			log_numer_by_entity.put(e, lsum);
		}

		double log_denom   = logSumExp(summands);
		
		this.mean_pragma_log_root += (log_numer_by_entity.get(nent) - log_denom);
		
		i = 0;
		for (int parent : v_parents) {
			int e = (parent == v) ? nent : entities[parent];
			double log_aff = log_affinity(parent, var.topics[parent], v, var.topics[v], phi, var.Z);
			double log_pragma = log_numer_by_entity.get(e) - log_denom;
			assert(log_pragma <= 0.0) : "not a probability! = " + log_pragma;
			log_pragma *= prm.gamma;
			ds[i] = W_mutate.get(parent, v) + log_aff + log_pragma;
			is[i] = parent;
			i++;
		}

		// Pick a new parent
		int p = IndexSampler.logSampleIndex(ds);

		return is[p]; // lookup the parent index
	}
	
	public int logRandomParent(int v, Set<Integer> v_parents, int[] topic, double[] param, double[] log_Z) {

		int n_parents = v_parents.size();

		double[] ds = new double[n_parents];
		int[] is = new int[n_parents];

		int i = 0;
		for (int parent : v_parents) {
			double log_aff = log_affinity(parent, topic[parent], v, topic[v], param, log_Z);
			ds[i] = W_mutate.get(parent, v) + log_aff;
			is[i] = parent;
			i++;
		}

		// Pick a new parent
		int p = IndexSampler.logSampleIndex(ds);
		return is[p]; // lookup the parent index
	}
	
	// Sample tree | ordering, topics
	public void samplePhylogeny(boolean pragmatics) {
		Variate prop = new Variate(state);

		int nent = 0;
		int [] entities = new int[N];
		Map<String, Integer> namefreq = Maps.newHashMap();
		
		Set<Integer> hs = Sets.newHashSet();
		for (int y : state.ord) {
			hs.add(y);
			Set<Integer> ps = Sets.intersection(hs, parents(y)); // pruning
			if(pragmatics) {
				prop.tree[y] = logRandomParentPragma(y, ps, prop, prm.phi, entities, nent);				
				
				// Starting a new entity
				if(prop.tree[y] == y) {
					entities[y] = nent++;
				} else {
					int x = prop.tree[y];
					entities[y] = entities[x];
				}
				
				// Count the name
				String name = canonical_names.get(y);
				if(namefreq.containsKey(name)) {
					int c = namefreq.get(name);
					namefreq.put(name, c+1);
				} else {
					namefreq.put(name, 1);
				}
			} else {
				prop.tree[y] = logRandomParent(y, ps, prop.topics, prm.phi, prop.Z);
			}
		}
		
		if (!isSpanningTree(prop.tree)) {
			throw new RuntimeException();
		}

		accept(prop); // always accepted
	}

	protected void accept(Variate var) {
		state = var;
	}

	protected int sampleMentionTopic(int v) {
		if (prm.context_model == ContextModel.NONE) {
			return 0;
		}
		double[] topic_dist = prm.context_topic_dists[v];
		return IndexSampler.sampleIndex(topic_dist);
	}

	// Sample topics for all mentions from their context topic distribution
	// NOTE: this ignores interactions between topics
	protected int[] sampleMentionTopics() {
		int[] ts = new int[N];
		for (int v = 0; v < N; v++) {
			ts[v] = sampleMentionTopic(v);
		}
		return ts;
	}
	
	public void logContextDists() {
		for(int i=0; i<N; i++) {
			log.info("Unary context distribution (" + i + "):");
			for(int k=0; k<prm.NTOPICS; k++) {
				log.info(k+"="+prm.context_topic_dists[i][k]);
			}
		}
	}

	public FactorGraph blockFactorGraphFromTree(int[] curr_p,
			double[][] context_dists, double[] params, Set<Integer> block) {
		FactorGraph fg = new FactorGraph();

		int NTOPICS = context_dists[0].length;

		// Create a variable for the topic of each mention
		HashMap<Integer, Var> topics = Maps.newHashMap();

		// for(int i=0; i<curr_p.length; i++) {
		for (int i : block) {
			topics.put(i, new Var(VarType.LATENT, NTOPICS, Integer.toString(i),
					null));
		}

		// Add unary factors
		for (int i : block) {
			ExplicitFactor unary = new ExplicitFactor(new VarSet(topics.get(i)));

			// Set the distribution according to the document-specific topic
			// distribution
			for (int z = 0; z < NTOPICS; z++) {
				unary.setValue(z, Math.log(context_dists[i][z]));
			}

			fg.addFactor((Factor) unary);
		}

		// Add binary factors
		for (int child : block) {
			int parent = curr_p[child];
			if (parent != child) {
				VarSet vars = new VarSet(topics.get(child), topics.get(parent));
				ExplicitFactor binary = new ExplicitFactor(vars);

				// Iterate over all the configurations
				for (int c = 0; c < vars.calcNumConfigs(); c++) {
					VarConfig config = vars.getVarConfig(c);

					int child_topic = config.getState(topics.get(child));
					int parent_topic = config.getState(topics.get(parent));

					binary.setValue(c, this.dot(parent, child, parent_topic,
							child_topic, params));
				}

				fg.addFactor((Factor) binary);
			}
		}

		return fg;
	}

	// Uses log-probabilities
	public FactorGraph factorGraphFromTree(int[] curr_p, double[][] context_dists, double[] params) {
		FactorGraph fg = new FactorGraph();
		
		int NTOPICS = context_dists[0].length;

		// Create a variable for the topic of each mention
		List<Var> topics = new ArrayList<Var>();
		for (int i = 0; i < curr_p.length; i++) {
			topics.add(new Var(VarType.LATENT, NTOPICS, Integer.toString(i), null));
		}

		// Add unary factors
		for (int i = 0; i < curr_p.length; i++) {
			ExplicitFactor unary = new ExplicitFactor(new VarSet(topics.get(i)));
			
			// Set the distribution according to the document-specific topic
			// distribution
			for (int z = 0; z < NTOPICS; z++) {
				unary.setValue(z, Math.log(context_dists[i][z]));
			}
			
			fg.addFactor(unary);
		}

		// Add binary factors
		for (int child = 0; child < curr_p.length; child++) {
			int parent = curr_p[child];
			if (parent != child) {
				VarSet vars = new VarSet(topics.get(child), topics.get(parent));
				ExplicitFactor binary = new ExplicitFactor(vars);

				// Iterate over all the configurations
				for (int c = 0; c < vars.calcNumConfigs(); c++) {
					VarConfig config = vars.getVarConfig(c);

					int child_topic = config.getState(topics.get(child));
					int parent_topic = config.getState(topics.get(parent));

					binary.setValue(c, this.dot(parent, child, parent_topic,
							child_topic, params));
				}

				fg.addFactor((Factor) binary);
			}
		}

		return fg;
	}

	public int[] jointBlockTopicProposal(int[] curr_p, Set<Integer> block,
			int[] curr_topic, double[] params) {

		// 1. Initialize the graphical model
		FactorGraph fg = blockFactorGraphFromTree(curr_p,
				prm.context_topic_dists, params, block);

		// 2. Compute the beliefs at each node
		BeliefPropagationPrm prm = new BeliefPropagationPrm();
		prm.maxIterations = 1;
		prm.logDomain = true;
		prm.schedule = BpScheduleType.TREE_LIKE;
		prm.updateOrder = BpUpdateOrder.SEQUENTIAL;
		// Don't normalize the messages, so that the partition function is the
		// same as in the brute force approach.
		prm.normalizeMessages = false;

		// Run BP
		BeliefPropagation bp = new BeliefPropagation(fg, prm);
		bp.run();
		
		// 3. Sample from the beliefs at each node
		int[] ret = new int[curr_p.length];
		System.arraycopy(curr_topic, 0, ret, 0, curr_p.length);

		for (Var var : fg.getVars()) {
			int v = Integer.parseInt(var.getName());

			if (!block.contains(v)) {
				throw new RuntimeException("bad vertex index");
			}

			DenseFactor marg = bp.getMarginals(var);

			// The marginals will be in log space
			ret[v] = IndexSampler.logSampleIndex(marg.getValues());
		}

		return ret;
	}

	public int[] jointTopicProposal(int[] curr_p, double[][] unary_factors, double[] params) {

		// 1. Initialize the graphical model
		FactorGraph fg = factorGraphFromTree(curr_p, unary_factors, params);

		// 2. Compute the beliefs at each node
		BeliefPropagationPrm prm = new BeliefPropagationPrm();
		prm.maxIterations = 1;
		prm.logDomain = true;
		prm.schedule = BpScheduleType.TREE_LIKE;
		prm.updateOrder = BpUpdateOrder.SEQUENTIAL;
		// Don't normalize the messages, so that the partition function is the
		// same as in the brute force approach.
		prm.normalizeMessages = false;

		// Run BP
		BeliefPropagation bp = new BeliefPropagation(fg, prm);
		bp.run();

		// 3. Sample from the beliefs at each node
		int[] ret = new int[curr_p.length];
		int v = 0;
		for (Var var : fg.getVars()) {
			DenseFactor marg = bp.getMarginals(var);

			// The marginals will be in log space
			ret[v++] = IndexSampler.logSampleIndex(marg.getValues());
		}

		return ret;
	}

	public static double logAdd(double logX, double logY) {
		if (logY > logX) {
			double temp = logX;
			logX = logY;
			logY = temp;
		}
		if (logX == Double.NEGATIVE_INFINITY) {
			return logX;
		}
		double negDiff = logY - logX;
		if (negDiff < -20) {
			return logX;
		}
		return logX + Math.log(1.0 + Math.exp(negDiff));
	}
	
	public static double logSubstract(double logX, double logY) {
		if(logX <= logY) {
			throw new RuntimeException("log of negative number");
		}
		if(logY == Double.NEGATIVE_INFINITY) {
		    return logX;
		}
		return logX + Math.log1p(-Math.exp(logY-logX));
	}

	protected double log_affinity(int x, int x_topic, int y, int y_topic, double[] param, double[] log_Z) {
		if(!prm.root_affinity) {
			if(x==y) {
				return prm.root_log_weight; // represents a probability if prm.root_affinity == true
			} else {
				return dot(x, y, x_topic, y_topic, prm.phi) - log_Z[y] + prm.log_one_minus_root_prob;
			}
		}
		return dot(x, y, x_topic, y_topic, param) - log_Z[y];
	}

	protected double logSumExp(double x1, double x2) {
		double m = Math.max(x1, x2);
		return m + Math.log(Math.exp(x1 - m) + Math.exp(x2 - m));
	}

	protected double logSumExp(List<Double> nums) {
		if (nums.size() == 0) {
			return Double.NEGATIVE_INFINITY;
		}
		double max_exp = nums.get(0);
		double sum = 0;
		int i;
		for (i = 1; i < nums.size(); i++) {
			if (nums.get(i) > max_exp) {
				max_exp = nums.get(i);
			}
		}

		for (i = 0; i < nums.size(); i++) {
			sum += Math.exp(nums.get(i) - max_exp);
		}
		return Math.log(sum) + max_exp;
	}

	public static double binaryLogAdd(double x, double y) {
		if (x == Double.NEGATIVE_INFINITY)
			return y;
		if (y == Double.NEGATIVE_INFINITY)
			return x;
		return Math.max(x, y) + Math.log1p(Math.exp(-Math.abs(x - y)));
	}

	public void fastLogZs(Variate var, double[] params) {
		int[] last_mention_by_topic = new int[prm.NTOPICS];
		Arrays.fill(last_mention_by_topic, -1);
		for (int i = 0; i < var.ord.size(); i++) {
			int y = var.ord.get(i);
			int t = var.topics[y];
			List<Double> sums = new ArrayList<Double>();
			if (last_mention_by_topic[t] >= 0) {
				int z = last_mention_by_topic[t];
				int zi = var.perm.get(z);
				// All subsequent mentions since we last computed this Z
				for (int x : var.ord.subList(zi, i)) { // exclude self
					sums.add(dot(x, y, var.topics[x], var.topics[y], params));
				}
				var.Z[y] = binaryLogAdd(var.Z[z], logSumExp(sums));
			} else {
				for (int x : var.ord.subList(0, i + 1)) {
					if(x == y && !prm.root_affinity) {
						continue;
					}
					sums.add(dot(x, y, var.topics[x], var.topics[y], params));
				}
				var.Z[y] = logSumExp(sums);
			}
			last_mention_by_topic[t] = y;
		}
	}
	
	public void slowLogZs(Variate var, double [] params) {
		for(int y=0; y<N; y++) {
			int i = var.perm.get(y);
			List<Double> sums = Lists.newArrayList();
			for(int x : var.ord.subList(0, i+1)) {
				if(x == y && !prm.root_affinity) {
					continue;
				}
				sums.add(dot(x, y, var.topics[x], var.topics[y], params));
			}
			var.Z[y] = logSumExp(sums);
		}
	}
	
	public Variate getCurrentState() {
		return state;
	}

	protected double logPaffinity(Variate var, double[] phi) {
		double log_p = 0;
		for (int y = 0; y < N; y++) {
			int x = var.tree[y];
			double log_aff = log_affinity(x, var.topics[x], y, var.topics[y], phi, var.Z);
			log_p += log_aff;
		}
		return log_p;
	}

	protected double logBlockQAffinity(int[] p, int[] topic, double[] param,
			Set<Integer> block) {
		double logq = 0;

		for (int y : block) {
			int x = p[y];

			// Unary factor
			logq += Math.log(prm.context_topic_dists[y][topic[y]]);

			// Binary factor
			logq += this.dot(x, y, topic[x], topic[y], param);
		}

		return logq;
	}

	protected double logQaffinity(Variate var, double[] param) {
		double log_q = 0d;

		for (int y = 0; y < this.N; y++) {
			int x = var.tree[y];

			// Unary factor
			log_q += Math.log(prm.context_topic_dists[y][var.topics[y]]);

			// Binary factor
			log_q += dot(x, y, var.topics[x], var.topics[y], param);
		}

		return log_q;
	}

	public void metropolisHastingsOrdering() {
		Variate prop = new Variate(state);

		// Sample a proposal
		prop.ord = sampleOrderingGivenTree(prop.tree);
		prop.perm = permFromOrdering(prop.ord);

		// Compute corresponding normalization terms
		fastLogZs(prop, prm.phi);

		// Accept or reject the proposal
		double lp_new = logPaffinity(prop, prm.phi);
		double lp_old = logPaffinity(state, prm.phi);
		double log_p_ratio = lp_new - lp_old;
		double log_u = Math.log(RandomNumberGenerator.nextDouble());
				
		if (log_u < log_p_ratio) {
			accept(prop);
			nperm_samples_accepted += 1;
		} else {
			// Ordering rejected
		}
		nperm_samples_total += 1;
	}

	public void blockMetropolisHastingsTopics() {
		Variate prop = new Variate(state);

		// The topics are sampled as a block for each connected component
		List<Set<Integer>> cc = getConnectedComponents(state.tree);

		for (Set<Integer> block : cc) {
			prop.topics = jointBlockTopicProposal(prop.tree, block,
					prop.topics, prm.phi);

			boolean changed = false;
			for (int y : block) {
				if (prop.topics[y] != state.topics[y]) {
					changed = true;
					break;
				}
			}

			if (changed) {
				fastLogZs(prop, prm.phi);
			}

			double new_log_P = logPaffinity(prop, prm.phi);
			double old_log_P = logPaffinity(state, prm.phi);

			double new_log_Q = logBlockQAffinity(prop.tree, prop.topics,
					prm.phi, block);
			double old_log_Q = logBlockQAffinity(state.tree, state.topics,
					prm.phi, block);

			double log_ratio = new_log_P - old_log_P + old_log_Q - new_log_Q;
			double log_u = Math.log(RandomNumberGenerator.nextDouble());

			if (log_u < log_ratio) {
				accept(prop);
				this.ntopic_samples_accepted += 1;
			}
			this.ntopic_samples_total += 1;
		}
	}

	public void metropolisHastingsTopics() {
		// Proposed state
		Variate prop = new Variate(state);

		// Sample a proposal
		prop.topics = jointTopicProposal(prop.tree, prm.context_topic_dists, prm.phi);
		fastLogZs(prop, prm.phi);

		// Compute the acceptance ratio

		// Sample accepted
		if (accept_new_topic(prop)) {
			accept(prop);
			this.ntopic_samples_accepted += 1;
		}

		this.ntopic_samples_total += 1;
	}

	boolean accept_new_topic(Variate prop) {
		// P-ratio
		double lp_new = logPaffinity(prop, prm.phi);
		double lp_old = logPaffinity(state, prm.phi);

		// Q-ratio
		double lq_new = logQaffinity(prop, prm.phi);
		double lq_old = logQaffinity(state, prm.phi);

		// Decide if we're keeping it
		double log_ratio = lp_new - lp_old + lq_old - lq_new;
		double log_u = Math.log(RandomNumberGenerator.nextDouble());
		
		return log_u < log_ratio;
	}

	public int[] getCurrentTopic() {
		return state.topics;
	}

	public double[] getCurrentZ() {
		return state.Z;
	}

	protected void gibbsSampleTopics() {
		for (int y=0; y<N; y++) {
			int new_topic = lda.sampleEntityTopicFromContextDist(y);
			instances.get(y).setHypLabel(new StringLabel(Integer.toString(new_topic)));
			state.topics[y] = new_topic;
		}
	}

	protected void printEvalScore() {
		Evaluator b3Eval = new b3Evaluator();
		b3Eval.evaluate(instances);
		b3Eval.display(new PrintWriter(System.err, true));
	}

	public void topicModelOptimize(boolean opt) {
		prm.lda_opt = opt;
	}

	protected void initTopicModel() {
		if (prm.context_model == ContextModel.NONE || prm.dont_train_topic_model) {
			return;
		}

		// Create the document/context array
		docs = new String[corpus.docs().size()][];

		int index = 0;
		for (Document d : corpus.docs()) {
			String body = Joiner.on(" ").join(d.getContext());
			docs[index] = preprocessRawDoc(body);
			index++;
		}

		// Print stats on the contexts
		printContextStats();

		lda = new LDA(prm.NTOPICS, prm.alpha_sum, prm.tt_beta);		
		
		// lda.setSymmetricAlpha(true); // avoid xxENTITYxx being put in the
		// stopword topic
		lda.setNumIterations(1000);
		lda.setBurninPeriod(0);

		// If we're doing hyper-param optimization
		if (prm.lda_opt == false) {
			lda.setOptimizeInterval(0);
		}

		// Set the number of threads
		lda.setNumThreads(this.num_thread);

		// Add the mention contexts
		lda.addRawDocs(docs, vocabulary);

		// Train the topic model
		try {
			lda.estimate();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Print the top words from each topic
		lda.printTopWords(new PrintStream(System.out), 10, false);

		log.debug("Estimating context-specific topic distributions...");
		prm.context_topic_dists = new double[N][];
		for (int v = 0; v < N; v++) {
			prm.context_topic_dists[v] = lda.sampleDocumentTopicDist(v);

			if (this.DEBUG) {
				System.err.print("v=" + v);
				for (int t = 0; t < prm.NTOPICS; t++) {
					System.err.print(" " + prm.context_topic_dists[v][t]);
				}
			}
		}

		// Assign initial topics to each instance
		Set<Integer> topics = Sets.newHashSet();
		for (int v = 0; v < N; v++) {
			int new_topic;
			if (!this.fix_initial_state) {
				new_topic = sampleMentionTopic(v);
				state.topics[v] = new_topic;
				//log.debug("initial topic " + v + " = " + new_topic);
			} else {
				new_topic = state.topics[v];
			}
			instances.get(v).setHypLabel(
					new StringLabel(Integer.toString(new_topic)));
			topics.add(new_topic);
		}

		log.debug(topics.size() + " distinct topics initially");

		log.debug("If we take those topics as cluster ID's, get we these results:");

		printEvalScore();
	}

	protected void setEdgeWeights() {

		long startTime = System.currentTimeMillis();

		if (W_mutate == null) {
			W_mutate = new SimpleMatrix(N, N);
		}

		double[] log_probs = edit_model.logp();

		int index = 0;
		for (int v = 0; v < N; v++) {
			for (int u : parents(v)) {
				double w;
				if (u != v) { // ======== MUTATE EDGE: u -> v
					String parent = canonical_names.get(u);
					String child = canonical_names.get(v);

					w = prm.log_mu + log_probs[index++]; // EDIT

					if (parent.equals(child)) {
						w += Math.log(1.0 - prm.mu); // COPY
					}
				} else { // ======== GENERATE EDGE: START -> v
					// Set the edge weight
					w = log_probs[index++];
				}

				assert (!Double.isNaN(w));
				assert (!Double.isInfinite(w)) : canonical_names.get(u)
						+ " -> " + canonical_names.get(v);

				W_mutate.set(u, v, w);
			}
		}

		long currTime = System.currentTimeMillis();
		long millis = currTime - startTime;

		String time = String.format(
				"%d min, %d sec",
				TimeUnit.MILLISECONDS.toMinutes(millis),
				TimeUnit.MILLISECONDS.toSeconds(millis)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
								.toMinutes(millis)));

		log.debug("Time to set edge weights: " + time);
	}

	public static List<Integer> sampleUniformInterleaving(List<Integer> t1,
			List<Integer> t2) {
		List<Integer> interleaving = new ArrayList<Integer>();
		int len = t1.size() + t2.size();
		List<Integer> t1_copy = new ArrayList<Integer>(t1);
		List<Integer> t2_copy = new ArrayList<Integer>(t2);
		for (int i = 0; i < len; i++) {
			int r = RandomNumberGenerator.nextInt(t1_copy.size()
					+ t2_copy.size());
			if (r < t1_copy.size()) {
				int v = t1_copy.remove(0);
				interleaving.add(v);
			} else {
				int v = t2_copy.remove(0);
				interleaving.add(v);
			}
		}
		return interleaving;
	}

	protected static List<Integer> sampleInterleaving(int u, List<List<Integer>> children) {

		// Base case: return [u]
		if (children.get(u).size() == 0) {
			List<Integer> ret = new ArrayList<Integer>();
			ret.add(u);
			return ret;
		}

		// Accumulator
		List<Integer> accum = sampleInterleaving(children.get(u).get(0),
				children);

		List<Integer> remainder = new ArrayList<Integer>(children.get(u));
		remainder.remove(0);

		for (int v : remainder) {
			List<Integer> c_list = sampleInterleaving(v, children);
			accum = sampleUniformInterleaving(accum, c_list);
		}

		accum.add(0, u);
		return accum;
	}

	// This method adds the root at pos 0, so vertices are indexed by
	// v+1 where v is the vertex index.
	public static List<List<Integer>> getChildren(int[] p) {
		// Construct a map from vertex to children(vertex)
		List<List<Integer>> children = new ArrayList<List<Integer>>();
		for (int i = 0; i < p.length + 1; i++) {
			children.add(new ArrayList<Integer>());
		}
		for (int i = 0; i < p.length; i++) {
			int parent = p[i];
			if (parent == i) {
				children.get(0).add(i + 1);
			} else {
				children.get(parent + 1).add(i + 1);
			}
		}
		return children;
	}

	// Sanity checking
	public boolean orderingRespectsTree(Variate var) {

		// No repetitions in perm
		Set<Integer> uniq = new HashSet<Integer>();
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for (int y = 0; y < N; y++) {
			int rank = var.perm.get(y);
			if (uniq.contains(rank)) {
				log.debug("duplicate rank in permutation");
				System.exit(1);
			}
			uniq.add(rank);
			if (rank > max)
				max = rank;
			if (rank < min)
				min = rank;
		}

		boolean bad_edge = false;

		for (int y = 0; y < N; y++) {
			int x = var.tree[y];

			if (var.perm.get(x) > var.perm.get(y) && x != y) {
				log.debug("parent vertex = " + x + ", parent rank = "
						+ var.perm.get(x));
				log.debug("child vertex  = " + y + ", child rank  = "
						+ var.perm.get(y));
				bad_edge = true;
			}
		}

		if (bad_edge) {
			log.debug("Permutation does not respect tree.");
			System.exit(1);
		}

		return true;
	}

	public static List<Integer> sampleOrderingGivenTree(int[] p) {

		if (p.length < 1) {
			throw new RuntimeException("empty tree");
		}

		// Construct a list of children for each vertex
		List<List<Integer>> children = getChildren(p);

		// Sample an ordering uniformly at random
		List<Integer> ordering = sampleInterleaving(0, children);

		if (ordering.size() < 1) {
			throw new RuntimeException("empty interleaving");
		}

		List<Integer> ord = Lists.newArrayList();

		for (int i = 1; i < ordering.size(); i++) {
			int y = ordering.get(i) - 1;
			ord.add(y);
		}

		return ord;
	}

	// Returns a mention mapping
	public static List<Integer> permFromOrdering(List<Integer> ordering) { 
		List<Integer> perm = new ArrayList<Integer>(ordering);
		for (int i = 0; i < ordering.size(); i++) {
			int rank = ordering.get(i);
			perm.set(rank, i);
		}
		return perm;
	}

	public boolean sanityCheckOrdering(Variate var) {

		if (var.ord == null || var.ord.size() == 0) {
			log.debug("null or empty ord");
			return false;
		}

		if (state.tree == null) {
			log.debug("null tree");
			return false;
		}

		//log.debug(var.ord.size());
		//log.debug(var.perm.size());
		//log.debug(N);
		
		// Check if the ordering is correct given the tree
		for (int i = 0; i < N; i++) {
			int y = var.ord.get(i); // mention y at position i
			if (y >= N)
				return false;
			int x = var.tree[y]; // the parent of mention y
			if (x >= N)
				return false;
			for (int j = 0; j < N; j++) {
				if (var.ord.get(j) == x) {
					if (j > i)
						return false;
					break;
				}
			}
		}

		return true;
	}

	public void getInitialState() {
		
		// Special case for a fixed ordering
		if(prm.fixed_ordering) {
			log.info("initializing with fixed ordering...");
			this.state.ord = Lists.newArrayList();
			for(int i=0; i<N; i++) {
				this.state.ord.add(i);
			}
			
			this.state.perm = permFromOrdering(this.state.ord);
			
			if(!sanityCheckOrdering(this.state)) {
				log.error("bad ordering");
				System.exit(1);
			}
			
			fastLogZs(state, prm.phi);
			
			samplePhylogeny(prm.pragmatics);
			
			if (!sanityCheckOrdering(state)) {
				log.debug("problem with ordering");
				throw new RuntimeException();
			}

			// Fix entity assignments
			state.entities = entitiesFromTree(state.tree);
			
			// Sample topics
			if (prm.context_model != ContextModel.NONE) {
				state.topics = jointTopicProposal(state.tree, prm.context_topic_dists, prm.phi);

				if(DEBUG) {
					log.debug("Topics");
					for(int i=0; i<N; i++) {
						log.debug("\t"+state.topics[i]);
					}
				}
			} else {
				// Everything is the same topic
				state.topics = new int[N];
				for (int y = 0; y < N; y++) {
					state.topics[y] = 0;
				}
			}

			// Compute normalizing constants for affinity model
			fastLogZs(state, prm.phi);

			// Print initial state
			//printTree();
			
			log.debug("Init perm log prob: " + logPaffinity(state, prm.phi));
			
			return;
		}
		
		// ------- END FIXED ORDERING SPECIAL CASE
		
		if(prm.init == SamplerInit.RANDOM) {
			
			log.info("Doing random init...");
			
			for(int y=0; y<N; y++) {
				state.tree[y] = RandomNumberGenerator.nextInt(y+1);
			}

		} else if(prm.init == SamplerInit.TARJAN) {

			log.info("Doing Tarjan init...");
			
			double average_gen_log_weight = 0.0;
			int tot_gen = 0;
			double average_mut_log_weight = 0.0;
			int tot_mut = 0;

			List<Node> nodes = new ArrayList<Node>();
			nodes.add(new Node(0)); // ROOT node
			for (int y = 0; y < N; y++) {
				nodes.add(new Node(y + 1));
			}
			AdjacencyList g = new AdjacencyList();

			// No ordering here
			// NOTE: This initialization does not consider context similarity

			nodes.add(new Node(0)); // ROOT node
			for (int y = 0; y < N; y++) {

				for (int x : parents(y)) {

					double m = W_mutate.get(x, y);
					double a;
					if(prm.root_affinity) {
						a = dot(x, y, state.topics[x], state.topics[y], prm.phi);
					} else {
						a = prm.root_log_weight;
					}
					double w = m + a;

					if (x == y) {
						average_gen_log_weight += w;
						tot_gen++;
					} else {
						average_mut_log_weight += w;
						tot_mut++;
					}


					if (x == y) {
						g.addEdge(nodes.get(0), nodes.get(y + 1), w);
					} else {
						g.addEdge(nodes.get(x + 1), nodes.get(y + 1), w);
					}

				}
			}

			log.debug("[INIT] Average mutate weight: " + average_mut_log_weight
					/ tot_mut);
			log.debug("[INIT] Average generate weight: " + average_gen_log_weight
					/ tot_gen);

			int[] s = new int[N];

			if (init.equals("best")) {
				double[][] W = new double[N + 1][];
				for (int i = 0; i < N + 1; i++) {
					W[i] = new double[N + 1];
					Arrays.fill(W[i], Double.NEGATIVE_INFINITY);
				}

				for (Edge e : g.getAllEdges()) {
					int x = e.getSource().getName();
					int y = e.getTarget().getName();
					W[x][y] = e.getWeight();
				}

				CompleteGraph G = new CompleteGraph(W);
				Edmonds eds = new Edmonds();
				int[] invertedMaxBranching = new int[N + 1];
				log.info("Finding max branching...");

				long startTime = System.currentTimeMillis();

				eds.getMaxBranching(G, 0, invertedMaxBranching);

				long currTime = System.currentTimeMillis();
				long millis = currTime - startTime;

				String time = String.format(
						"%d min, %d sec",
						TimeUnit.MILLISECONDS.toMinutes(millis),
						TimeUnit.MILLISECONDS.toSeconds(millis)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
								.toMinutes(millis)));

				log.debug("Time to compute maximum branching: " + time);

				for (int to = 1; to < N + 1; to++) {
					int from = invertedMaxBranching[to];
					int x = from - 1;
					int y = to - 1;
					if (from == 0) {
						s[y] = y;
					} else {
						s[y] = x;
					}
				}

				System.arraycopy(s, 0, state.tree, 0, N);

				
			} else {
				log.debug("unrecognized option, init = " + this.init);
				System.exit(1);
			}

		}
		
		// Do an initial evaluation
		List<Set<Integer>> cc = getConnectedComponents(state.tree);
		assert (cc != null) : "null connected components";
		labelInstances(cc);
		Evaluator b3Eval = new b3Evaluator();
		b3Eval.evaluate(instances);
		StringWriter writer = new StringWriter();
		b3Eval.display(new PrintWriter(writer));
		log.info("[init B3] " + writer.toString());

		if (!isSpanningTree(state.tree))
			throw new RuntimeException();

		log.debug(cc.size() + " initial connected components.");

		// Sample a permutation compatible with this tree
		log.debug("Sampling the initial ordering compatible with the starting phylogeny..");

		state.ord = sampleOrderingGivenTree(state.tree);
		state.perm = permFromOrdering(state.ord);

		if (!sanityCheckOrdering(state)) {
			log.debug("problem with ordering");
			throw new RuntimeException();
		}

		// Fix entity assignments
		state.entities = entitiesFromTree(state.tree);
		
		// Sample topics
		if (prm.context_model != ContextModel.NONE) {
			state.topics = jointTopicProposal(state.tree, prm.context_topic_dists, prm.phi);

			if(DEBUG) {
				log.debug("Topics");
				for(int i=0; i<N; i++) {
					log.debug("\t"+state.topics[i]);
				}
			}
		} else {
			// Everything is the same topic
			state.topics = new int[N];
			for (int y = 0; y < N; y++) {
				state.topics[y] = 0;
			}
		}

		// Compute normalizing constants for affinity model
		fastLogZs(state, prm.phi);

		// Print initial state
		//printTree();
		
		log.debug("Init perm log prob: " + logPaffinity(state, prm.phi));
	}

	public int[] getCurrentTree() {
		return state.tree;
	}

	public static List<Integer> entitiesFromTree(int[] phylogeny) {
		Map<Integer, Integer> cc_assignments = Maps.newHashMap();
		int num_cc = 0;

		// Assign each vertex to a connected component
		for (int v = 0; v < phylogeny.length; v++) {
			if (cc_assignments.containsKey(v))
				continue;

			Set<Integer> ancestors = Sets.newHashSet();
			int j = v;

			// Walk up the tree until we reach the root
			ancestors.add(j);
			int hit_assigned = -1;
			while (phylogeny[j] != j) {
				j = phylogeny[j];
				if (cc_assignments.containsKey(j)) {
					hit_assigned = cc_assignments.get(j);
					break;
				}
				ancestors.add(j);
			}

			// If we hit the root without meeting any assigned
			// vertices, assign the current vertex and every vertex
			// encountered along the way to a new connected component.
			if (hit_assigned < 0) {
				for (int k : ancestors) {
					cc_assignments.put(k, num_cc);
				}
				num_cc++;
			} else {
				for (int k : ancestors) {
					cc_assignments.put(k, hit_assigned);
				}
			}
		}

		List<Integer> entities = new ArrayList<Integer>(phylogeny.length);
		for (int y = 0; y < phylogeny.length; y++) {
			int assignment = cc_assignments.get(y);
			entities.add(assignment);
		}
		return entities;
	}

	protected void printContextStats() {
		Set<String> types = new HashSet<String>();
		int tokens = 0;
		for (int i = 0; i < docs.length; i++) {
			tokens += docs[i].length;
			for (String s : docs[i]) {
				types.add(s);
			}
		}
		log.debug("===== Document stats =====");
		log.debug("Total num docs: " + docs.length);
		log.debug("Num types: " + types.size());
		log.debug("Num tokens: " + tokens);
		log.debug("Average doc length: " + ((double) tokens / docs.length));
	}

	public static String ranksToPerm(List<Integer> ranks) {

		List<Integer> perm = new ArrayList<Integer>();
		for (int i = 1; i <= ranks.size(); i++) {
			// Looking for item j of rank i
			for (int j = 0; j < ranks.size(); j++) {
				if (ranks.get(j) == i) {
					perm.add(j);
				}
			}
		}

		return perm.toString();
	}
	
	public String[] preprocessRawDoc(String raw) {
		List<String> words = new ArrayList<String>();
		for (String word : raw.split("\\s+")) {

			String norm_word = new String(word);

			// Strip punctuation
			norm_word = norm_word.replaceFirst(PUNC_PREFIX, "");
			norm_word = norm_word.replaceFirst(PUNC_SUFFIX, "");

			// Drop anything less than three characters
			if (norm_word.length() < 3) {
				continue;
			}

			if (norm_word.length() != 4 // preserve years, e.g. 1984
					&& isNumeric(word)) { // but drop everything else
				continue;
			}

			// Lower case it
			String lower_norm_word = norm_word.toLowerCase();

			// Remove trailing possessives (e.g. 's)
			lower_norm_word = StringUtils.remove(lower_norm_word, "'s");

			// Add to the word sequence
			words.add(lower_norm_word);
		}

		// Remove stop words
		List<String> without_stopwords = Lists.newArrayList();
		StopWords stopwords = StopWords.getInstance();
		for(String word : words) {
			if(!stopwords.isStopWord(word)) {
				without_stopwords.add(word);
			}
		}

		return without_stopwords.toArray(new String[without_stopwords.size()]);
	}
	
	public void setSamplerInit(SamplerInit i) {
		prm.init = i;
	}
	
	public void setDebug(boolean b) {
		this.DEBUG = b;
	}
	
	public void setNumESamples(int n) {
		prm.NUM_E_SAMPLES = n;
	}

	public void diagnostics(boolean b) {
		this.DIAGNOSTICS = b;
	}

	public void setLogRootWeight(double log_weight) {
		if(!prm.root_affinity) {
			if(log_weight > 0.0) {
				log.error("log weight is a probability if root_affinity == false: lw="+log_weight);
				throw new RuntimeException("bad log_weight");
			}
		}
		
		prm.root_log_weight = log_weight;
		prm.log_one_minus_root_prob = logSubstract(0, log_weight);
		log.debug("root log weight = " + prm.root_log_weight);
		log.debug("log complement(root log weight)=" + prm.log_one_minus_root_prob);
	}

	public void setNumDecodeSamples(int n2) {
		prm.NUM_DECODE_SAMPLES = n2;
	}

	public void setAdaGradEta(double eta) {
		prm.adaGradEta = eta;
	}
	
	public void setPragmaGamma(double gamma) {
		prm.gamma = gamma;
	}
	
	// Use the pragmatic model
	public void usePragmaticModel(boolean b) {
		log.info("Pragmatic model = " + b);
		prm.pragmatics = b;
	}

	public int getNumMentions() {
		return N;
	}

	public void logCurrentState() {
		log.debug("Tree:");
		for(int i=0; i<N; i++) {
			log.debug("\t"+state.tree[i]);
		}
		log.debug("Topics:");
		for(int i=0; i<N; i++) {
			log.debug("\t"+state.topics[i]);
		}
		log.debug("Ordering / Permutation:");
		for(int i=0; i<N; i++) {
			log.debug("\t"+state.ord.get(i)+ " / "+state.perm.get(i));
		}
	}

	public void setContextModel(String model_str) {
		System.err.println("Context mutation model: " + model_str);
		if (model_str.equals("none")) {
			prm.context_model = ContextModel.NONE;
			log.debug("No topics!");
		} else if (model_str.equals("fixed_topics")) {
			prm.context_model = ContextModel.FIXED_TOPICS;
			log.debug("Fixed topics -- no resampling!");
		} else if (model_str.equals("sampled_topics")) {
			prm.context_model = ContextModel.SAMPLED_TOPICS;
			log.debug("Sampled topics!");
		} else if (model_str.equals("block_sampled_topics")) {
			prm.context_model = ContextModel.BLOCK_SAMPLED_TOPICS;
			log.debug("Block sampled topics!");
		}
	}

	protected int[] ccToInts(List<Set<Integer>> cc) {
		int l = 0;
		int[] ret = new int[N];
		for (Set<Integer> c : cc) {
			for (int m : c) {
				ret[m] = l;
			}
			l += 1;
		}
		return ret;
	}

	public void labelInstances(List<Set<Integer>> cc) {
		int cluster = 0;
		for (Set<Integer> component : cc) {
			for (int v : component) {
				instances.get(v).setHypLabel(
						new StringLabel(Integer.toString(cluster)));
			}
			cluster++;
		}
	}

	protected int numComponents(int[] p) {
		int cc = 0;
		for (int i = 0; i < p.length; i++) {
			if (p[i] == i) {
				cc++;
			}
		}
		return cc;
	}
	
	public void assignLabelsToInstances(int[] labels) {

		if (labels == null) {
			throw new RuntimeException("null labels");
		}

		// Assign labels to instances
		for (int i = 0; i < N; i++) {
			int l = labels[i];
			instances.get(i).setHypLabel(new StringLabel(Integer.toString(l)));
		}
	}
	
	public void setContextModel(ContextModel cm) {
		prm.context_model = cm;
	}

	public void setEMIter(int iter) {
		prm.em_iter = iter;
	}
	
	public void fixedOrdering(boolean b) {
		prm.fixed_ordering = b;
	}
	
	public void setPruning(boolean pruning) {
		prm.trigram_pruning = pruning;
	}
	
	public void setMBR(boolean mbr) {
		prm.do_mbr_decode = mbr;
	}

	public void setMutationModel(MutationModel model) {
		prm.mutation_model = model;
	}

	public void setMutationModelLR(double learning_rate_2) {
		ConditionalStringModel.STEP_SIZE = learning_rate_2;
	}
}
