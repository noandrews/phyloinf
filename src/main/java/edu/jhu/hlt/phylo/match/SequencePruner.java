/***
 * INPUT: collection of strings
 * OUTPUT: a map from strings to similar strings, where "similar"
 *         is defined as sharing at least one trigram
 *
 * There is support for adding additional constraints:
 *   - cluster constraints;
 *   - length constraints
 *
 *
 * WARNING: Code that uses this currently assumes that the like(x) function
 *          always returns a set with (at least) x itself in it.
 *
 * @author Nicholas Andrews
 */

package edu.jhu.hlt.phylo.match;

import com.google.common.collect.*;
import java.util.*;

import java.text.Normalizer;

public class SequencePruner {

    public static final String START_STR = "___START___";
   
    Map<String,Set<String>> allowed        = null;  // every pair in a cluster is allowed
    Map<String,Set<String>> trigram_index  = null;  // NGRAM => STRING
    Map<String,Set<String>> fourgram_index = null;  // NGRAM => STRING

    Map<String,Set<String>> children       = null;
    Map<String,Set<String>> parents        = null;

    Set<String> types                      = null;  // all observed types
    int ntypes                             = 0;
    int length_constr                      = -1;
    boolean just_trigrams                  = true;  // dont use 4grams for longer strings
    Set<String> canonical_names            = null;

    Map<String,Set<String>> flattree       = null;
    Set<String> supervised                 = null;
    Set<String> unsupervised               = null;

    List<Set<String>> clusters             = null;

    boolean no_unsupervised_to_supervised_edges = false;

    public SequencePruner() {
        trigram_index  = Maps.newHashMap();
        fourgram_index = Maps.newHashMap();
        types          = Sets.newHashSet();
    }

    public SequencePruner(int length_constr) {
        trigram_index  = Maps.newHashMap();
        fourgram_index = Maps.newHashMap();
        types          = Sets.newHashSet();
        this.length_constr = length_constr;
    }

    public List<Set<String>> getClusters() {
        return clusters;
    }

    public void setNoUnsupToSup(boolean b) {
        this.no_unsupervised_to_supervised_edges = b;
    }

    public static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        return s;
    }

    public void setMaxLengthDelta(int delta) {
        this.length_constr = delta;
    }

    public void setFlatTreeConstraints(Map<String,Set<String>> flattree) {
        this.flattree = flattree;
        this.supervised = new HashSet<String>(); // supervised (not canonical)
        for(String key : flattree.keySet()) {
            Set<String> cluster = flattree.get(key);
            supervised.addAll(cluster);

            // DEBUG XXXX
            if(key.equals("Bernanke") || cluster.contains("Bernanke")) {
                System.err.println("Key = " + key);
                System.err.println("Cluster:");
                for(String cstring : cluster) {
                    System.err.println("\t" + cstring);
                }
            }

            //            supervised.add(key);
        }
        this.unsupervised = new HashSet<String>();
        for(String s : types) {
            if(!supervised.contains(s) && !flattree.keySet().contains(s)) {
                this.unsupervised.add(s);
            }
        }
        System.err.println("unsupervised size = " + unsupervised.size());
        System.err.println("supervised size = " + supervised.size());
    }

    public void setCanonicalNames(Set<String> canonical_names) {
        System.err.println("Setting canonical names...");
        this.canonical_names = canonical_names;
    }

    // NOTE: This is applied after the ngram constraint, so anything
    //       disallowed according to ngrams takes priority.
    //
    // NOTE: Vertices not specified in this list of allowed transitions
    //       will be assumed to be constrained by the ngram constraint
    //       rather than have no allowed constraints.
    //
    // NOTE: We still want transitions *to* vertices not in a known
    //       cluster to be allowed.
    public void constrainTransitions(List<Set<String>> clusters) {

        this.clusters = clusters;

        assert(trigram_index.size() > 0) : "Call this after make_index(...)";

        if(!just_trigrams) {
            assert(fourgram_index.size() > 0) : "Call this after make_index(...)";
        }

        System.err.println(clusters.size() + " cluster constraints...");

        Set<String> in_cluster = Sets.newHashSet();

        for(Set<String> c : clusters) {
            for(String s : c) {
                in_cluster.add(s);
            }
        }

        this.supervised = in_cluster;

        Set<String> not_in_cluster = Sets.newHashSet(types);
        not_in_cluster.removeAll(in_cluster);

        this.unsupervised = not_in_cluster;

        System.err.println(not_in_cluster.size() + " of " + types.size() + " types not in clusters.");

        allowed = Maps.newHashMap();

        for(Set<String> c : clusters) {
            for(String s : c) {
                allowed.put(s, c);
            }
        }
    }

    public int ntypes() {
        return this.ntypes;
    }

    public Set<String> children(String x) {
        assert(children != null);
        assert(children.containsKey(x)) : "Key " + x + " not in hash";
        return children.get(x);
    }

    public Set<String> parents(String x) {
        assert(parents != null);
        assert(parents.containsKey(x)) : "Key " + x + " not in hash";
        return parents.get(x);
    }

    public void precompute_adjacency_hashes() {
        long startTime;
        long endTime;

        System.err.println("Precomputing adjacency hashes...");

        startTime = System.nanoTime();

        parents  = new HashMap<String,Set<String>>();
        children = new HashMap<String,Set<String>>();

        for(String s : types) {
            
            //            System.err.println("s="+s);

            // Parents
            Set<String> ps = new HashSet<String>();
            for(String t : types) {
                if(!s.equals(t)) {
                    if(like(t).contains(s)) {
                        ps.add(t);
                        //                        System.err.println("parent="+t);
                    }
                }
            }
            if(like(null).contains(s)) {
                ps.add(null);
            }
            parents.put(s, ps);

            if(ps.size() == 0) {
                if(this.unsupervised.contains(s)) {
                    System.err.println("Unsupervised type.");
                } else {
                    System.err.println("Supervised type.");
                }
            }

            assert(ps.size() > 0) : s + " has no parents!";
            
            Collection<String> cs = like(s);
            
            if(cs instanceof Set) {
                // Children
                children.put(s, (Set)cs);
            } else {
                children.put(s, new HashSet<String>(cs));
            }
        }

        // Children of start/diamond (null)
        Collection<String> cs = like(null);

        if(cs instanceof Set) {
            // Children
            children.put(null, (Set)cs);
        } else {
            children.put(null, new HashSet<String>(cs));
        }

        endTime = System.nanoTime();
        System.err.println("Done precomputing adjacency hashes in "
                           + ((endTime-startTime)/1000000000.0) + " seconds.");
    }

    public void make_index(Collection<String> strings) {
        Set<String> types = Sets.newHashSet();

        if(just_trigrams) {
            for(String s : strings) {
                types.add(s);
                for(String fg : get_ngrams(s,3)) {
                    if(trigram_index.containsKey(fg)) {
                        trigram_index.get(fg).add(s);
                    } else {
                        HashSet<String> key_value = Sets.newHashSet();
                        key_value.add(s);
                        trigram_index.put(fg,key_value);
                    }
                }
            }
        } else {

            for(String s : strings) {
                types.add(s);
                if(s.length() <= 3) {
                    for(String fg : get_ngrams(s,3)) {
                        if(trigram_index.containsKey(fg)) {
                            trigram_index.get(fg).add(s);
                        } else {
                            HashSet<String> key_value = Sets.newHashSet();
                            key_value.add(s);
                            trigram_index.put(fg,key_value);
                        }
                    }
                } else {
                    for(String fg : get_ngrams(s,4)) {
                        if(fourgram_index.containsKey(fg)) {
                            fourgram_index.get(fg).add(s);
                        } else {
                            HashSet<String> key_value = Sets.newHashSet();
                            key_value.add(s);
                            fourgram_index.put(fg,key_value);
                        }
                    }
                }
            }
        }
        this.types  = types;
        this.ntypes = types.size();
    }

    public static List<String> get_ngrams(String x, int order) {

        if(x.length() < order) {
            System.err.println(x + " length = " + x.length() + " but ngram order = " + order);
            System.exit(1);
        }
        
            //        assert(x.length() >= order) : x + " : " + order;

        x = stripAccents(x);

        List<String> ngrams = Lists.newArrayList();
        for(int i=0;i<x.length()-order+1;i++) {
            String ng = x.substring(i,i+order).toLowerCase(); // doesn't include end index
            if(ng.indexOf(" ")==-1) {
                // check alphanumeric
                for(int j=0;j<ng.length();j++) {
                    if(!Character.isLetter(x.charAt(j))) {
                        continue;
                    }
                }
                ngrams.add(ng);
            }
        }

        // DEBUG
        //        System.err.println(order + "grams for: " + x);
        //        for(String ng : ngrams) {
        //            System.err.println("\t" + ng);
        //        }
        // DEBUG

        //        assert(ngrams.size() > 0);

        return ngrams;
    }

    // Returns a collection of strings that share ngrams
    // and (optionally) that are allowed according the 
    // the cluster constraints

    // If "null" is passed as the input string, strings 
    // adjacent to START (\diamond) are returned
    public Collection<String> like(String input) {
        Set<String> friends = Sets.newHashSet();

        if(flattree!=null) {
            // Flattree

            // START -> *
            if(input == null) {
                friends.addAll(flattree.keySet());
                friends.addAll(this.unsupervised);
                return friends;
            }

            // ====================== SUPERVISED TYPE =========================
            if(supervised.contains(input)) { // Excludes the ROOT
                // * No friends
                // * Not allowed to start from \diamond
                return friends;
            } else if(flattree.containsKey(input)) { 
                // ============= Canonical name ================

                Set<String> temp = new HashSet<String>();
                temp.addAll(this.unsupervised);   // Add all unsupervised names

                // Prune any edges between strings with no trigrams in common
                List<String> ngrams = get_ngrams(input, 3);
                Set<String> triset = new HashSet<String>();
                for(String tg : ngrams) {
                    triset.addAll(trigram_index.get(tg));
                }

                // For all unsupervised types, check that they share
                // trigrams with the canonical name.
                for(String s : temp) {
                    if(triset.contains(s)) {
                        friends.add(s);
                    }
                }

                friends.addAll(flattree.get(input)); // Add all known aliases
            }

            // ===================== UNSUPERVISED TYPE =======================

            // No transitions (like supervised aliases)

            return friends;
        }

        if(just_trigrams) {
            
            // START -> x
            if(input == null) {

                friends.addAll(this.types);

            } else {

                List<String> ngrams = get_ngrams(input, 3);
                for(String fg : ngrams) { // for each ngram
                    Set<String> matching_strings = trigram_index.get(fg);
                    friends.addAll(matching_strings);
                }

            }

            // Remove self-transitions
            friends.remove(input);

        } else {
            System.err.println("Not implemented.");
            System.exit(1);
        }
        
        if(allowed!=null) { // if there are cluster constraints

            if(input == null) { // DIAMOND -> x
                friends.clear();
                if(canonical_names == null) {
                    friends.addAll(this.types);      // DIAMOND -> ANYTHING
                } else {
                    friends.addAll(this.types);      // DIAMOND -> ANYTHING
                    friends.removeAll(supervised);   // remove all links to supervised types
                    friends.addAll(canonical_names); // *except* those deemed canonical names
                }
                return friends;
            }
            else if(allowed.containsKey(input)) {
                // If input is in a cluster, only allow transitions to
                // other strings in the same cluster
                //                friends.retainAll(allowed.get(input));  // set intersection

                // Remove all links to names in OTHER clusters
                friends.removeAll(supervised);

                // Add back in strings from the cluster in question.
                // Overrule any trigram constraint if we know things
                // are in a cluster.
                friends.addAll(allowed.get(input));

                // Except the self-transitions
                friends.remove(input);
            } else {
                // if input is UNSUPERVISED (and not DIAMOND), it
                // can only transition to other unsupervised types

                if(this.no_unsupervised_to_supervised_edges) {
                    friends.removeAll(allowed.keySet());
                }
            }
        }

        // Nothing except START can transition to CANONICAL names
        if(canonical_names != null) {
            friends.removeAll(canonical_names);
        }
        
        return friends;
    }

    public static boolean shareTrigrams(String a, String b) {
        List<String> a_trigrams = get_ngrams(a,3);
        List<String> b_trigrams = get_ngrams(b,3);

        Set<String> tmp = Sets.newTreeSet();
        for (String x : a_trigrams)
            if (b_trigrams.contains(x))
                tmp.add(x);

        return tmp.size() > 0;
    }
    
    public static boolean shareFourgrams(String a, String b) {
    	List<String> a_fourgrams = get_ngrams(a,4);
        List<String> b_fourgrams = get_ngrams(b,4);

        Set<String> tmp = Sets.newTreeSet();
        for (String x : a_fourgrams)
            if (b_fourgrams.contains(x))
                tmp.add(x);

        return tmp.size() > 0;
    }

    public Collection<String> check_for_singletons() {

        assert(types != null);
        assert(types.size() > 0);

        Collection<String> singletons = Lists.newArrayList();
        for(String s : types) {
            if( like(null).contains(s) ) {    // \E edge null -> s
                continue;
            }
            for(String t : types) {
                if(!s.equals(t)) {
                    if(like(t).contains(s)) { // \E edge t    -> s
                        continue;
                    }
                }
            }
            singletons.add(s);
        }
        return singletons;
    }

    public void print_like(String s) {
        System.out.println("Like '" + s + "': ");
        for(String x : like(s)) {
            System.out.print(x + " ");
        } 
        System.out.println("");
    }
}