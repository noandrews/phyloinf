package edu.jhu.hlt.phylo.match;

import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.math3.util.Pair;

import com.beust.jcommander.internal.Lists;

import edu.jhu.hlt.optimize.BottouSchedule;
import edu.jhu.hlt.optimize.GainSchedule;
import edu.jhu.hlt.optimize.SGD;
import edu.jhu.hlt.optimize.BottouSchedule.BottouSchedulePrm;
import edu.jhu.hlt.optimize.SGD.SGDPrm;
import edu.jhu.hlt.optimize.function.DifferentiableBatchFunction;
import edu.jhu.hlt.phylo.distrib.LogLinearTransducer;
import edu.jhu.hlt.phylo.distrib.StringEditModel;
import edu.jhu.hlt.phylo.lang.Alphabet;
import edu.jhu.hlt.phylo.lang.AnnotatedString;
import edu.jhu.hlt.phylo.util.RandomNumberGenerator;
import edu.jhu.prim.vector.IntDoubleDenseVector;
import edu.jhu.prim.vector.IntDoubleVector;

public class StringEditModelTrainer {

    private Pair<List<AnnotatedString>,List<AnnotatedString>> readPairs(String filename) {
    	Path path = FileSystems.getDefault().getPath(filename);
    	String line = null;
    	List<AnnotatedString> inputs  = Lists.newArrayList();
    	List<AnnotatedString> outputs = Lists.newArrayList();
    	try {
        	BufferedReader reader = Files.newBufferedReader(path, Charset.defaultCharset() );
			while ( (line = reader.readLine()) != null ) {
				String [] io = line.trim().split("\t");
				inputs.add(new AnnotatedString(io[0]));
				outputs.add(new AnnotatedString(io[1]));
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
    	return new Pair<List<AnnotatedString>,List<AnnotatedString>>(inputs, outputs);
    }

    public static AnnotatedString [] fromList(List<AnnotatedString> list) {
        AnnotatedString [] arr = new AnnotatedString[list.size()];
        for(int i=0; i<list.size(); i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }

    int [] sampleWithoutReplacement(int N, int size) {
    	int [] ret = new int[size];
    	
    	List<Integer> choices = Lists.newArrayList();
    	for(int i=0; i<N; i++) choices.add(i);
    	
    	for(int i=0; i<size; i++) {
    		int index = RandomNumberGenerator.nextInt(choices.size());
    		ret[i] = choices.get(index);
    	}
    	
    	return ret;
    }
    
    public boolean execute(CommandLine cmd) {

        Alphabet A = null;

        // ===== Training =====

        if(cmd.hasOption("train") && !cmd.hasOption("model")) {
            System.err.println("Must specify model type.");
            System.exit(1);
        }

        // This will either populate a new dictionary, or use an
        // existing dictionary from a previously trained model.
        Pair<List<AnnotatedString>, List<AnnotatedString>> train_pairs = null;
        System.err.println("Loading training pairs...");
        train_pairs = readPairs(cmd.getOptionValue("train"));

        if(!cmd.hasOption("input")) {
            A = AnnotatedString.getAlphabet();
        }
        
        // Initialize the model
        LogLinearTransducer model = new LogLinearTransducer();
        
        // Fix the training pairs
        model.init(fromList(train_pairs.getFirst()), fromList(train_pairs.getSecond()));

        // Train the model
        
        BottouSchedulePrm schedprm = new BottouSchedulePrm();
        schedprm.initialLr = 10;
        schedprm.lambda = 0.01;
       	GainSchedule sched = new BottouSchedule(schedprm); 

        SGDPrm sgdprm = new SGDPrm();
        sgdprm.sched = sched;
        sgdprm.numPasses = 5;
        sgdprm.batchSize = 1;
        sgdprm.autoSelectLr = true;
        SGD optim = new SGD(sgdprm);
        IntDoubleVector param = new IntDoubleDenseVector();
        optim.maximize((DifferentiableBatchFunction) model, param);

        // Stop the alphabet growth
        A.stopGrowth();
        
        // ===== Compute avg held-out log prob =====
        if(cmd.hasOption("probs")) {
            Pair<List<AnnotatedString>, List<AnnotatedString>> pairs = readPairs(cmd.getOptionValue("probs"));
            AnnotatedString [] inputs = fromList(pairs.getFirst());
            AnnotatedString [] outputs = fromList(pairs.getSecond());
            model.init(inputs, outputs);
            double lp = model.getValue(param);
            System.out.println("held-out lp = "+(lp/inputs.length));
    	}

        return true;
    }

    private static Options createOptions() {
		Options options = new Options();

        options.addOption("batch","batch",true,"Batch size.");
        options.addOption("iter","iter",true, "Bumber of training iterations.");
        options.addOption("t","train",true,"Path to training data.");
        options.addOption("d","dev",true,"Path to development data.");
        options.addOption("m","model",true,"Model type.");
        options.addOption("a","dict",true,"Path to phonetic dictionary.");
        options.addOption("p","threads",true,"Number of threads to use.");
        options.addOption("probs","probs",true,"Path to test data.");
        
        return options;
    }

    public static void main(String [] args) {
        
        final String usage = "java " + StringEditModelTrainer.class.getName() + " [OPTIONS]";
		final CommandLineParser parser = new PosixParser();
		final Options options = createOptions();
		CommandLine cmd = null;
		final HelpFormatter formatter = new HelpFormatter();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e1) {
            System.err.println(e1.getMessage());
			formatter.printHelp(usage, options, true);
			System.exit(-1);
		}

		final StringEditModelTrainer trainer = new StringEditModelTrainer();
		final boolean success = trainer.execute(cmd);
		if(! success) {
			formatter.printHelp(usage, options, true);
			System.exit(-1);
		}
	}
}