// Nicholas Andrews
// noa@jhu.edu

package edu.jhu.hlt.phylo.distrib;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.base.Objects;
import com.google.common.collect.*;

import edu.jhu.hlt.optimize.AdaGrad;
import edu.jhu.hlt.optimize.SGD;
import edu.jhu.hlt.optimize.AdaGrad.AdaGradPrm;
import edu.jhu.hlt.optimize.SGD.SGDPrm;
import edu.jhu.hlt.optimize.function.DifferentiableBatchFunction;
import edu.jhu.hlt.phylo.lang.AnnotatedString;
import edu.jhu.hlt.phylo.lang.AnnotatedString.EntityType;
import edu.jhu.prim.list.IntArrayList;
import edu.jhu.prim.vector.IntDoubleSortedVector;
import edu.jhu.prim.vector.IntDoubleVector;

public class ConditionalStringModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6642039901389996261L;

	// Logging
	private static final Logger log = LogManager.getLogger(ConditionalStringModel.class.getName());
	
    public static double STEP_SIZE = 0.1d;

    int num_token_pairs = 0; // not distinct string pairs
    
    AnnotatedString [] names;
    
    AnnotatedString [] xs;
    AnnotatedString [] ys;
    
    IntDoubleVector param = new IntDoubleSortedVector();

    Set<EntityType> types                   = Sets.newHashSet();
    Map<EntityType, StringEditModel> models = Maps.newHashMap();
    Map<Integer,Set<Integer>> parents;
    Map<StringPair, Integer> pairIndex;

    LogLinearTransducer edit_model;
    
    public ConditionalStringModel(int nthread,
    							  List<String> data,
    							  List<AnnotatedString.Language> lang, 
    							  List<AnnotatedString.EntityType> types, 
    							  Map<Integer,Set<Integer>> parent_map) {    	
        initialize(nthread, data, lang, types);
        
        // Initialize edit model
        int size = AnnotatedString.getAlphabet().size();
        log.info("alphabet size = " + size);

        edit_model = new LogLinearTransducer();
        
        this.parents = parent_map;
    }

    public void initialize(int nthread, 
    		List<String> data, 
    		List<AnnotatedString.Language> lang, 
    		List<AnnotatedString.EntityType> mention_types) {
        
    	log.info(data.size() + " string pairs");
    	
    	assert(data.size() == lang.size());
    	assert(lang.size() == mention_types.size());
    	
    	double total_name_len = 0.0;
    	
    	names = new AnnotatedString[data.size()];
        for(int i=0; i<data.size(); i++) {
            types.add(mention_types.get(i));
            names[i] = new AnnotatedString(data.get(i), lang.get(i), mention_types.get(i));
            total_name_len += names[i].len();
        }
        double mean_len = total_name_len/data.size();
        log.info("Mean name len = " + mean_len);
    }
    
    public void finalize() {
    	
    	pairIndex = Maps.newHashMap();
    	num_token_pairs = 0;
    	
    	for(int y=0; y<names.length; y++) {
    		for(int x : parents.get(y)) {
    			num_token_pairs += 1;
    			String input;
    			if(x==y) {
    				input = null;
    			} else {
    				input = names[x].str();
    			}
    			String output = names[y].str();
        		StringPair pair = new StringPair(input, output, x, y);
        		if(!pairIndex.containsKey(pair)) {          // if this distinct pair isn't in the hash, give
            		pairIndex.put(pair, pairIndex.size());  // it a new index and add it
        		}
    		}
    	}
    	
    	System.err.println(num_token_pairs + " token pairs total");
    	System.err.println(pairIndex.size() + " type pairs total");
    	 
    	// Each unique (input string, output string) is mapped to a distinct
    	// position in these arrays
    	this.xs = new AnnotatedString[pairIndex.size()];
    	this.ys = new AnnotatedString[pairIndex.size()];
    	
    	for(StringPair pair : pairIndex.keySet()) {
    		int index = pairIndex.get(pair);
    		if(pair.input == null) {
    			xs[index] = null;
    		} else {
    			xs[index] = names[pair.x];
    		}
    		ys[index] = names[pair.y];
    	}
    	
    	log.info("initializing edit model with " + xs.length + " pairs");
    	edit_model.init(xs, ys); // do all the feature extraction
    	log.info("done initializing edit model");
    }
    
    public void optimize(Map<StringPair, Double> pairs) {
    	    	
    	if(pairs.size() < 1) {
    		RuntimeException e = new RuntimeException("no pairs in batch em step");
    		e.printStackTrace();
    		throw e;
    	}
    	
    	AnnotatedString [] inputs = new AnnotatedString[pairs.keySet().size()];
    	AnnotatedString [] outputs = new AnnotatedString[pairs.keySet().size()];
    	double [] ws = new double[pairs.keySet().size()]; // total number of pairs

    	int i = 0;
    	for(StringPair pair : pairs.keySet()) {
    		int pair_index = pairIndex.get(pair);
    		inputs[i] = this.xs[pair_index];
    		outputs[i] = this.ys[pair_index];
    		ws[i] = pairs.get(pair);
    		i += 1;
    	}
    	
    	log.info("initializing edit model...");
    	edit_model.data(inputs, outputs);
    	edit_model.setWeights(ws);
    	log.info("done initializing edit model.");
    	
    	log.info("optimizing edit model...");

        AdaGradPrm schedprm = new AdaGradPrm();
        schedprm.eta = STEP_SIZE;
        SGDPrm prm = new SGDPrm();
        prm.sched = new AdaGrad(schedprm);
        prm.numPasses = 2;        
        prm.batchSize = 1;
        prm.autoSelectLr = false;
        SGD optim = new SGD(prm);

        optim.maximize((DifferentiableBatchFunction) edit_model, param);
    	log.info("done optimizing edit model: " + edit_model.getNumDimensions());
    }
    
    // Return: token-token probabilities
    public double [] logp() {
    	// Step 1: Get all the type probabilities
    	double [] type_logp = edit_model.logp(this.xs, this.ys, this.param);
    	
    	// Step 2: Use the type probabilities to set the token probabilities
    	double [] ret = new double[num_token_pairs];
    	
    	int ret_i = 0;
    	for(int y=0; y<names.length; y++) {
    		for(int x : parents.get(y)) {
    			String input;
    			if(x==y) {
    				input = null;
    			} else {
    				input = names[x].str();
    			}
    			String output = names[y].str();
        		StringPair pair = new StringPair(input, output, x, y);
        		int index = pairIndex.get(pair);
        		ret[ret_i++] = type_logp[index];
    		}
    	}
    	
    	return ret;
    }
    
    public double [] logp(IntArrayList inputs, IntArrayList outputs) {
        //System.err.print(inputs.size() + " pairs.");
        Set<IO> pairs                = Sets.newHashSet();
        for(int i=0; i<inputs.size(); i++) {
            String input;
            if(inputs.get(i) < 0) input = null;
            else                  input = names[ inputs.get(i) ].str();
            pairs.add(new IO(input, names[outputs.get(i)].str(),
                             inputs.get(i), outputs.get(i)));
        }
        //System.err.println(" " + pairs.size() + " of them distinct.");

        List<IO> list = Lists.newArrayList();
        list.addAll(pairs);

        int [] xs = new int[list.size()];
        int [] ys = new int[list.size()];

        for(int i=0; i<list.size(); i++) {
            xs[i] = list.get(i).first_int;
            ys[i] = list.get(i).second_int;
        }

        double [] unique_logp = logp(xs, ys);

        double [] ret = new double[inputs.size()];
        for(int i=0; i<inputs.size(); i++) {
            String input;
            if(inputs.get(i) < 0) input = null;
            else                  input = names[ inputs.get(i) ].str();
            IO io = new IO(input, names[outputs.get(i)].str(),
                           inputs.get(i), outputs.get(i));
            int j = list.indexOf(io);
            ret[i] = unique_logp[j];
        }

        return ret;
    }

    public double [] logp(int [] xs, int [] ys) {
    	AnnotatedString [] inputs  = new AnnotatedString[xs.length];
    	AnnotatedString [] outputs = new AnnotatedString[xs.length];
    	for(int i=0; i<xs.length; i++) {
    		if(xs[i] == -1)         inputs[i] = null;
    		else                    inputs[i] = names[ xs[i] ];
    		outputs[i] = names[ ys[i] ];
    	}
    	return edit_model.logp(inputs, outputs);
    }

    public double logp(int x_index, int y_index) {
        throw new UnsupportedOperationException();
    }
    
    public double logp(String xstr, String ystr) {
        throw new UnsupportedOperationException();
    }
    
    public static class StringPair {
    	public int x;
    	public int y;
    	public String input;
    	public String output;
    	public StringPair(String input, String output, int x, int y) {
    		this.input = input;
    		this.output = output;
    		this.x = x;
    		this.y = y;
    	}
    	@Override
    	public boolean equals(final Object obj) {
    		StringPair other = (StringPair)obj;
    		return Objects.equal(input, other.input) && Objects.equal(output, other.output);
    	}
    	@Override
    	public int hashCode() {
    		return Objects.hashCode(input, output);
    	}
    }
    
    public static class IO {
        public final String first;
        public final String second;
        public final int first_int;
        public final int second_int;
        public IO(String I, String O, Integer I_int, Integer O_int) {
            this.first  = I;
            this.second = O;
            this.first_int = I_int;
            this.second_int = O_int;
        }

        public int hashCode() {
            int hashFirst = first != null ? first.hashCode() : 0;
            int hashSecond = second != null ? second.hashCode() : 0;
            return (hashFirst + hashSecond) * hashSecond + hashFirst;
        }

        public boolean equals(Object other) {
            if (other instanceof IO) {
                IO otherPair = (IO) other;
                return 
                    ((  this.first == otherPair.first ||
                        ( this.first != null && otherPair.first != null &&
                          this.first.equals(otherPair.first))) &&
                     (this.second == otherPair.second ||
                      ( this.second != null && otherPair.second != null &&
                        this.second.equals(otherPair.second))) );
            }

            return false;
        }
    }
}