package edu.jhu.hlt.phylo.distrib;

/**
 * This exception is thrown when we attempt to unobserve more events
 * than we have observed (see {@link Distribution#observe}), or when
 * an unnormalized distribution sums to <= 0.
 */

class NegativeCountException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2209850443127078553L;

	NegativeCountException(String message) {
		super(message);
	}
}