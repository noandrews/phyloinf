// Nicholas Andrews
// noa@jhu.edu

package edu.jhu.hlt.phylo.distrib;

import edu.jhu.hlt.phylo.lang.AnnotatedString;
import edu.jhu.hlt.phylo.lang.Alphabet;

public interface StringEditModel {
    
    public void setAlphabet(Alphabet A);
    public Alphabet getAlphabet();
    
    public void init(AnnotatedString [] inputs, AnnotatedString [] outputs);
    public double improve(AnnotatedString [] inputs, AnnotatedString [] outputs, double [] weights);
    public void improve(int [] batch, double [] ws);
    public double [] logp();
    public double logp(AnnotatedString input, AnnotatedString output);
    public double [] logp(AnnotatedString [] inputs, AnnotatedString [] outputs);
    public double calc_ll(AnnotatedString [] inputs, AnnotatedString [] outputs, double [] weights);
    public double calc_ll(AnnotatedString [] inputs, AnnotatedString [] outputs);    
    public AnnotatedString sample(AnnotatedString input);
    public void removeCachedData();
    public void setWeights(double [] ws);
    
}