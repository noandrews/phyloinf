// Nicholas Andrews
// noa@jhu.edu

package edu.jhu.hlt.phylo.distrib;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.beust.jcommander.internal.Maps;
import com.google.common.collect.Lists;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import edu.jhu.hlt.optimize.function.DifferentiableBatchFunction;
import edu.jhu.hlt.optimize.function.Regularizer;
import edu.jhu.hlt.optimize.function.ValueGradient;
import edu.jhu.hlt.optimize.functions.L2;
import edu.jhu.hlt.phylo.lang.Alphabet;
import edu.jhu.hlt.phylo.lang.AnnotatedString;
import edu.jhu.hlt.phylo.math.ArrayMath;
import edu.jhu.prim.list.IntArrayList;
import edu.jhu.prim.util.math.LogAddTable;
import edu.jhu.prim.vector.IntDoubleSortedVector;
import edu.jhu.prim.vector.IntDoubleVector;

public class LogLinearTransducer implements StringEditModel, Serializable, DifferentiableBatchFunction {
	
	private static final long serialVersionUID = 6283920842133288926L;

	// Hash function
    protected static HashFunction hf = Hashing.murmur3_32();
    
    // Size of input and output alphabets.  
    // E.g., the input alphabet characters are represented by integers
    // 0..sizeInAlph-1, with sizeInAlph and sizeInAlph+1 representing EOS and EOS'.
    protected int sizeInAlph;
    protected int sizeOutAlph;
	
    // State of the automaton.
    final static int NOEDIT = 0;    // state if previous action was a no-edit
    final static int EDIT = 1;      // state if previous action was some kind of edit (perhaps COPY)
    final static int PRENOEDIT = 2; // state if next action will be a no-edit
    final static int PREEDIT  = 3;  // state if next action will be some kind of edit (perhaps COPY)
    
    // Edit actions.  
    final static int SUB  = 0;
    final static int INS  = 1;
    final static int COPY = 2;
    final static int DEL  = 3;
    
    // Precompute some hash codes
    static final int null_hc = hf.newHasher().putString("NULL").hash().hashCode();
	static final int op_hc = hf.newHasher().putString("OP").hash().hashCode();
	static final int x_hc  = hf.newHasher().putString("X=").hash().hashCode();
	static final int y_hc  = hf.newHasher().putString("Y=").hash().hashCode();
	static final int cl_hc = hf.newHasher().putString("cl(X)=").hash().hashCode();
	static final int ca_hc = hf.newHasher().putString("ca(X)=").hash().hashCode();
    
    // These features don't depend on the data, so they can just be computed in the constructor
    protected static IntArrayList[][] state_features;
	
    // All state required to extract and index features
    protected Map<HashCode, Integer> fkeymap;
    //protected Map<Integer, Double> fvalmap;
    protected IntDoubleVector param;
    protected Features feat;
    
    // Reference to training data
    AnnotatedString [] inputs = null;
    AnnotatedString [] outputs = null;
    double [] weights = null;
    
    // Mappings from between characters and indices
    protected Alphabet X = null; // input alphabet
    protected Alphabet Y = null; // output alphabet

    protected Regularizer reg;
	private double SIGMA;
	
    public LogLinearTransducer() {
        // Initialize alphabets
        this.X = AnnotatedString.getAlphabet();
        this.Y = AnnotatedString.getAlphabet();
    	
    	this.sizeInAlph = X.size();
        this.sizeOutAlph = Y.size();
        
        this.SIGMA = 1.0;
        this.reg = new L2(SIGMA);
        
        // Initialize maps
        fkeymap = Maps.newHashMap();
        
        // Initialize features
        this.feat = new Features(sizeInAlph, sizeOutAlph);
        this.feat.computeCharClasses(this.X);
        
        // Initialize state transition features
        state_features = new IntArrayList[2][];
        for(int curr=0; curr<2; curr++) {
        	state_features[curr] = new IntArrayList[2];
        	for(int next=0; next<2; next++) {
        		int k1 = indexAddIfAbsent( hf.newHasher().putString("S1=").putInt(curr).putString("S2=").putInt(next).hash() );
        		int k2 = indexAddIfAbsent( hf.newHasher().putString("S2=").putInt(next).hash()                               );
        		state_features[curr][next] = new IntArrayList(2);
        		state_features[curr][next].add(k1);
        		state_features[curr][next].add(k2);
        	}
        }
       
        
    }
    
    // Dot product of given features with parameter vector fvalmap
    protected double dot(IntArrayList fs) {
        double sum = 0d;
        for(int i=0; i<fs.size(); i++) {
            sum += value(fs.get(i));
        }
        return sum;
    }
    
    protected void edit_count(FeatureInstance instance,
    		SumTotal sums,                 // some computation from the forward pass
    		CharPair cp,
    		int op,                        // INS, DEL, SUB, COPY
    		int ipos,                      // position in input string
    		AnnotatedString input,         // input string
    		int opos,                      // position in output string
    		AnnotatedString output,        // output string
    		double exp,                    // expected count
    		Features feat,
    		IntDoubleSortedVector grad) {

    	int [] es;
    	if(cp.x == feat.EOS || cp.x == feat.EOS_null) { // Must pick INS
    		es = Features.OPS_INS;
    	} else {
    		if(cp.equal) es = Features.OPS;
    		else         es = Features.OPS_NOCOPY;
    	}

    	for(int l=0; l<es.length; l++) {
    		int e = es[l];
    		for(int y=0; y<feat.OP_SIZE[e]; y++) {
    			double prob = Math.exp(sums.lp(e, y)); // FIXME: does lp expect the index l or the value es[l]=e?
    			double exp_count;
    			if(e == DEL || e == COPY)
    				if(e == op)              exp_count = exp*(1d - prob);
    				else                     exp_count = exp*(0d - prob);
    			else 
    				if(e == op && y == cp.y) exp_count = exp*(1d - prob);
    				else                     exp_count = exp*(0d - prob);

    			IntArrayList fs = instance.edit_feat[ipos][opos][l][y];
    			for(int i=0; i<fs.size(); i++) {
    				int find = fs.get(i);
    				grad.set(find, grad.get(find)+exp_count);
    			}
    		}
    	}
    }
    
    protected void edit_logcount(FeatureInstance instance,
		    SumTotal sums,
            CharPair cp,
            int op,                        // INS, DEL, SUB, COPY
            int ipos,                      // position in input string
            AnnotatedString input,         // input string
            int opos,                      // position in output string
            AnnotatedString output,        // output string
            double log_exp,                // expected count
            Features feat,
            IntDoubleSortedVector grad
            ) {
    	double exp = Math.exp(log_exp);
    	edit_count(instance, sums, cp, op, ipos, input, opos, output, exp, feat, grad);
    }
    
    private double state_dot(int next, int curr, Features feat) {
    	double sum = 0.0;
    	
    	for(int i=0; i<state_features[curr][next].size(); i++) {
    		int k = state_features[curr][next].get(i);
    		sum += value( k );
    	}

        return sum;
	}
    
    protected void state_logcount(int next, int curr, double log_exp, Features feat, IntDoubleSortedVector grad) {
        double exp = Math.exp(log_exp);
        state_count(next, curr, exp, feat, grad);
    }
    
    protected void countFeature(Integer k, IntDoubleSortedVector grad, double exp) {
    	double v = grad.get(k);
    	grad.set(k, v+exp);
    }
    
    protected void state_count(int next, int curr, double exp, Features feat, IntDoubleSortedVector grad) {
    	double [] sums = new double[Features.NUM_STATE];
        for(int y=0; y<sums.length; y++) {
            sums[y] = state_dot(next, curr, feat);
        }
        
        double total = ArrayMath.logSum(sums);
        
        for(int y=0; y<sums.length; y++) {
            double prob = Math.exp(sums[y] - total);

            assert(prob >= 0 && prob <= 1);

            double exp_count;
            if(y==next)
                exp_count = exp*(1.0 - prob);
            else
                exp_count = exp*(0.0 - prob);

            for(int i=0; i<state_features[curr][next].size(); i++) {
            	countFeature( state_features[curr][next].get(i), grad, exp_count );
            }
        }
    }
    
    protected double state_logp(int next, int curr, Features feat) {
        double [] sums = new double[Features.NUM_STATE];
        for(int y=0; y<sums.length; y++) {
            sums[y] = state_dot(y,curr,feat);
        }
        double total = ArrayMath.logSum(sums);
        return sums[next] - total;
    }
    
    public double value(int k) {
    	return param.get(k);
    }
    
    public int indexAddIfAbsent(HashCode f) {
    	if(fkeymap.containsKey(f)) {
    		return fkeymap.get(f);
    	}
    	int k = fkeymap.size();
    	fkeymap.put(f, k);
    	return k;
    }
    
    public int index(HashCode f) {
    	return fkeymap.get(f);
    }
    
    public static List<HashCode> getEditFeatures(int y, int op, int x, int ipos, int opos, AnnotatedString input, AnnotatedString output, Features feat) {
    	
        if(input == null) {
        	// Must insert
        	List<HashCode> fs = Lists.newArrayList(hf.newHasher().putInt(null_hc).putInt(y).hash());
        	return fs;
        }
        
        List<HashCode> fs = Lists.newArrayList();
        
        // P(op)
        fs.add( hf.newHasher().putInt(op_hc).putInt(op).hash());
        
        // P(op|x)
        fs.add( hf.newHasher().putInt(op_hc).putInt(op).putInt(x_hc).putInt(x).hash());
        
        // P(op|class(x))
        fs.add( hf.newHasher().putInt(op_hc).putInt(op).putInt(cl_hc).putInt( feat.getCharacterClass(x) ).hash());
        
        // P(op|case(x))
        fs.add( hf.newHasher().putInt(op_hc).putInt(op).putInt(ca_hc).putInt( feat.getCharacterCase(x) ).hash());
        
        // P(op,y|x)
        fs.add( hf.newHasher().putInt(op_hc).putInt(op).putInt(x_hc).putInt(x).putInt(y_hc).putInt(y).hash());
        
        return fs;
    }
    
    protected SumTotal getSumTotal(FeatureInstance instance, CharPair cp, int ipos, int opos, AnnotatedString input, AnnotatedString output, Features feat) {
   	 int MAX = feat.sizeOutAlph+2;
        double [] sums = new double[MAX*4];
        for(int d=0;d<sums.length;d++) sums[d] = Double.NEGATIVE_INFINITY;
        int [] es;

        if(cp.x == feat.EOS || cp.x == feat.EOS_null) { // Must pick INS
            es = Features.OPS_INS;
        } else {
            if(cp.equal) es = Features.OPS;
            else         es = Features.OPS_NOCOPY;      // INS, DEL, SUB
        }

        for(int l=0; l<es.length; l++) {
       	 	int e = es[l];
       	 	for(int y=0; y<feat.OP_SIZE[e]; y++) {     	 
       	 		IntArrayList fs = instance.edit_feat[ipos][opos][l][y];
           	 	assert(fs.size() != 0) : "empty feature list";
           	 	double exp_dot = Math.exp(dot(fs));
           	 	sums[e*MAX + y] = exp_dot;
            }
        }
        
        double logZ = ArrayMath.logSum(sums);
        assert(!Double.isInfinite(logZ) && !Double.isNaN(logZ)) : "NaN or divergent log sum total: " + logZ;
        
        return new SumTotal(sums, logZ, MAX);
   }
    
    public IntArrayList indices(List<HashCode> hcs) {
    	IntArrayList fs = new IntArrayList(hcs.size());
    	for(HashCode hc : hcs) {
    		fs.add( index(hc) );
    	}
    	return fs;
    }
    
    public IntArrayList indicesAddIfAbsent(List<HashCode> hcs) {
    	IntArrayList fs = new IntArrayList(hcs.size());
    	for(HashCode hc : hcs) {
    		fs.add( indexAddIfAbsent(hc) );
    	}
    	return fs;
    }
    
	protected static class SumTotal {
        double [] sums;
        double total;
        int MAX_OP_SIZE;
        public SumTotal(double [] sums, double total, int MAX_OP_SIZE) {
            this.sums        = sums;
            this.total       = total;
            this.MAX_OP_SIZE = MAX_OP_SIZE;
        }
        public double lp(int op, int y) {
            return sums[op*MAX_OP_SIZE + y] - total;
        }
    }
    
    public static class ForwardPassResult {
        public double [][][] alpha;
        public SumTotal [][] sums;
        FeatureInstance instance;
        public ForwardPassResult(double [][][] alpha, SumTotal [][] sums, FeatureInstance instance) {
            this.alpha = alpha;
            this.sums  = sums;
            this.instance = instance;
        }
    }
	
    public ForwardPassResult log_forward_pass(int k, AnnotatedString x, AnnotatedString y, Features feat) {

    	// Retrieve instance features from the cache
    	FeatureInstance instance = new FeatureInstance(x, y, feat, false);
    	
        int xlen = (x==null) ? 0 : x.len(); // length not including EOS or EOS'
        int ylen = y.len();
        
        double[][][] alpha = new double[4][][]; // include room for EOS symbol, and for a little sloshing over so that we don't have to do boundary tests
        for (int state=0;state<4;++state) {
            alpha[state] = new double[xlen+2][];
            for (int chx=0;chx<xlen+2;++chx) {
                alpha[state][chx] = new double[ylen+2];
                for (int chy=0; chy<ylen+2; ++chy) {
                    alpha[state][chx][chy] = Double.NEGATIVE_INFINITY;
                }
            }
        }

        SumTotal [][] sums = new SumTotal[xlen+2][];
        for(int chx=0; chx<xlen+2; ++chx) {
            sums[chx] = new SumTotal[ylen+2];
        }
        
        alpha[NOEDIT][0][0] = 0; // log(1)
        for (int i=0; i <= xlen; ++i) {
            for (int j=0; j <= ylen; ++j) {
                CharPair cp = new CharPair(x,y,i,j,feat.sizeInAlph,feat.sizeOutAlph);
                
                // State model
                alpha[PRENOEDIT][i][j]  = LogAddTable.logAdd(alpha[PRENOEDIT][i][j], alpha[NOEDIT][i][j]+state_logp(NOEDIT, NOEDIT, feat));
                alpha[PRENOEDIT][i][j]  = LogAddTable.logAdd(alpha[PRENOEDIT][i][j], alpha[EDIT  ][i][j]+state_logp(NOEDIT, EDIT, feat));
                alpha[PREEDIT  ][i][j]  = LogAddTable.logAdd(alpha[PREEDIT  ][i][j], alpha[NOEDIT][i][j]+state_logp(EDIT,   NOEDIT, feat));
                alpha[PREEDIT  ][i][j]  = LogAddTable.logAdd(alpha[PREEDIT  ][i][j], alpha[EDIT  ][i][j]+state_logp(EDIT,   EDIT, feat));
                
                sums[i][j] = getSumTotal(instance, cp, i, j, x, y, feat);
       
                // Edit model
                if (cp.equal) {
                    alpha[NOEDIT][i+1][j+1] = LogAddTable.logAdd(alpha[NOEDIT][i+1][j+1], alpha[PRENOEDIT][i][j]);
                    alpha[EDIT  ][i+1][j+1] = LogAddTable.logAdd(alpha[EDIT  ][i+1][j+1], alpha[PREEDIT  ][i][j] + sums[i][j].lp(COPY,0));
                }

                alpha[EDIT][i+1][j+1] = LogAddTable.logAdd(alpha[EDIT][i+1][j+1], alpha[PREEDIT][i][j] + sums[i][j].lp(SUB,cp.y));
                alpha[EDIT][i  ][j+1] = LogAddTable.logAdd(alpha[EDIT][i  ][j+1], alpha[PREEDIT][i][j] + sums[i][j].lp(INS,cp.y));
                alpha[EDIT][i+1][j  ] = LogAddTable.logAdd(alpha[EDIT][i+1][j  ], alpha[PREEDIT][i][j] + sums[i][j].lp(DEL,0));
            }
        }
        
        return new ForwardPassResult(alpha, sums, instance);
    }
    
    public static class EStepResult {
        public int start_index;
        public int stop_index;
        public int [] batch;
        public double [] logp;
        public IntDoubleSortedVector grad;
        public double ll;
        public EStepResult(int start_index, int stop_index, int [] batch, int N) {
            this.start_index = start_index;
            this.stop_index  = stop_index;
            this.batch = batch;
            logp  = new double[N];
            grad  = new IntDoubleSortedVector(); // FIXME: is Sorted the right thing to use here?

            for(int i=0; i<N; i++) {
                logp[i] = Double.POSITIVE_INFINITY;
            }
        }
        public void add(EStepResult result) {        	
            // sum the total ll
            ll += result.ll;
            
            // set the pair log probabilities
            for(int i=result.start_index; i<result.stop_index; i++) {
            	int index = batch[i];
                if(logp[index] < 0) {
                	System.err.println("batch.length = " + batch.length);
                	System.err.println("index = " + index);
                	System.err.println("i = " + i);
                	System.err.println("logp[index] = " + logp[index]);
                	RuntimeException e = new RuntimeException("Overwriting a value in logp!");
                    e.printStackTrace();
                    throw e;
                }
                logp[index] = result.logp[index];
            }

            // sum the gradients
            grad.add(result.grad);
        }
    }
    
    public EStepResult log_forward_backward(
    		AnnotatedString [] xs, 
    		AnnotatedString [] ys, 
    		double [] weights, 
    		int start_index,
    		int end_index,
    		int [] batch,
    		Features feat, 
    		boolean just_ll) {
    	
    	assert(weights != null);
    	assert(batch != null);
    	
        EStepResult result = new EStepResult(start_index, end_index, batch, weights.length);

        for(int k : batch) {
        	
            double weight = weights[k];           // weight of this training pair
            AnnotatedString x = xs[k];            // input string; could be null
            AnnotatedString y = ys[k];            // output string
            int xlen = (x==null) ? 0 : x.len();   // length not including EOS or EOS'
            int ylen = y.len();
            double log_z, log_z_reverse;          // partition function computed by forward and backward passes
            
            // forward pass
            ForwardPassResult forward = log_forward_pass(k, x, y, feat);
            
            FeatureInstance instance = forward.instance;
            double [][][] log_alpha = forward.alpha;
            SumTotal [][] sums      = forward.sums;

            log_z = log_alpha[NOEDIT][xlen+1][ylen+1]; // sum of all paths
         
            //System.err.println("log z = " + log_z);
            assert(!Double.isInfinite(log_z) && !Double.isNaN(log_z)) : "NaN or divergent partition; something is wrong";
            
            result.logp[k]  = log_z;
            result.ll      += weights[k] * log_z;

            if (just_ll) continue; // if just computing ll, move on

            // Backwards pass
            double log_scale = Math.log(weight) - log_z;
            
            double[][][] log_beta = new double[4][][];
            for (int state=0;state<4;++state) {
                log_beta[state] = new double[xlen+2][];
                for (int chx=0;chx<xlen+2;++chx) {
                    log_beta[state][chx] = new double[ylen+2];
                    for (int chy=0; chy<ylen+2; ++chy) {
                        log_beta[state][chx][chy] = Double.NEGATIVE_INFINITY;
                    }
                }
            }
            double lp; // to cache some computation
            log_beta[NOEDIT][xlen+1][ylen+1] = 0; // prob = 1
            for (int i=xlen; i >= 0; --i) {
                for (int j=ylen; j >=0; --j) {
                    CharPair cp = new CharPair(x,y,i,j,feat.sizeInAlph,feat.sizeOutAlph);
                    
                    // Count the edits
                    if (cp.equal) {
                        // A COPY by staying in the NOEDIT state
                        log_beta[PRENOEDIT][i][j] = LogAddTable.logAdd(log_beta[PRENOEDIT][i][j], log_beta[NOEDIT][i+1][j+1]);

                        // A COPY by moving to the EDIT state and deciding to COPY
                        lp = sums[i][j].lp(COPY,0);
                        log_beta[PREEDIT  ][i][j] = LogAddTable.logAdd(log_beta[PREEDIT  ][i][j], lp + log_beta[EDIT][i+1][j+1]);
                        edit_logcount(instance, sums[i][j], cp, COPY, i, x, j, y,
                                      log_alpha[PREEDIT][i][j] + lp + log_beta[EDIT][i+1][j+1] + log_scale,
                                      feat, result.grad);
                    }

                    // Substitute
                    lp = sums[i][j].lp(SUB, cp.y);
                    log_beta[PREEDIT][i][j] = LogAddTable.logAdd(log_beta[PREEDIT][i][j], lp + log_beta[EDIT][i+1][j+1]);
                    edit_logcount(instance, sums[i][j], cp, SUB, i, x, j, y,
                                  log_alpha[PREEDIT][i][j] + lp + log_beta[EDIT][i+1][j+1] + log_scale,
                                  feat, result.grad);

                    // Insert
                    lp = sums[i][j].lp(INS, cp.y);
                    log_beta[PREEDIT][i][j] = LogAddTable.logAdd(log_beta[PREEDIT][i][j], lp + log_beta[EDIT][i  ][j+1]);
                    edit_logcount(instance, sums[i][j], cp, INS, i, x, j, y,
                                  log_alpha[PREEDIT][i][j] + lp + log_beta[EDIT][i  ][j+1] + log_scale,
                                  feat, result.grad);

                    // Delete
                    lp = sums[i][j].lp(DEL, 0);
                    log_beta[PREEDIT][i][j] = LogAddTable.logAdd(log_beta[PREEDIT][i][j], lp + log_beta[EDIT][i+1][j  ]);
                    edit_logcount(instance, sums[i][j], cp, DEL, i, x, j, y,
                                  log_alpha[PREEDIT][i][j] + lp + log_beta[EDIT][i+1][j  ] + log_scale,
                                  feat, result.grad);

                    // Count the state transitions
                    lp = state_logp(NOEDIT, NOEDIT, feat);
                    log_beta[NOEDIT][i][j] = LogAddTable.logAdd(log_beta[NOEDIT][i][j], lp + log_beta[PRENOEDIT][i][j]);
                    state_logcount(NOEDIT,NOEDIT, log_alpha[NOEDIT][i][j] + lp + log_beta[PRENOEDIT][i][j] + log_scale,
                                   feat, result.grad);

                    lp = state_logp(NOEDIT, EDIT, feat);
                    log_beta[EDIT  ][i][j] = LogAddTable.logAdd(log_beta[EDIT  ][i][j], lp + log_beta[PRENOEDIT][i][j]);
                    state_logcount(NOEDIT,EDIT, log_alpha[EDIT  ][i][j] + lp + log_beta[PRENOEDIT][i][j] + log_scale,
                                   feat, result.grad);

                    lp = state_logp(EDIT, NOEDIT, feat);
                    log_beta[NOEDIT][i][j] = LogAddTable.logAdd(log_beta[NOEDIT][i][j], lp + log_beta[PREEDIT  ][i][j]);
                    state_logcount(EDIT,NOEDIT,log_alpha[NOEDIT][i][j] + lp + log_beta[PREEDIT  ][i][j] + log_scale,
                                   feat, result.grad);

                    lp = state_logp(EDIT, EDIT, feat);
                    log_beta[EDIT  ][i][j] = LogAddTable.logAdd(log_beta[EDIT  ][i][j], lp + log_beta[PREEDIT  ][i][j]);
                    state_logcount(EDIT,EDIT, log_alpha[EDIT  ][i][j] + lp + log_beta[PREEDIT  ][i][j] + log_scale,
                                   feat, result.grad);
                }
            }
            log_z_reverse = log_beta[NOEDIT][0][0];
            assert (Math.abs(log_z - log_z_reverse) < 1e-2) : "Forward probability != backward probability ("+log_z+" != "+log_z_reverse+")";
        }
        
        //System.err.println(result.ll);
        return result;
    }
    
    public double logp(int k, AnnotatedString x, AnnotatedString y) {
        int xlen = (x==null) ? 0 : x.len(); // length not including EOS or EOS'
        int ylen = y.len();
        ForwardPassResult forward = log_forward_pass(k, x, y, feat);
        double log_z = forward.alpha[NOEDIT][xlen+1][ylen+1];  // sum over all alignments
        return log_z;
    }
	
    /** Looking up characters in a string and checking whether they're
     * equal is a little bit annoying because of EOS and EOS' codes.
     * So this class encapsulates that stuff. If the input character
     * is not in the output alphabet, then this returns false.
     */
    public static class CharPair {
        int x,y;       // the two characters
        boolean equal; // are they considered equal?
        
        public CharPair(AnnotatedString strx, AnnotatedString stry, int i, int j, int sizeInAlph, int sizeOutAlph) {
            if (strx==null) {
                x = sizeInAlph+1;                                          // input EOS' character
                if (j >= stry.len()) { y = sizeOutAlph+1; equal = true; }  // output EOS' character
                else { y = stry.glyphAt(j); equal = false; }               // output regular character
            } else if (i >= strx.len()) {
                x = sizeInAlph;                                            // input EOS character
                if (j >= stry.len()) { y = sizeOutAlph; equal = true; }    // output EOS character
                else { y = stry.glyphAt(j); equal = false; }               // output regular character
            } else {
                x = strx.glyphAt(i);                                       // input regular character
                if (j >= stry.len()) { y = sizeOutAlph; equal = false; }   // output EOS character
                else { y = stry.glyphAt(j); equal = (strx.upperGlyphAt(i)==stry.upperGlyphAt(j)); }              // output regular character
            }
        }
    }
    
    public class FeatureInstance {
        
    	// Length of each string
    	public int xlen;
    	public int ylen;
    	
    	// References to the original strings
    	AnnotatedString x;
    	AnnotatedString y;
    	
    	// Extracted features for each:
    	//	* input string position
    	//	* output string position
    	//  * operation
    	//  * output character
    	IntArrayList [][][][] edit_feat;
    		
        public FeatureInstance(AnnotatedString x, AnnotatedString y, Features feat, boolean addIfAbsent) {
        	xlen = (x==null) ? 0 : x.len(); // length not including EOS or EOS'
            ylen = y.len();
            
            edit_feat = new IntArrayList[xlen+1][][][];
            for (int i=0; i <= xlen; ++i) {
                edit_feat[i] = new IntArrayList[ylen+1][][];
                for (int j=0; j <= ylen; ++j) {
                    CharPair cp = new CharPair(x,y,i,j,feat.sizeInAlph,feat.sizeOutAlph);
                    
                    int [] es;
                    if(cp.x == feat.EOS || cp.x == feat.EOS_null) { // Must pick INS
                        es = Features.OPS_INS;
                    } else {
                        if(cp.equal) es = Features.OPS;
                        else         es = Features.OPS_NOCOPY;      // INS, DEL, SUB
                    }
                                        
                    edit_feat[i][j] = new IntArrayList[es.length][];

                    //for(int e : es) {
                    for(int k=0; k<es.length; k++) {
                    	edit_feat[i][j][k] = new IntArrayList[feat.OP_SIZE[es[k]]];
                        for(int chy=0; chy<feat.OP_SIZE[es[k]]; chy++) {
                        	if(addIfAbsent) {
                        		edit_feat[i][j][k][chy] = indicesAddIfAbsent( getEditFeatures(chy, es[k], cp.x, i, j, x, y, feat) );
                        	} else {
                        		edit_feat[i][j][k][chy] = indices( getEditFeatures(chy, es[k], cp.x, i, j, x, y, feat) );
                        	}
                        }
                    }
                }
            }
        }
    }

    public static class Features implements Serializable {

        private static final long serialVersionUID = 7526472295622776148L;

        public int sizeInAlph;
        public int sizeOutAlph; 

        // Character codes
        public int BOS;      // beginning-of-string character
        public int EOS;      // end-of-string character
        public int EOS_null; // end-of-string character for null input
        public int EPS;      // epsilon character

        // Number of latent regions
        public final static int NUM_STATE   = 2;

        // Model types
        public final static int NUM_MODEL   = 5;

        // NOTE: INS and SUB match
        public final static int MODEL_SUB    = 0;
        public final static int MODEL_INS    = 1;
        public final static int MODEL_OP     = 2;
        public final static int MODEL_STATE  = 3;
        public final static int MODEL_SUBINS = 4;

        public final static int [] INS_ARRAY    = {MODEL_INS};
        public final static int [] SUBINS_ARRAY = {MODEL_SUB, MODEL_INS};
        public final static int [] OP_ARRAY     = {0,1,2,3};

        // Operation lists
        final static int [] OPS        = {SUB,INS,COPY,DEL}; // ORDER MATTERS
        final static int [] OPS_NOCOPY = {SUB,INS,DEL};
        final static int [] OPS_INS    = {INS}; 

        // Feature dimensions
        public int [] LABEL = new int[NUM_MODEL];
        public int [] OP_SIZE = new int[4];

        public int CONTEXT;
        
        // ================== Feature types ====================

        // Character classes
        public static int NUM_CHAR_CLASS    = 9;
        
        public static int CLASS_SPACE       = 0;
        public static int CLASS_CONS        = 1;
        public static int CLASS_VOWEL       = 2;
        public static int CLASS_PUNCT       = 3;
        public static int CLASS_OTHER       = 4;
        public static int CLASS_NUMBER      = 5;
        public static int CLASS_BOS         = 6;
        public static int CLASS_EOS         = 7;
        public static int CLASS_EOS_prime   = 8;

        public static int CASE_UPPER        = 0;
        public static int CASE_LOWER        = 1;
        public static int CASE_NONE         = 2;
        
        // Names for each feature type
        //protected Map<Integer, HashBiMap<Integer, String>> ftype_names  = Maps.newHashMap();
        
        // Character class for each input symbol
        protected int [] symbol_classes = null;
        protected int [] symbol_case    = null;
        
        // Quick lookup for equal characters (case insensitive)
        protected boolean [][] fuzzy_eq = null;
        
        // Some data structures to index into features quickly
        protected int num_feat_total = 0;
        protected final int [] num_feat_per_label = new int[NUM_MODEL];
        protected final int [][] feat_start       = new int[NUM_MODEL][];
        protected final int [] model_start        = new int[NUM_MODEL];
        protected final int [] NUM_FEAT_TYPE      = new int[NUM_MODEL];
        protected final int [][] FEAT_CLASS_SIZES = new int[NUM_MODEL][];
        protected final boolean[][] ACTIVE_FEAT   = new boolean[NUM_MODEL][];

        public Features(int sizeInAlph, int sizeOutAlph) {

            System.err.println(sizeInAlph + " characters in the input alphabet");
            System.err.println(sizeOutAlph + " characters in the output alphabet");

            this.sizeInAlph  = sizeInAlph;
            this.sizeOutAlph = sizeOutAlph;

            this.EOS         = sizeOutAlph;
            this.EOS_null    = sizeOutAlph+1;
            this.BOS         = sizeOutAlph+2;
            
            this.OP_SIZE[COPY]      = 1;
            this.OP_SIZE[DEL]       = 1;
            this.OP_SIZE[INS]       = sizeOutAlph;  // not allowed to insert EOS or EOS'
            this.OP_SIZE[SUB]       = sizeOutAlph;  // not allowed to substitute EOS or EOS'

            this.CONTEXT            = sizeInAlph+3; // Space of input characters
        }

        public boolean fuzzyEqual(int ch1, int ch2) {
        	if(ch1>=fuzzy_eq.length || ch2 >= fuzzy_eq.length) return false;
        	if(ch1<0 || ch2 <0) return false;
        	return fuzzy_eq[ch1][ch2];
        }
        
        // Precompute hash sets of character classes
        public void computeCharClasses(Alphabet X) {
            // Precompute character classes
            System.err.println("Precomputing character classes...");
            
            String CS = "bcdfghjklmnpqrstvwxz" + 
                "BCDFGHJKLMNPQRSTVWXZ";
            String VS = "aeiouy" +
                "AEIOUY";
            
            String PS = ",;:-!/'.%$&\"_?";
            
            // Determine character fuzzy equality
            int size = X.size();
            fuzzy_eq = new boolean[size][];
            for(int i=0; i<size; i++) {
        		Character c1 = (Character)X.lookupObject(i);
            	fuzzy_eq[i] = new boolean[size];
            	for(int j=0; j<size; j++) {
            		Character c2 = (Character)X.lookupObject(j);
            		if(Character.isUpperCase(c1) != Character.isUpperCase(c2) &&  // a,a = false; a,A = true
            		   Character.toLowerCase(c1) == Character.toLowerCase(c2)) {
            			fuzzy_eq[i][j] = true;
            		} else {
            			fuzzy_eq[i][j] = false;
            		}
            	}
            }
            
            // Character class mappings
            Iterator<?> iter = X.iterator();
        
            symbol_classes = new int[X.size()+3];
            symbol_case    = new int[X.size()+3];
            
            // Iterator through each Character in the alaphabet
            while (iter.hasNext()) {
                Object o = iter.next();
                Character c = (Character)o;
                int k = X.lookupIndex(o);
                
                if(Character.isUpperCase(c)) {
                    symbol_case[k] = 1;
                } else {
                    symbol_case[k] = 0;
                }

                if(CS.indexOf(c) != -1) 
                    symbol_classes[k] = CLASS_CONS;
                else if(VS.indexOf(c) != -1)
                    symbol_classes[k] = CLASS_VOWEL;
                else if(Character.isWhitespace(c))
                    symbol_classes[k] = CLASS_SPACE;
                else if(Character.isDigit(c))
                    symbol_classes[k] = CLASS_NUMBER;
                else if(PS.indexOf(c) != -1)
                    symbol_classes[k] = CLASS_PUNCT;
                else
                    symbol_classes[k] = CLASS_OTHER;
                
                //System.err.println(c + " : " + symbol_classes[k]);
            }
        
            // Add entries for BOS, EOS, EOS'
            symbol_classes[BOS]      = CLASS_BOS;
            symbol_classes[EOS]      = CLASS_EOS;
            symbol_classes[EOS_null] = CLASS_EOS_prime;
        }
    
        public int getCharacterClass(int x) {
            return symbol_classes[x];
        }

        public int getCharacterCase(int x) {
            return symbol_case[x];
        }
    }
    // -- end of Features class
    
	@Override
	public void setAlphabet(Alphabet A) {
		this.X = A;
		this.Y = A;
	}

	@Override
	public Alphabet getAlphabet() {
		return this.X;
	}

	@Override
	public void init(AnnotatedString[] inputs, AnnotatedString[] outputs) {
		this.inputs = inputs;
		this.outputs = outputs;
		
		// uniform instance weights (possibly partial counts)
		this.weights = new double[inputs.length];
		for(int i=0; i<inputs.length; i++) {
			weights[i] = 1.0;
		}
		
		// initialize features
        // Initialize the rest of the features
        System.out.println("initializing features...");
        for(int i=0; i<inputs.length; i++) {
        	new FeatureInstance(inputs[i], outputs[i], feat, true);
        }
        System.out.println("done initializing features...");
	}
	
	public void data(AnnotatedString[] inputs, AnnotatedString[] outputs) {
		this.inputs = inputs;
		this.outputs = outputs;
		
		// uniform instance weights (possibly partial counts)
		this.weights = new double[inputs.length];
		for(int i=0; i<inputs.length; i++) {
			weights[i] = 1.0;
		}
		
		// features assumed fixed
	}

	@Override
	public double improve(AnnotatedString[] inputs, AnnotatedString[] outputs,
			double[] weights) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void improve(int[] batch, double[] ws) {
		throw new UnsupportedOperationException();
	}

	@Override
	public double[] logp() {
		throw new UnsupportedOperationException();
	}
	
	public double[] logp(AnnotatedString[] inputs, AnnotatedString[] outputs, IntDoubleVector param) {
		this.param = param;
		this.inputs = inputs;
		this.outputs = outputs;
		int [] batch = new int[this.inputs.length];
		this.weights = new double[this.inputs.length];
		for(int i=0; i<this.inputs.length; i++) {
			batch[i] = i;
			this.weights[i] = 1.0;
		}
		EStepResult r = log_forward_backward(this.inputs, this.outputs, this.weights, 0, this.inputs.length, batch, feat, true);
		return r.logp;
	}

	@Override
	public double logp(AnnotatedString input, AnnotatedString output) {
		throw new UnsupportedOperationException();
	}

	@Override
	public double[] logp(AnnotatedString[] inputs, AnnotatedString[] outputs) {
		throw new UnsupportedOperationException();
	}

	@Override
	public double calc_ll(AnnotatedString[] inputs, AnnotatedString[] outputs, double[] weights) {
		throw new UnsupportedOperationException();
	}

	@Override
	public double calc_ll(AnnotatedString[] inputs, AnnotatedString[] outputs) {
		throw new UnsupportedOperationException();
	}

	@Override
	public AnnotatedString sample(AnnotatedString input) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeCachedData() {
		// nothing to do
	}

	@Override
	public double getValue(IntDoubleVector param, int[] batch) {
		this.param = param;
		EStepResult r = log_forward_backward(this.inputs, this.outputs, this.weights, 0, this.inputs.length, batch, feat, true);
		return r.ll - reg.getValue(param);
	}

	@Override
	public int getNumExamples() {
		return this.inputs.length;
	}

	@Override
	public double getValue(IntDoubleVector param) {
		this.param = param;
		int [] batch = new int[this.inputs.length];
		for(int i=0; i<this.inputs.length; i++) { batch[i] = i; }
		EStepResult r = log_forward_backward(this.inputs, this.outputs, this.weights, 0, this.inputs.length, batch, feat, true);
		return r.ll - reg.getValue(param);
	}

	@Override
	public int getNumDimensions() {
		return this.fkeymap.size();
	}

	@Override
	public IntDoubleVector getGradient(IntDoubleVector param) {
		this.param = param;
		int [] batch = new int[this.inputs.length];
		for(int i=0; i<this.inputs.length; i++) { batch[i] = i; }
		EStepResult r = log_forward_backward(this.inputs, this.outputs, this.weights, 0, this.inputs.length, batch, feat, false);
		IntDoubleVector grad = reg.getGradient(param);
		r.grad.subtract(grad);
		return r.grad;
	}

	@Override
	public ValueGradient getValueGradient(IntDoubleVector param) {
		this.param = param;
		int [] batch = new int[this.inputs.length];
		for(int i=0; i<this.inputs.length; i++) { batch[i] = i; }
		EStepResult r = log_forward_backward(this.inputs, this.outputs, this.weights, 0, this.inputs.length, batch, feat, false);
		double val = reg.getValue(param);
		IntDoubleVector grad = reg.getGradient(param);
		r.grad.subtract(grad);
		return new ValueGradient(r.ll-val, grad);
	}
	
	@Override
	public IntDoubleVector getGradient(IntDoubleVector param, int[] batch) {
		this.param = param;
		EStepResult r = log_forward_backward(this.inputs, this.outputs, this.weights, 0, this.inputs.length, batch, feat, false);
		IntDoubleVector grad = reg.getGradient(param);
		r.grad.subtract(grad);
		return r.grad;
	}

	@Override
	public ValueGradient getValueGradient(IntDoubleVector param, int[] batch) {
		this.param = param;
		EStepResult r = log_forward_backward(this.inputs, this.outputs, this.weights, 0, this.inputs.length, batch, feat, false);
		IntDoubleVector grad = reg.getGradient(param);
		r.grad.subtract(grad);
		return new ValueGradient(r.ll-reg.getValue(param), r.grad);
	}

	@Override
	public void setWeights(double[] ws) {
		this.weights = ws;
	}
}
