package edu.jhu.hlt.phylo.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import edu.jhu.hlt.phylo.lang.AnnotatedString.EntityType;
import edu.jhu.hlt.phylo.lang.AnnotatedString.Language;
import edu.jhu.hlt.phylo.lang.Corpus;
import edu.jhu.hlt.phylo.lang.Document;
import edu.jhu.hlt.phylo.lang.Mention;

public class SimpleDataReader implements DataReader {
	
	// Logging
	private static final Logger log = LogManager.getLogger(SimpleDataReader.class.getName());
	
    List<Document> documents = Lists.newArrayList();
    Corpus corpus = null;

    public SimpleDataReader() {
        super();
    }

    public void loadPath(String pathname) throws FileNotFoundException {
        File path = new File(pathname);

        if(!path.exists()) {
            throw new FileNotFoundException(pathname);
        }

        if(path.isDirectory()) {
            throw new RuntimeException(this.getClass().getName() + ": Dir given; expected file");
        }

        BufferedReader br = new BufferedReader(new FileReader(pathname));
        String line;
        int line_number = 0;
        try {
            while ((line = br.readLine()) != null) {
            	line = line.replaceAll("[\n\r]", "");

                String [] tokens = line.split("\t");

                // If it's a bad name, skip the tweet
                if(tokens.length != 4) {
                    log.warn("Skipping bad line: " + line);
                    continue;
                }

                // Create the document
                String goldLabel = tokens[0];
                String docId = line_number + "";
                Document doc = new Document(docId);

                // Set the document body
                String bodystr = tokens[3];
                Iterable<String> iter = Splitter.on(CharMatcher.BREAKING_WHITESPACE)
                	       .trimResults()
                	       .omitEmptyStrings()
                	       .split(bodystr);
                for(String s : iter) {
                	doc.addContext(s);
                }

                // Add the named entity mention
                Mention m = new Mention(tokens[2], docId, goldLabel, Language.English, EntityType.PER);
                doc.addMention(m);
             
                documents.add(doc);
                line_number ++;
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

	public Iterator<Document> iterator() {
		return documents.iterator();
	}
	
	public List<Document> getDocuments() {
		return documents;
	}
	
	public Corpus getCorpus() {
		if(corpus == null) {
			corpus = new Corpus(documents);
		}
		return corpus;
	}
}
