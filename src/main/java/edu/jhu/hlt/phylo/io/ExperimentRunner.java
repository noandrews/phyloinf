package edu.jhu.hlt.phylo.io;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import edu.jhu.hlt.phylo.clustering.phylo.PhyloMentionClusterer;
import edu.jhu.hlt.phylo.clustering.phylo.PhyloMentionClusterer.ContextModel;
import edu.jhu.hlt.phylo.clustering.phylo.PhyloMentionClusterer.SamplerInit;
import edu.jhu.hlt.phylo.evaluate.CEAFEvaluator;
import edu.jhu.hlt.phylo.evaluate.Evaluator;
import edu.jhu.hlt.phylo.evaluate.NVIEvaluator;
import edu.jhu.hlt.phylo.evaluate.b3Evaluator;
import edu.jhu.hlt.phylo.util.Instance;
import edu.jhu.hlt.phylo.util.RandomNumberGenerator;

public class ExperimentRunner {
	  @Parameter(names = "-corpus", description = "Input corpus location", required = true)
	  private String data_file;

	  @Parameter(names = "-debug", description = "Debugging output")
	  private boolean debug = false;
	  
	  @Parameter(names = "-root_log_weight", description = "Log weight of root vertex")
	  private double root_log_weight = -0.05;
	  
	  @Parameter(names = "-num_em_iter", description = "Number of EM iterations")
	  private int num_em_iter = 25;
	  
	  @Parameter(names = "-num_estep_samples", description = "Number of E-step samples")
	  private int num_estep_samples = 100;
	  
	  @Parameter(names = "-num_decode_samples", description = "Number of samples after training")
	  private int num_decode_samples = 100;
	  
	  @Parameter(names = "-ntopic", description = "Number of topics")
	  private int num_topic = 16;
	  
	  @Parameter(names = "-b3", description = "B-CUBED evaluation")
	  private boolean b3 = true;
	  
	  @Parameter(names = "-ceaf", description = "CEAF evaluation")
	  private boolean ceaf = false;
	  
	  @Parameter(names = "-nvi", description = "NVI evaluation")
	  private boolean nvi = false;
	  
	  @Parameter(names = "-resample_topics", description = "If topics should be resampled")
	  private boolean resample_topics = false;
	  
	  @Parameter(names = "-learning_rate_1", description = "Affinity model learning rate")
	  private double learning_rate_1 = 0.1;
	  
	  @Parameter(names = "-learning_rate_2", description = "Mutation model learning rate")
	  private double learning_rate_2 = 0.1;
	  
	  @Parameter(names = "-pruning", description = "Prune dissimilar strings")
	  private boolean pruning = false;
	  
	  @Parameter(names = "-pragma", description = "Use pragmatic version of mutation model")
	  private boolean pragma = false;
	  
	  @Parameter(names = "-gamma", description = "Strength of pragmatic effect")
	  private double gamma = 0.1;
	  
	  @Parameter(names = "-mbr", description = "Minimum Bayes risk decoding")
	  private boolean mbr = false;
	  	  
	  @Parameter(names = "-print_tree", description = "Output the best tree")
	  private boolean print_tree = false;
	  
	  @Parameter(names = "-random_init", description = "Random Gibbs initialization")
	  private boolean random_init = true;
	  
	  @Parameter(names = "-fixed_ordering", description = "Don't resample the ordering")
	  private boolean fixed_ordering = false;
	  
	  public void run() throws FileNotFoundException {
		  // Load the corpus
		  DataReader reader = new SimpleDataReader();
		  reader.loadPath(data_file);
		  
		  // Configuration
		  RandomNumberGenerator.setSeed(42);
		  
		  PhyloMentionClusterer phylo = new PhyloMentionClusterer();

		  phylo.setDebug(debug); 
		  phylo.setLogRootWeight(root_log_weight);
		  phylo.setNumTopics(num_topic);
		  phylo.setEMIter(num_em_iter);
		  phylo.setNumESamples(num_estep_samples);
		  phylo.setNumDecodeSamples(num_decode_samples);
		  phylo.setAdaGradEta(learning_rate_1);
		  phylo.setMutationModelLR(learning_rate_2);
		  phylo.setMBR(mbr);
		  phylo.setPruning(pruning);
		  phylo.usePragmaticModel(pragma);
		  phylo.fixedOrdering(fixed_ordering);
		  
		  if(pragma) phylo.setPragmaGamma(gamma);
		  
		  if(resample_topics) phylo.setContextModel(ContextModel.SAMPLED_TOPICS);
		  else phylo.setContextModel(ContextModel.FIXED_TOPICS);
		  
		  if(random_init) phylo.setSamplerInit(SamplerInit.RANDOM);
		  else phylo.setSamplerInit(SamplerInit.TARJAN);
		  
		  // Cluster
		  phylo.initialize(reader.getCorpus());
		  List<Instance> instances = phylo.cluster();
		  
		  // Evaluate
		  evaluate(instances);
		  
		  // Print
		  if(print_tree) {
			  phylo.printTree();
		  }
	  }
	  
	  public void evaluate(List<Instance> instances) {
		  PrintWriter pw = new PrintWriter(System.out,true);
		  if (b3) {
			  Evaluator b3Eval = new b3Evaluator();
			  b3Eval.evaluate(instances);
			  b3Eval.display(pw);
		  }
		  if (nvi) {
			  Evaluator nviEval = new NVIEvaluator();
			  nviEval.evaluate(instances);
			  nviEval.display(pw);
		  }
		  if (ceaf) {
			  Evaluator ceafEval = new CEAFEvaluator();
			  ceafEval.evaluate(instances);
			  ceafEval.display(pw);
		  }
	  }
	  
	  public static void main(String ... args) throws FileNotFoundException {
		  ExperimentRunner runner = new ExperimentRunner();
		  new JCommander(runner, args);
		  runner.run();
	  }
}
