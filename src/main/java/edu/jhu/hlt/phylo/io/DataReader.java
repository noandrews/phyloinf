package edu.jhu.hlt.phylo.io;

import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.List;

import edu.jhu.hlt.phylo.lang.Corpus;
import edu.jhu.hlt.phylo.lang.Document;

public interface DataReader {
	public void loadPath(String pathname) throws FileNotFoundException;
	public Iterator<Document> iterator();
	public List<Document> getDocuments();
	public Corpus getCorpus();
}
