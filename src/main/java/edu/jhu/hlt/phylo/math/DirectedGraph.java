package edu.jhu.hlt.phylo.math;

import java.util.*;
import com.google.common.collect.*;

/**
 * This class is a stub with only a few useful methods.
 * @author Nicholas Andrews
 */
public class DirectedGraph<T> {
	Map<T,Set<T>> children = Maps.newHashMap();
	Map<T,Set<T>> parents = Maps.newHashMap();

	// ==================================================

	public Set<T> getChildren(T v) {
		return children.get(v);
	}

	public Set<T> getParents(T v) {
		return parents.get(v);
	}

	public void addEdge(T v1, T v2) {
		if(children.containsKey(v1)) {
			children.get(v1).add(v2);
		} else {
			Set<T> es = Sets.newHashSet();
			es.add(v2);
			children.put(v1,es);
		}
	}
}