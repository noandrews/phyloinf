package edu.jhu.hlt.phylo.math;

import no.uib.cipr.matrix.VectorEntry;
import no.uib.cipr.matrix.sparse.SparseVector;

/**
 * Various math operations for mtj vectors.
 * 
 * @author Spence Green
 *
 */
public class VectorMath {

	private static final double LOG_E_2 = Math.log(2.0);

	/**
	 * Uses normalized counts internally.
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	public static double klDivergence(SparseVector from, SparseVector to) {
		final double fromTot = sum(from);
		final double toTot = sum(to);

		double result = 0.0;
		for(VectorEntry vEntry : from) {
			double fromVal = vEntry.get();
			if(fromVal == 0.0) continue;

			fromVal /= fromTot;
			double toVal = to.get(vEntry.index());
			toVal /= toTot;

			double logFract = Math.log(fromVal / toVal);
			if (logFract == Double.NEGATIVE_INFINITY)
				return Double.NEGATIVE_INFINITY; // can't recover

			result += fromVal * (logFract / LOG_E_2); // express it in log base 2
		}

		return result;
	}

	public static double sum(SparseVector v) {
		double[] values = v.getData();
		double sum = 0.0;
		for(double value : values)
			sum += value;
		return sum;
	}

    public static void substract(SparseVector u, SparseVector v) {
	for(VectorEntry ve : v) {
	    u.set(ve.index(), u.get(ve.index()) - ve.get());
	}
    }

    public static SparseVector add(SparseVector u, SparseVector v) {
	SparseVector sv = new SparseVector(Integer.MAX_VALUE);
	sv.add(u);
	sv.add(v);
	return sv;
    }

    public static boolean approxEqual(double x, double y) {
	return Math.abs(x-y) < 0.01;
    }

    public static boolean approxEqual(SparseVector u, SparseVector v) {
	for(VectorEntry ve : u) {
	    if(!approxEqual(ve.get(), v.get(ve.index()))) {
		return false;
	    }
	}
	return true;
    }
}
