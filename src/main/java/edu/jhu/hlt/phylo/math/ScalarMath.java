package edu.jhu.hlt.phylo.math;

public class ScalarMath {
    public static final double logAdd(double x, double y) {
        if(x == Double.NEGATIVE_INFINITY)
            return y;
        if(y == Double.NEGATIVE_INFINITY)
            return x;
        return Math.max(x, y) + Math.log1p(Math.exp( -Math.abs(x - y) ));
    }
}
