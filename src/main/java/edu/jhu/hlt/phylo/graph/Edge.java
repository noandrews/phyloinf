package edu.jhu.hlt.phylo.graph;

public class Edge implements Comparable<Edge> {
    
    final Node from, to;
    final double weight;
    
    public Edge(final Node argFrom, final Node argTo, final double argWeight){
        from = argFrom;
        to = argTo;
        weight = argWeight;
    }

    public Node getSource() {
        return from;
    }

    public Node getTarget() {
        return to;
    }
    
    public double getWeight() {
    	return weight;
    }

    public int compareTo(final Edge argEdge){
        return Double.compare(weight,argEdge.weight);
    }
}