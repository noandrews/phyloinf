package edu.jhu.hlt.phylo.graph;

import java.io.Serializable;
import java.util.*;

/**
 * An adjacency list for storing graph edges.
 * 
 * @author Spence Green
 *
 * @param <K>
 */
public class EdgeList <K> implements Serializable {

	private static final long serialVersionUID = -1056611764613706975L;

	private final Map<K,Set<K>> adjacencyList;
	private static final int INIT_CAPACITY = 5000;

	public EdgeList() {
		adjacencyList = new HashMap<K,Set<K>>(INIT_CAPACITY);
	}

	public Set<K> getAdjacentNodes(K node) {
		if(adjacencyList.containsKey(node))
			return adjacencyList.get(node);

		return Collections.unmodifiableSet(new HashSet<K>());
	}

	/**
	 * Adds a directed edge from node1 to node2.
	 * 
	 * @param node1
	 * @param node2
	 */
	public void addDirectedEdge(K node1, K node2) {
		if(adjacencyList.containsKey(node1)) {
			adjacencyList.get(node1).add(node2);
		} else {
			Set<K> edgeSet = new HashSet<K>();
			edgeSet.add(node2);
			adjacencyList.put(node1, edgeSet);
		}
	}
	
	public void addBidirectedEdge(K node1, K node2) {
		if(adjacencyList.containsKey(node1)) {
			adjacencyList.get(node1).add(node2);
		} else {
			Set<K> edgeSet = new HashSet<K>();
			edgeSet.add(node2);
			adjacencyList.put(node1, edgeSet);
		}
		if(adjacencyList.containsKey(node2))
			adjacencyList.get(node2).add(node1);
		else {
			Set<K> edgeSet = new HashSet<K>();
			edgeSet.add(node1);
			adjacencyList.put(node2, edgeSet);
		}
	}

	public void removeDirectedEdge(K node1, K node2) {
		if(adjacencyList.containsKey(node1) && adjacencyList.containsKey(node2)) {
			adjacencyList.get(node1).remove(node2);
			adjacencyList.get(node2).remove(node1);
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Map.Entry<K, Set<K>> entry : adjacencyList.entrySet()) {
			sb.append(entry.getKey().toString());
			for(K otherNode : entry.getValue())
				sb.append(",").append(otherNode.toString());
			sb.append("\n");
		}
		return sb.toString();
	}
}
