// Nicholas Andrews
// noa@jhu.edu

package edu.jhu.hlt.phylo.evaluate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.jhu.hlt.phylo.util.Instance;
import edu.jhu.hlt.phylo.util.Label;
import edu.jhu.hlt.phylo.util.Util;

/**
 * A "soft" version of the b^3 scoring metric as described by Bagga
 * and Baldwin (1998). This works on a symmetric matrix where the
 * (i,j) entry is the posterior probability that instances x_i and x_j
 * are in the same cluster.
 * 
 * @author Nicholas Andrews
 *
 */
public class softB3Evaluator implements Evaluator {

	protected String EVAL_NAME = "softB3";

	protected double precisions = 0.0;
	protected double precisions2 = 0.0;

	protected double recalls = 0.0;
	protected double recalls2 = 0.0;

	protected double f1 = 0.0;

	protected int nGoldClusters = 0;
	protected int nHypClusters = 0;

	protected TargetClusterCollection targets = null;

    protected double [][] posteriors = null;

	public softB3Evaluator(double [][] posteriors) {
        this.posteriors = posteriors;
    }

	/**
	 * Only evaluate gold clusters. This is the form of the ACE2008 evaluation
	 * on the 50 XDOC "target" entities.
	 * 
	 * @param targetClusterCollection
	 */
	public softB3Evaluator(double [][] posteriors, 
                           TargetClusterCollection targetClusterCollection) {
		targets = targetClusterCollection;
		EVAL_NAME += "_tgt";
	}
	
	@Override
	public void clear() {
		precisions = 0.0;
		precisions2 = 0.0;

		recalls = 0.0;
		recalls2 = 0.0;

		f1 = 0.0;

		nGoldClusters = 0;
		nHypClusters = 0;
	}

	/**
	 * Convenience method that returns an empty set (instead of null) when the key
	 * is not in the map.
	 * 
	 * @param <K>
	 * @param <V>
	 * @param label
	 * @param map
	 * @return
	 */
	protected static <K,V> Set<V> getLabeledSet(K label, Map<Label,Set<V>> map) {
		Set<V> labeledSet = map.get(label);
		if (labeledSet == null) {
			System.err.println("WARNING: No labeled set for: " + label.toString());
			labeledSet = new HashSet<V>();
		}
		return labeledSet;
	}
	
	/**
	 * Returns F1
	 */
	public double evaluate(List<? extends Instance> instances) {
		Map<Label,Set<Instance>> key = new HashMap<Label,Set<Instance>>();
		Map<Label,Set<Instance>> response = new HashMap<Label,Set<Instance>>();
		for(Instance instance : instances) {
			Label goldLabel = instance.getGoldLabel();

			//Map the labels to the cross-lingual reference 
			//(if this hasn't already been done in PipelineRunner)
			if(targets != null && targets.contains(goldLabel))
				goldLabel = targets.entityIdToClusterId(goldLabel);

			// Reconstruct key
			Util.addToSet(key, goldLabel, instance);

			// Reconstruct response
			Label hypLabel = instance.getHypLabel();
			Util.addToSet(response, hypLabel, instance);
		}

		nGoldClusters = key.size();
		nHypClusters = response.size();

		Set<Label> observedTargetGoldClusters = new HashSet<Label>(nGoldClusters);
		Set<Label> observedTargetHypClusters = new HashSet<Label>(nHypClusters);
		
		//Now do the scoring
        int i = 0;
		for(Instance instance : instances) {
			Label goldLabel = instance.getGoldLabel();
			Label hypLabel = instance.getHypLabel();
			
			if(targets != null) {
				//Only compute P/R/F1 for the target entities
				if( ! (targets.contains(goldLabel) || targets.containsLabelMapping(goldLabel))) continue;
				if(targets.contains(goldLabel))
					goldLabel = targets.entityIdToClusterId(goldLabel);
				
				// Update statistics based on the targets
				observedTargetGoldClusters.add(goldLabel);
				nGoldClusters = observedTargetGoldClusters.size();
				observedTargetHypClusters.add(instance.getHypLabel());
				nHypClusters = observedTargetHypClusters.size();
			}
			
			// Extract the clusters
			final Set<Instance> goldSet = getLabeledSet(goldLabel, key);
			Set<Instance> guessSet = new HashSet<Instance>(getLabeledSet(hypLabel, response));
		
			double guessSetSize = 0.0;
            double posteriorCorrect = 0.0;

            for(int j=0; j<instances.size(); j++) {
                double w = posteriors[i][j];
                Instance inst = instances.get(j);
                if(goldSet.contains(inst)) {
                    posteriorCorrect += w;
                }
                guessSetSize += w;
            }
			
			guessSet.retainAll(goldSet);
			final double numCorrect = (double) guessSet.size();

			// guessSetSize will only be zero if the entity consists entirely of non-target entities
			if (guessSetSize != 0.0) {
				precisions += posteriorCorrect / guessSetSize;
				//recalls    += numCorrect / (double) goldSet.size();
				recalls += posteriorCorrect / (double) goldSet.size();
				precisions2++;
				recalls2++;
			}

            i ++;
		}

		double prec = precisions / precisions2;
		double rec = recalls / recalls2;
		f1 = (2.0 * prec * rec) / (prec + rec);

		return f1;
	}

	@Override
	public void display(PrintWriter pw) {
		double dispP = precisions / precisions2;
		dispP *= 100.0;

		double dispR = recalls / recalls2;
		dispR *= 100.0;

		double dispF1 = f1 * 100.0;
		
		pw.printf("%s:\tP: %.2f\tR: %.2f\tF1: %.2f\tnGold: %d\tnGuess: %d%n",EVAL_NAME,dispP,dispR,dispF1,nGoldClusters,nHypClusters);
	}

	/**
	 * 
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length < 1) {
			System.err.printf("Usage: java %s [-t targets] clustering%n", b3Evaluator.class.getName());
			System.exit(-1);
		}

		String targetFile = (args[0].equals("-t")) ? args[1] : null;
		TargetClusterCollection targets = (targetFile == null) ? null : new TargetClusterCollection(targetFile);
		if(targets != null) System.out.printf("Loaded targets from %s%nTarget details: %s%n%n",targetFile,targets.toString());
		Evaluator b3Eval = (targets == null) ? new b3Evaluator() : new b3Evaluator(targets);

		String clusteringFile = (targetFile == null) ? args[0] : args[2];

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(clusteringFile)), "UTF-8"));

			List<Instance> instances = new ArrayList<Instance>();
			for(String line; (line = br.readLine()) != null; ) {
				String[] toks = line.split("\t");
				assert toks.length == 2;

				String hypLabel = toks[0];
				String[] chainIds = (toks[1]).split(",");
				for (String chainId : chainIds) {
					Instance instance = new Instance(chainId, hypLabel);
					instances.add(instance);
				}
			}
			br.close();

			b3Eval.evaluate(instances);
			b3Eval.display(new PrintWriter(System.out,true));

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
