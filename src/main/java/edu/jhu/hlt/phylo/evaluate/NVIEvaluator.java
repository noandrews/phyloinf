package edu.jhu.hlt.phylo.evaluate;

import java.io.PrintWriter;
import java.util.*;

import edu.jhu.hlt.phylo.util.Instance;
import edu.jhu.hlt.phylo.util.Label;


/** 
 * Implements the V, VI, and NVI clustering measures as described in "The NVI Clustering Evaluation Measure" 
 * (Reichart and Rappoport).
 *
 * @author Nick Andrews
 * @author Spence Green
 */
public class NVIEvaluator implements Evaluator {

	private String EVAL_NAME = "NVI";

	private double V;
	private double VI;
	private double NVI;
	private double NVIK;

	private double hck;
	private double hkc;

	private int nGoldClusters = 0;
	private int nHypClusters = 0;
	
	@Override
	public void clear() {
		V = 0.0;
		VI = 0.0;
		NVI = 0.0;
		NVIK = 0.0;

		hck = 0.0;
		hkc = 0.0;

		nGoldClusters = 0;
		nHypClusters = 0;
	}

	double HC(int[] C, int N) {
		double entropy = 0.0;
		for(int i = 0; i < C.length; i++) {// sum over classes
			double sum = (double) C[i];
			if(sum != 0)
				entropy += (sum / N) * Math.log(sum / N);
		}
		return -entropy;
	}

	double HK(int[] K, int N) {
		double entropy = 0.0;
		for(int i = 0; i < K.length; i++) {// sum over clusters
			double sum = (double) K[i];
			if(sum != 0)
				entropy += (sum / N) * Math.log(sum / N);
		}
		return -entropy;
	}

	void HCK_HKC(int[][] A, int[] C, int[] K, int N) {
		hck = 0.0;
		hkc = 0.0;
		for(int i = 0; i < K.length; i++) {
			for(int j = 0; j < C.length; j++) {
				double a_c_k = (double) A[j][i];
				if(K[i] != 0 && a_c_k != 0.0)
					hck += (a_c_k / N) * Math.log(a_c_k / K[i]);
				if(C[j] != 0 && a_c_k != 0.0)
					hkc += (a_c_k / N) * Math.log(a_c_k / C[j]);
			}
		}
		hck *= -1.0;
		hkc *= -1.0;
	}

	public double evaluate(List<? extends Instance> instances) {
		Map<Label,Integer> goldmap = new HashMap<Label,Integer>();
		Map<Label,Integer> predmap = new HashMap<Label,Integer>();

		for(Instance instance : instances) {
			Label gold = instance.getGoldLabel();
			if( ! goldmap.containsKey(gold))
				goldmap.put(gold,new Integer(goldmap.keySet().size()));

			Label pred = instance.getHypLabel();
			if( ! predmap.containsKey(pred))
				predmap.put(pred,new Integer(predmap.keySet().size()));
		}

		nGoldClusters = goldmap.keySet().size();   // |C|
		nHypClusters = predmap.keySet().size();  // |K|

		// build contingency matrix |C| x |K| such that a_{ij} is the number
		// of elements of class c_i that are assigned to cluster k_j
		int[][] A = new int[nGoldClusters][nHypClusters]; // assume java initializes to 0
		int[] C = new int[nGoldClusters]; //Row sums
		int[] K = new int[nHypClusters]; //Column sums

		for(Instance instance : instances) {
			int i = goldmap.get(instance.getGoldLabel());
			int j = predmap.get(instance.getHypLabel());
			A[i][j] += 1; 
			C[i] += 1;
			K[j] += 1;
		}

		final int N = instances.size();
		final double hc = HC(C,N);
		final double hk = HK(K,N);
		HCK_HKC(A,C,K,N);

		// VI measure
		VI = hck + hkc;

		// V measure
		final double h = (hc == 0.0) ? 1.0 : 1.0 - hck/hc;
		final double c = (hk == 0.0) ? 1.0 : 1.0 - hkc/hk;
		V = 2*h*c/(h+c);

		// NVI (both H(C) and H(K) normalization)
		NVI = (hc == 0.0) ? hk : (hck+hkc)/hc;
		NVIK = (hk == 0.0) ? hc : (hck+hkc)/hk;

		return NVI;
	}

	/**
	 * Start with a solution
	 * 
	 * @param A
	 */
	public double evaluate(int[][] A) {
		int[] C = new int[A.length]; //Row sums
		int[] K = new int[A[0].length]; //Column sums
		int N = 0;
		for(int i = 0; i < A.length; i++) {
			for(int j = 0; j < A[0].length; j++) {
				C[i] += A[i][j];
				K[j] += A[i][j];
				N += A[i][j];
			}
		}

		final double hc = HC(C,N);
		final double hk = HK(K,N);
		HCK_HKC(A,C,K,N);

		// VI measure
		VI = hck + hkc;

		// V measure
		final double h = (hc == 0.0) ? 1.0 : 1.0 - hck/hc;
		final double c = (hk == 0.0) ? 1.0 : 1.0 - hkc/hk;
		V = 2*h*c/(h+c);

		// NVI (both H(C) and H(K) normalization)
		NVI = (hc == 0.0) ? hk : (hck+hkc)/hc;
		NVIK = (hk == 0.0) ? hc : (hck+hkc)/hk;
		
		return NVI;
	}

	@Override
	public void display(PrintWriter pw) {
		pw.printf("%s:\tV: %.4f\tVI: %.4f\tNVI: %.4f\tNVIK: %.4f\tnGold: %d\tnGuess: %d%n",EVAL_NAME,this.V,this.VI,this.NVI,this.NVIK,this.nGoldClusters,this.nHypClusters);
	}


	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		int[][] A = new int[10][10];

		// This is the example from (Reichart and Rappoport, 2009) given in Table 1.
		for(int i = 0; i < A.length; i++) {
			for(int j = 0; j < 4; j++) {
				if(j == 0) 
					A[i][i] = 7;
				else {
					int j_mod = (i + j) % 10;
					A[i][j_mod] = 1;
				}
			}
		}

		NVIEvaluator eval = new NVIEvaluator();
		eval.evaluate(A);
		eval.display(new PrintWriter(System.out,true));
	}
}