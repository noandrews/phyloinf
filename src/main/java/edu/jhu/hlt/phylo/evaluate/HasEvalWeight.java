package edu.jhu.hlt.phylo.evaluate;

public interface HasEvalWeight {

	public double getEvalWeight();
	
	public void setEvalWeight(double w);
}
