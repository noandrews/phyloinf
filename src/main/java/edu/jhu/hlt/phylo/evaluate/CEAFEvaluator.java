package edu.jhu.hlt.phylo.evaluate;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.jhu.hlt.phylo.util.HungarianAlgorithm;
import edu.jhu.hlt.phylo.util.Instance;
import edu.jhu.hlt.phylo.util.Label;

/**
 * Implements the CEAF scorer described in
 * "On Coreference Resolution Performance Metrics" (Luo, 2005).
 * 
 * This is a modified version of the implementation from Reconcile 1.0.
 * 
 * @author mgormley
 */
public class CEAFEvaluator implements Evaluator {

	private static final String EVAL_NAME = "CEAF";
	
    private static final int PRECISION = 0, RECALL = 1, F = 2, RESULT_SIZE = 3;

    private double fMeasure;

	@Override
	public void clear() {
		fMeasure = 0.0;
	}
    
    @Override
    public void display(PrintWriter pw) {
        pw.printf(" %s: %.2f%n", EVAL_NAME,this.fMeasure * 100.0);
        pw.flush();
    }

    @Override
    public double evaluate(List<? extends Instance> instances) {
        double[][] scores = scoreRaw(instances);
        fMeasure = scores[0][F];
        return fMeasure;
    }

    private static double[][] newRawScoreArray() {
        double[][] result = new double[2][RESULT_SIZE];
        Arrays.fill(result[0], 0.0);
        Arrays.fill(result[1], 0.0);
        return result;
    }

    private static Map<Label, Set<Instance>> getGoldClusters(List<? extends Instance> instances) {
        Map<Label, Set<Instance>> goldClusters = new HashMap<Label, Set<Instance>>();

        // Make gold clusters
        for (Instance instance : instances) {
            Label goldLabel = instance.getGoldLabel();

            if (goldClusters.containsKey(goldLabel))
                goldClusters.get(goldLabel).add(instance);
            else {
                Set<Instance> iSet = new HashSet<Instance>();
                iSet.add(instance);
                goldClusters.put(goldLabel, iSet);
            }
        }
        return goldClusters;
    }

    private static Map<Label, Set<Instance>> getHypClusters(List<? extends Instance> instances) {
        Map<Label, Set<Instance>> hypClusters = new HashMap<Label, Set<Instance>>();

        // Make guess clusters
        for (Instance instance : instances) {
            Label hypLabel = instance.getHypLabel();
            if (hypClusters.containsKey(hypLabel))
                hypClusters.get(hypLabel).add(instance);
            else {
                Set<Instance> iSet = new HashSet<Instance>();
                iSet.add(instance);
                hypClusters.put(hypLabel, iSet);
            }
        }
        return hypClusters;
    }

    private double[][] scoreRaw(List<? extends Instance> instances) {
        double[][] result = newRawScoreArray();

        Map<Label, Set<Instance>> goldClusters = getGoldClusters(instances);
        Map<Label, Set<Instance>> hypClusters = getHypClusters(instances);

        double score = scoreHelper(goldClusters, hypClusters);
        double keyScore = scoreHelper(goldClusters, goldClusters);
        double resScore = scoreHelper(hypClusters, hypClusters);
        result[0][PRECISION] = score;
        result[0][RECALL] = score;
        result[1][PRECISION] = resScore;
        result[1][RECALL] = keyScore;
        result[0][F] = 2 * score / (keyScore + resScore);

        return result;
    }

    private double scoreHelper(Map<Label, Set<Instance>> goldClusters, Map<Label, Set<Instance>> hypClusters) {
        // Get the set of unique clusters
        int size = goldClusters.size();
        size = size >= hypClusters.size() ? size : hypClusters.size();
        if (size == 0) {
            return 0;
        }
        float[][] scores = new float[size][size];
        // This max variable is Reconcile's way of negating similarity since the
        // particular implementation of the Hungarian algorithm minimizes
        // instead of maximizing.
        float max = 0;
        for (float[] score : scores) {
            Arrays.fill(score, max);
        }
        int i = 0;
        for (Set<Instance> hypCluster : hypClusters.values()) {
            int j = 0;
            for (Set<Instance> goldCluster : goldClusters.values()) {
                scores[j][i] = max - (float) similarity(goldClusters, goldCluster, hypClusters, hypCluster);
                j++;
            }
            i++;
        }

        HungarianAlgorithm ha = new HungarianAlgorithm();
        int[][] solution = ha.computeAssignments(copyOf(scores));
        double cost = 0;
        for (i = 0; i < solution.length; i++) {
            if (solution[i][0] >= 0) {
                cost += (double) (max - scores[solution[i][0]][i]);
            }
        }

        if (Double.isNaN(cost)) {
            throw new RuntimeException("Cost was NaN");
        }
        return cost;
    }
    
    private static float[][] copyOf(float[][] array) { 
		float[][] aCopy = new float[array.length][];
		for(int i = 0; i < aCopy.length; i++) {
			aCopy[i] = new float[array[i].length];
			System.arraycopy(array[i], 0, aCopy[i], 0, array[i].length);
		}
		return aCopy; 
	}

    private static double similarity(Map<Label, Set<Instance>> goldClusters, Set<Instance> goldCluster,
            Map<Label, Set<Instance>> hypClusters, Set<Instance> hypCluster) {
        int numIntersect;
        if (!goldClusters.equals(hypClusters)) {
            numIntersect = numIntersect(goldCluster, hypCluster);
        } else {
            numIntersect = goldCluster.size();
        }
        return 2 * numIntersect / (double) (goldCluster.size() + hypCluster.size());
    }

    private static int numIntersect(Set<Instance> goldCluster, Set<Instance> hypCluster) {
        int res = 0;
        for (Instance gold : goldCluster) {
            if (hypCluster.contains(gold)) {
                res++;
            }
        }
        return res;
    }
}