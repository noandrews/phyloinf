package edu.jhu.hlt.phylo.evaluate;

import java.io.PrintWriter;
import java.util.List;

import edu.jhu.hlt.phylo.util.Instance;
import edu.jhu.hlt.phylo.util.Label;

/**
 * Simple accuracy calculation from a list of labeled instances.
 * 
 * @author Matt Gormley
 *
 */
public class AccuracyEvaluator implements Evaluator {

	private static final String EVAL_NAME = "Accuracy";

	private double result = 0.0;

	public double evaluate(List<? extends Instance> instances) {
		long numInstances = 0;
		long numPredicted = 0;
		for(Instance instance : instances) {
			Label instanceLabel = instance.getGoldLabel();
			if (instanceLabel != null) {
				numInstances++;
				Label predictedLabel = instance.getHypLabel();
				if(predictedLabel.equals(instanceLabel)) {
					numPredicted++;
				}
			}
		}

		result = (double)numPredicted/numInstances;

		return result;
	}

	@Override
	public void display(PrintWriter pw) {
		pw.printf("%s: %.2f%n", EVAL_NAME, result);
	}
	
	@Override
	public void clear() {
		result = 0.0;
	}
}
