package edu.jhu.hlt.phylo.evaluate;

import java.io.PrintWriter;
import java.util.List;

import edu.jhu.hlt.phylo.util.Instance;


public interface Evaluator {

	/**
	 * Run the evaluator.
	 * 
	 * @param instances
	 * @return A most commonly analyzed statistic (e.g. F1).
	 */
	double evaluate(List<? extends Instance> instances);
	
	/**
	 * Display results of the previous call to evaluate().
	 * 
	 * @param pw
	 */
	void display(PrintWriter pw);
	
	/**
	 * Reset to default internal state.
	 */
	void clear();
}
