package edu.jhu.hlt.phylo.evaluate;

import java.io.*;
import java.util.*;

import edu.jhu.hlt.phylo.util.Label;
import edu.jhu.hlt.phylo.util.StringLabel;


/**
 * Defines a mapping between an instance label and a cluster label. If an instance label is
 * not contained in the collection, then it should be assumed that the instance is a singleton
 * cluster.
 * 
 * @author Spence Green
 *
 */
public class TargetClusterCollection implements Serializable {

	private static final long serialVersionUID = -4723423352519016536L;

	private final Map<String,String> entityToCluster;

    // Initialize an empty target cluster collection
    public TargetClusterCollection() {
        entityToCluster = new HashMap<String,String>();
    }

	public TargetClusterCollection(String path) {
		entityToCluster = new HashMap<String,String>();
		load(path);
	}

    public void put(String entityId, String clusterLabel) {
        if(entityToCluster.containsKey(entityId)) {
            throw new RuntimeException("Target collection already contains entity: " + entityId);
        }
        entityToCluster.put(entityId, clusterLabel);
    }

	/**
	 * Loads a comma-separated file in which each line contains a list of instance labels. The line itself
	 * is interpreted as a single cluster.
	 * 
	 * @param path
	 */
	private void load(String path) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path)), "UTF-8"));

			for(int i = 1; br.ready(); i++) {
				String[] toks = br.readLine().trim().split(",");
				if(toks.length == 0) continue;
				String clusterLabel = String.format("%s_%d",toks[0],i);
				if(entityToCluster.values().contains(clusterLabel))
					throw new RuntimeException("Target collection already contains target " + clusterLabel);
				
				for(String entityId : toks) {
					if(entityToCluster.containsKey(entityId))
						throw new RuntimeException("Target collection already contains key " + entityId);
					entityToCluster.put(entityId, clusterLabel);
				}
			}

			br.close();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean contains(Label l) {
		return entityToCluster.containsKey(l.toString());
	}

	public Label entityIdToClusterId(Label l) {
		return new StringLabel(entityToCluster.get(l.toString()));
	}

	public boolean containsLabelMapping(Label l) {
		return entityToCluster.containsValue(l.toString());
	}
	
	public int size() {
		return entityToCluster.keySet().size();
	}

	public void print(PrintWriter pw) {
		StringBuilder sb = new StringBuilder();
		for(Map.Entry<String,String> entry : entityToCluster.entrySet())
			sb.append(String.format("%s\t%s\t%n",entry.getKey(),entry.getValue()));
		pw.println(sb.toString());
	}
	
	@Override
	public String toString() {
		Set<String> uniqueVals = new HashSet<String>(entityToCluster.values());
		return String.format("[ %d ids mapped to %d clusters ]",entityToCluster.keySet().size(),uniqueVals.size());
	}
}
