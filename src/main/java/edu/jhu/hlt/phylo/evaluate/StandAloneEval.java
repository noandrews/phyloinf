package edu.jhu.hlt.phylo.evaluate;

import java.io.*;
import java.util.*;

import edu.jhu.hlt.phylo.util.Instance;
import edu.jhu.hlt.phylo.util.Label;
import edu.jhu.hlt.phylo.util.StringLabel;

/**
 * 
 * @author Spence Green
 *
 */
public class StandAloneEval implements Evaluator {

	private final List<Evaluator> metrics;
	
	public StandAloneEval() {
		this(null);
	}

	public StandAloneEval(TargetClusterCollection targets) {
		metrics = new ArrayList<Evaluator>();
		
		if(targets == null) {
			metrics.add(new b3Evaluator());
			metrics.add(new NVIEvaluator());
			metrics.add(new CEAFEvaluator());
		} else {
			metrics.add(new b3Evaluator(targets));
		}
	}
	
	@Override
	public void clear() {}

	@Override
	public void display(PrintWriter pw) {
		for(Evaluator metric : metrics)
			metric.display(pw);
	}

	@Override
	public double evaluate(List<? extends Instance> instances) {
		for(Evaluator metric : metrics)
			metric.evaluate(instances);
		
		return 0;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length < 1) {
			System.err.printf("Usage: java %s [-t targets] [-c corrections] clustering%n", StandAloneEval.class.getName());
			System.exit(-1);
		}

		TargetClusterCollection targets = null;
		TargetClusterCollection corrections = null;
		String clusteringFile = null;
		for(int i = 0; i < args.length; i++) {
			if(args[i].equals("-t")) {
				String targetFile = args[++i];
				targets = new TargetClusterCollection(targetFile);
				System.out.printf("Loaded targets from %s%nTarget details: %s%n%n",targetFile,targets.toString());
				
			} else if(args[i].equals("-c")) {
				String corrFile = args[++i];
				corrections = new TargetClusterCollection(corrFile);
				System.out.printf("Loaded corections from %s%nCorrection details: %s%n%n",corrFile,corrections.toString());
				
			} else {
				clusteringFile = args[i];
				break;
			}
		}
		
		final Evaluator eval = (targets == null) ? new StandAloneEval() : new StandAloneEval(targets);

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(clusteringFile)), "UTF-8"));

			List<Instance> instances = new ArrayList<Instance>();
			for(String line; (line = br.readLine()) != null; ) {
				String[] toks = line.split("\t");
				assert toks.length == 2;

				String hypLabel = toks[0];
				StringTokenizer st = new StringTokenizer(toks[1],",");
				while(st.hasMoreTokens()) {
					Label goldLabel = new StringLabel(st.nextToken());
					if(corrections != null && corrections.contains(goldLabel))
						goldLabel = corrections.entityIdToClusterId(goldLabel);
						
					Instance instance = new Instance(goldLabel.toString(), hypLabel);
					instances.add(instance);
				}
			}
			br.close();

			eval.evaluate(instances);
			eval.display(new PrintWriter(System.out,true));

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
