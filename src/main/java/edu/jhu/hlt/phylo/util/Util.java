package edu.jhu.hlt.phylo.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Util {
    /**
     * @return The resulting set
     */
    public static <X,Y> Set<Y> addToSet(Map<X,Set<Y>> map, X key, Y value) {
        Set<Y> values;
        if (map.containsKey(key)) {
            values = map.get(key);
            values.add(value);
        } else {
            values = new HashSet<Y>();
            values.add(value);
            map.put(key, values);
        }
        return values;
    }
    
    /**
     * @return The resulting list.
     */
    public static <X,Y> List<Y> addToList(Map<X,List<Y>> map, X key, Y value) {
        List<Y> values;
        if (map.containsKey(key)) {
            values = map.get(key);
            values.add(value);
        } else {
            values = new ArrayList<Y>();
            values.add(value);
            map.put(key, values);
        }
        return values;
    }
}
