package edu.jhu.hlt.phylo.util;


public class StringLabel implements Label {

  private static final long serialVersionUID = -4067261621927424174L;
  private String label;

  public StringLabel(String label) {
    this.label = label.trim().intern();
  }

  @Override
  public String toString() {
    return label;
  }  

  @Override
  public int hashCode() {
    return label.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    else if( ! (o instanceof StringLabel)) return false;
    else return label.equals(((StringLabel) o).label);
  }

  public String value() {
    return label;
  }
}
