package edu.jhu.hlt.phylo.util;

import java.io.Serializable;

public class Instance implements Serializable {

    private static final long serialVersionUID = 7363889964150800372L;
    protected Label goldLabel = null;
    protected Label hypLabel = null;
    protected boolean visible_label = false;
    
    public Instance(String gold, String hyp) {
        this.goldLabel = new StringLabel(gold);
        this.hypLabel = new StringLabel(hyp);
    }

    public Instance(String gold) {
    	this.goldLabel = new StringLabel(gold);
    }
    
    public Instance(Label goldLabel) {
        this.goldLabel = goldLabel;
    }

    public boolean hasVisibleLabel() {
    	return visible_label;
    }
    
    public void setVisibleLabel(boolean b) {
    	visible_label = b;
    }
    
    public Label getGoldLabel() {
        return goldLabel;
    }

    public void setGoldLabel(Label label) {
        this.goldLabel = label;
    }

    public Label getHypLabel() {
        return hypLabel;
    }

    public void setHypLabel(Label l) {
        hypLabel = l;
    }

    @Override
    public String toString() {
        return String.format("gold:%s\tguess:%s", goldLabel, hypLabel);
    }

    @Override
    public boolean equals(Object obj) {
        // Here we use equality by reference, i.e. the Object notion of equality
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        // Here we mirror the equals method with the Object notion of hashCode
        return super.hashCode();
    }
    
}
