package edu.jhu.hlt.phylo.util;

import java.io.Serializable;

public interface Label extends Serializable {

	public abstract String toString();
	
}
