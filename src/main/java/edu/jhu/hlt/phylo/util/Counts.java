package edu.jhu.hlt.phylo.util;

import java.util.Map;
import java.util.Set;
import java.util.Collection;

import com.google.common.collect.Maps;

/**
 * Maintains counts. Keys are removed when their count hits 0.
 *
 * @see Histogram.java
 * @author Nicholas Andrews
 */

public final class Counts<E> {
    Map<E,Integer> stats = Maps.newHashMap();
    public Counts() { }
    public Counts(Collection<E> es) {
	for(E e : es) {
	    add(e);
	}
    }
    public int c(E e) { 
        if(stats.containsKey(e)) {
            return stats.get(e); 
        } else {
            return 0;
        }
    }
    public int size() { return stats.size(); }
    public void add(E e) {
	if(!stats.containsKey(e)){
	    stats.put(e,1);
	} else{
	    stats.put(e,stats.get(e)+1);
	}
    }
    public void add(E e, int count) {
	if(!stats.containsKey(e)){
	    assert(count>0);
	    stats.put(e,count);
	} else {
	    stats.put(e,stats.get(e)+count);
	}
    }
    public void remove(E e) {
	int c = stats.get(e);
	if(c==1) {
	    stats.remove(e);
	} else {
	    stats.put(e,stats.get(e)-1);
	}
    }
    public void remove(E e, int count) {
	int c = stats.get(e);
	if(c-count==0) {
	    stats.remove(e);
	} else {
	    stats.put(e,stats.get(e)-count);
	}
    }
    public void observe(E e, int count) {
	assert(count!=0);
	if(!stats.containsKey(e)){
	    //	    assert(count>0);
	    stats.put(e,count);
	} else {
	    int diff = stats.get(e) + count;
	    if(diff==0) {
		stats.remove(e);
	    } else {
		stats.put(e,diff);
	    }
	}
    }
    public Set<E> keys() {
	return stats.keySet();
    }
    public String inspect() {
	StringBuilder builder = new StringBuilder();
	if(stats.keySet().size()==0) return "No keys!";
	for(E key : stats.keySet()) {
	    builder.append("c("+key+")="+stats.get(key)+"\n");
	}
	return builder.toString();
    }
    public int count(E e) { return stats.get(e); }
    
    // -------------------------- UNIT TEST ---------------------------
    public static void main(String args[]) {
	// Observe / unobserve interface
	Counts<String> stats = new Counts<String>();
	
	System.out.println("Testing add/remove interface");

	stats.add("a");
	stats.add("a");
	stats.add("b");

	System.out.println(stats.inspect());
	stats.remove("b");
	System.out.println(stats.inspect());
	stats.remove("a");
	System.out.println(stats.inspect());
	stats.remove("a");
	System.out.println(stats.inspect());

	System.err.println("Testing observe interface");

	stats.observe("a",2);
	stats.observe("b",2);
	stats.observe("c",1);
	
	System.out.println(stats.inspect());
	stats.observe("a",-2);
	System.out.println(stats.inspect());
	stats.observe("b",-1);
	System.out.println(stats.inspect());
	stats.observe("b",-1);
	System.out.println(stats.inspect());
	stats.observe("c",-1);
	System.out.println(stats.inspect());

    }
}