/**
 * @author Nicholas Andrews
 */
package edu.jhu.hlt.phylo.util;

import java.util.Map;

public class StringUtil {

    public static int getSymbolIndex(Character c, Map<Character,Integer> symbols, boolean growdict) {
        if(growdict && !symbols.containsKey(c)) {
            symbols.put(c,symbols.size());
        }
        assert(symbols.containsKey(c)) : "Missing dictionary entry: " + c;
        return symbols.get(c);
    }
    
    public static int [] intify(String x, Map<Character,Integer> dict, boolean growdict) {
        int [] res = new int[x.length()];
        for(int i=0;i<x.length();i++) {
            res[i] = getSymbolIndex(x.charAt(i), dict, growdict);
        }
        return res;
    }

}