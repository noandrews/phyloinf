package edu.jhu.hlt.phylo.util;



public class ClassificationLabel implements Label {
	
	private static final long serialVersionUID = 8942613641783716806L;
	
	private int label;
	
	public ClassificationLabel(int label) {	
		this.label = label;
	}

	@Override
	public String toString() {
		return String.valueOf(label);
	}
	
	@Override
	public boolean equals(Object o) {
		if(this == o) return true;
		else if( ! (o instanceof ClassificationLabel)) return false;
		else return label == ((ClassificationLabel) o).label;
	}
	
	@Override
	public int hashCode() {
		return Integer.valueOf(label).hashCode();
	}

	public int value() {
		return label;
	}
}
