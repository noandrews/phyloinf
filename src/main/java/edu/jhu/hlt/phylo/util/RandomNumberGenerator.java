// Nicholas Andrews
// noa@jhu.edu

package edu.jhu.hlt.phylo.util;

import java.util.Random;

import org.apache.commons.math3.random.RandomAdaptor;
import org.apache.commons.math3.random.Well44497b;
import org.apache.commons.math3.random.RandomGenerator;

/**
 * A wrapper class for a static random number generator.
 *
 * @author Nicholas Andrews
 */
public class RandomNumberGenerator {
    static RandomGenerator rng = new Well44497b(0);
    static public void setSeed(int seed) {
        rng.setSeed(seed);
    }
    static public double nextDouble() {
        return rng.nextDouble();
    }
    static public float nextFloat() {
        return rng.nextFloat();
    }
    static public int nextInt(int total) {
        return rng.nextInt(total);
    }
    static public double nextGaussian() {
        return rng.nextGaussian();
    }
    static public RandomGenerator getGenerator() {
        return rng;
    }
    static public Random getRandom() {
    	return RandomAdaptor.createAdaptor(rng);
    }
}