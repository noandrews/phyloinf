package edu.jhu.hlt.phylo.lang;

import java.io.Serializable;

import com.google.common.primitives.Longs;

import edu.jhu.hlt.phylo.lang.AnnotatedString.EntityType;
import edu.jhu.hlt.phylo.lang.AnnotatedString.Language;

public class Mention implements Serializable {

  private static final long serialVersionUID = 7333171775600537580L;
  private static long count = 0;
  
  private long id;
  private Language language;
  private String docId;
  private String entity;
  private String mention;
  private EntityType type;

  public Mention(String mention, String docId, String entity, Language lang, EntityType type) {
	  this.id = count++;
	  this.language = lang;
	  this.docId = docId;
	  this.entity = entity;
	  this.mention = mention;
	  this.type = type;
  }
  
  public Language getLanguage() { return language; }
  public String getDocId() { return docId; }
  public String getMention() { return mention; }
  public long getId() { return id; }
  public String getEntity() { return entity; }
  public EntityType getType() { return type; }
   
  @Override public boolean equals(final Object obj) {
      if(obj == this) return true;
      if(obj == null) return false;

      if (getClass ().equals (obj.getClass ())) {
          final Mention other = (Mention) obj;
          return id == other.id;
      }

      return false;
  }

  @Override public int hashCode() { return Longs.hashCode(id); }
}
