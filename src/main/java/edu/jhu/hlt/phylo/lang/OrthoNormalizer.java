package edu.jhu.hlt.phylo.lang;

import java.util.List;

public interface OrthoNormalizer {

  public List<String> normalize(String s);
}
