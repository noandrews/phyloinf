package edu.jhu.hlt.phylo.lang;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.io.File;
import java.io.FileReader;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.Configuration;

public class StopWords {

    static final Pattern punctuation = Pattern.compile("^(\\p{Punct}|\u00AD)+$");
	private static final Pattern singleChar = Pattern.compile("^.$");

	private Set<String> stopWords = new HashSet<String>();
	private List<Pattern> patterns = new ArrayList<Pattern>();

	private static StopWords instance = null;

	private StopWords() {}

	/**
	 * Factory method for the default english+arabic stop words list.
	 * @return
	 */
	public static StopWords getInstance() {
		if(instance == null) {
            try {
                instance = new StopWords();
                System.err.println("Loading configuration...");
                Configuration config = new PropertiesConfiguration("phylo.cfg");
                instance.addResource(config.getString("phylo.english_stopwords"));
                System.err.println("Done...");
                instance.addRegex(punctuation);
                instance.addRegex(singleChar);
            } catch (ConfigurationException e) {
                System.err.println("Problem loading configuration: " + e.getMessage());
                System.exit(1);
            }
		}
		return instance;
	}

	private void addRegex(Pattern pattern) {
		patterns.add(pattern);
	}

	private void addResource(String resource) {
        System.err.println("Loading: " + resource);
		try {
            File file = new File(resource);
            System.err.println(file.getCanonicalPath());
			BufferedReader reader = new BufferedReader(new FileReader(resource));
			String line;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.startsWith("#")) {
					continue;
				}
				if (!line.equals("")) {
					stopWords.add(line.trim().toLowerCase());
				}
			}
			reader.close();
		} catch (IOException e) {
            System.err.println("IO exception!");
			throw new RuntimeException(e);
		}
        System.err.println("Loaded resource");
	}

	public boolean isStopWord(String word) {
		word = word.trim().toLowerCase();
		if (stopWords.contains(word)) {
			return true;
		}
		for (Pattern p : patterns) {
			if (p.matcher(word).find()) {
				return true;
			}
		}
		return false;
	}
}
