package edu.jhu.hlt.phylo.lang;

import java.util.List;

import com.google.common.collect.Lists;

public class Document {
	String doc_id;
	
	List<Mention> mentions = Lists.newArrayList();
	List<String> context = Lists.newArrayList();
	
	public Document(String id) { this.doc_id = id; };
	public void addMention(Mention m) { mentions.add(m); }
	public void addContext(String s) { context.add(s); }
	public List<Mention> getMentions() { return mentions; }
	public List<String> getContext() { return context; }
}
