package edu.jhu.hlt.phylo.lang;

import java.util.List;

import com.beust.jcommander.internal.Lists;

public class Corpus {
	List<Document> docs = Lists.newArrayList();
	List<Mention> mentions = Lists.newArrayList();
	
	public Corpus(List<Document> docs) {
		this.docs = docs;
		
		for(Document d : docs) {
			for(Mention m : d.getMentions()) {
				mentions.add(m);
			}
		}
	}
	
	public List<Document> docs() { return docs; }
	public List<Mention> mentions() { return mentions; }
}
