package edu.jhu.hlt.phylo.lang;

import com.google.common.base.Objects;

import gnu.trove.map.hash.TCharIntHashMap;
import edu.jhu.hlt.phylo.lang.Alphabet;

public class AnnotatedString {
    static Alphabet A = new Alphabet();
    static TCharIntHashMap charfreq = new TCharIntHashMap();

    public static enum Language {Arabic, English, Czech, French, Urdu, Spanish, German}
    public static enum EntityType {GPE, PER, FAC, WEA, VEH, LOC, ORG, ACRONYM}

    Language language;
    EntityType type;

    int [] glyphs;
    int [] upper_glyphs;
    String s;

    public AnnotatedString(String s) {
        this(s, Language.English, EntityType.PER);
    }

    public AnnotatedString(String s, Language l, EntityType t) {
        this.s = s;
        this.language = l;
        this.type = t;
        this.glyphs = str2seq(s, A);
        this.upper_glyphs = str2seq(s.toUpperCase(), A);
    }

    public static void printCharFreq() {
        System.err.println(charfreq.size() + " distinct characters");
        for(char c : charfreq.keys()) {
            System.err.println(c + " : " + charfreq.get(c));
        }
    }

    public static int [] str2seq(String x, Alphabet A) {
		int [] res = new int[x.length()];
		for(int i=0;i<x.length();i++) {
            Character c = x.charAt(i);
            if(charfreq.containsKey(c)) {
                int freq = charfreq.get(c);
                charfreq.put(c, freq+1);
            } else {
                charfreq.put(c, 1);
            }
			int index = A.lookupIndex(c);
			if(index<0) {
                System.err.println("OOV character: " + x.charAt(i));
                System.exit(1);
			}
			res[i] = index;
		}
		return res;
	}

    public static int [] str2seq(String x) {
		int [] res = new int[x.length()];
		for(int i=0;i<x.length();i++) {
            Character c = x.charAt(i);
            if(charfreq.containsKey(c)) {
                int freq = charfreq.get(c);
                charfreq.put(c, freq+1);
            } else {
                charfreq.put(c, 1);
            }
			int index = A.lookupIndex(c);
			if(index<0) {
                System.err.println("OOV character: " + x.charAt(i));
                System.exit(1);
			}
			res[i] = index;
		}
		return res;
	}

    public static String seq2str(int [] x) {
        StringBuilder builder = new StringBuilder();
        for(int i=0; i<x.length; i++) {
            builder.append((Character)A.lookupObject(x[i]));
        }
        return builder.toString();
    }

    public static String seq2str(int [] x, Alphabet A) {
        StringBuilder builder = new StringBuilder();
        for(int i=0; i<x.length; i++) {
            builder.append((Character)A.lookupObject(x[i]));
        }
        return builder.toString();
    }

    public int len() { return s.length(); }

    public String str() { return s; }
    public Character charAt(int pos) { return s.charAt(pos); }

    public EntityType getType() { return type; }
    public Language getLanguage() { return language; }
    
    public static Alphabet getAlphabet() { return A; }
    public static void setAlphabet(Alphabet A) { AnnotatedString.A = A; }
    
    @Override
    public int hashCode(){
        return Objects.hashCode(s);
    }

    @Override
    public boolean equals(final Object obj){
        if(obj instanceof AnnotatedString){
            final AnnotatedString other = (AnnotatedString) obj;
            return Objects.equal(s, other.s);
        } else{
            return false;
        }
    }
    
    @Override
    public String toString() {
    	return str();
    }

    public int [] glyphs() {
    	return glyphs;
    }
    
	public int glyphAt(int j) {
		return glyphs[j];
	}
	
	public int upperGlyphAt(int j) {
		return upper_glyphs[j];
	}
}