package edu.jhu.hlt.phylo.topics;

import java.io.*;
import cc.mallet.types.*;

import cc.mallet.topics.FixedParallelTopicModel;

public class MalletTopicModel {

    FixedParallelTopicModel model;

    public MalletTopicModel(int ntopics) {
        model = new FixedParallelTopicModel(ntopics);
    }

    public void train(InstanceList instances) {
        model.addInstances(instances);

        try {
            model.estimate();
        } catch (Exception e) {
            // ...
        }
    }

    public void test(InstanceList instances) {
        // TODO
    }
    
    public static void main (String[] args) throws IOException {
        
        System.err.println("Loading instances from " + args[0] + "...");
        //        InstanceList instances = importer.readDirectory(new File(args[0]));
        InstanceList instances = InstanceList.load(new File(args[0]));
        System.err.println(instances.size() + " instances loaded.");
        MalletTopicModel topics = new MalletTopicModel(20);
        System.err.println("Training model...");
        topics.train(instances);

    }
}
